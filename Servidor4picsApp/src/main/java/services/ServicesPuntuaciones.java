package services;

import dao.DaoPuntuaciones;
import models.Puntuacion;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@RequestScoped
@Named
public class ServicesPuntuaciones {

    @Inject
    private DaoPuntuaciones daoPuntuaciones;

    public ServicesPuntuaciones() {
        daoPuntuaciones = new DaoPuntuaciones();
    }

    public List<Puntuacion> getAllPuntuaciones() {
        return daoPuntuaciones.getAllPuntuaciones();
    }

    public boolean savePuntuaciones(List<Puntuacion> puntuaciones){
        return daoPuntuaciones.savePuntuaciones(puntuaciones);
    }
}
