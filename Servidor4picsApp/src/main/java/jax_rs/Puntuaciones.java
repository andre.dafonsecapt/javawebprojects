package jax_rs;

import models.Puntuacion;
import services.ServicesPuntuaciones;
import utils.Constants;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("puntuaciones")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Puntuaciones {

    @Inject
    private ServicesPuntuaciones sp;

    @GET
    public List<Puntuacion> getAllPuntuaciones() {
        return sp.getAllPuntuaciones();
    }

    @POST
    public Response savePuntuaciones(List<Puntuacion> puntuaciones, @Context HttpServletResponse resp) {
        resp.setCharacterEncoding(Constants.UTF_8);
        String response;
        if (sp.savePuntuaciones(puntuaciones)){
            response = "Las puntuaciones se han guardado con exito.";
        } else {
            response = "ERROR: No se ha podido añadir puntuaciones.";
        }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }
}
