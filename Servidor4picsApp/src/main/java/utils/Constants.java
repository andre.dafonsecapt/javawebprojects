package utils;

public class Constants {

    public Constants() {
    }

    public static final String ALTER_AutoIncrement_Puntuaciones_QUERY = "ALTER TABLE puntuaciones AUTO_INCREMENT = 1;";
    public static final String SELECT_Puntuaciones_QUERY = "SELECT * FROM puntuaciones;";
    public static final String INSERT_Puntuaciones_QUERY = "INSERT INTO puntuaciones (`Nombre`, `Dificultad`, `Tiempo`, `Score`) VALUES (?, ?, ?, ?);";
    public static final String DELETE_Puntuaciones_QUERY = "DELETE FROM puntuaciones WHERE `id` = ?;";


    public static final String UTF_8 = "UTF-8";
}
