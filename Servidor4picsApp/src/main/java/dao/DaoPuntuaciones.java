package dao;

import models.Puntuacion;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.Constants;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RequestScoped
@Named
public class DaoPuntuaciones {

    public DaoPuntuaciones() {
        // empty constructor
    }

    public List<Puntuacion> getAllPuntuaciones() {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.query(Constants.SELECT_Puntuaciones_QUERY,
                BeanPropertyRowMapper.newInstance(Puntuacion.class));
    }

    public boolean savePuntuaciones(List<Puntuacion> puntuaciones){
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        try {
            List<Puntuacion> mySqlPuntuaciones = jtm.query(Constants.SELECT_Puntuaciones_QUERY, BeanPropertyRowMapper.newInstance(Puntuacion.class));
            mySqlPuntuaciones.forEach(p -> jtm.update(Constants.DELETE_Puntuaciones_QUERY, p.getId()));
            jtm.execute(Constants.ALTER_AutoIncrement_Puntuaciones_QUERY);
            puntuaciones.forEach(p -> jtm.update(Constants.INSERT_Puntuaciones_QUERY,
                    p.getNombre(),
                    p.getDificultad(),
                    p.getTiempo(),
                    p.getScore()));
        } catch (Exception e){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            return false;
        }
        return true;
    }
}
