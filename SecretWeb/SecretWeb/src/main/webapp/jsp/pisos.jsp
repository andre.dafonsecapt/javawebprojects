<%-- 
    Document   : adminPisos
    Created on : 10-Oct-2019, 23:12:48
    Author     : andre
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pisos</title>
    </head>
    <body>
        <h1>Pisos</h1>
        <ul>
            <c:forEach items="${pisosCifrados}" var="piso">
                <li><c:out value="${piso}"/></li> 
            </c:forEach>
        </ul>
        <a href="/SecretWeb/index.html">volver</a>
    </body>
</html>
