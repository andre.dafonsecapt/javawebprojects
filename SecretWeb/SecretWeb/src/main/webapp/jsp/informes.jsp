<%-- 
    Document   : informes
    Created on : 15 oct. 2019, 9:13:48
    Author     : andre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Informes</title>
    </head>
    <body>
        <h1>Informes</h1>
        <form name="myform" action="SecretWeb/ServletInformes" method="POST">
            <select name="informeIndex">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            <br><br>
            <input type="submit">
            <br><br>
        </form>
        <p>Informe: <c:out value="${informe}"/></p>
        <a href="/SecretWeb/index.html">volver</a>
    </body>
</html>
