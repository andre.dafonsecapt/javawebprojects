<%-- 
    Document   : login
    Created on : 13-Oct-2019, 13:36:22
    Author     : andre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <form name="myform" action="/SecretWeb/login" method="POST">
            Introduz tu usuário: <input type="text" name="user"/><br>
            Introduz tu password: <input type="password" name="password"/><br>
            Introduz tu número de cifra cesar: <input type="number" name="numeroCesar"/><br>
            <input type="submit" value="Submit"/>
        </form>
        <p><c:out value="${error}"/></p>
    </body>
</html>
