<%-- 
    Document   : datosUsuario
    Created on : 11-Oct-2019, 17:46:37
    Author     : andre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador de pisos</title>
    </head>
    <body>
        <h1>Datos personales</h1>
        <p>Usuario: <c:out value="${usuario}"/></p>
        <ul>
            <li>Password: <c:out value="${password}"/></li>
            <li>Ubicacion: <c:out value="${ubicacion}"/></li> 
            <li>Edad: <c:out value="${edad}"/></li> 
        </ul>
        <a href="/SecretWeb/index.html">volver</a>
    </body>
</html>
