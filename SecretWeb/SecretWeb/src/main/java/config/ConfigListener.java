package config;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionListener;

@WebListener()
public class ConfigListener implements ServletContextListener,
    HttpSessionListener, HttpSessionAttributeListener {

  // Public constructor is required by servlet spec
  public ConfigListener() {
  }

  // -------------------------------------------------------
  // ServletContextListener implementation
  // -------------------------------------------------------
  public void contextInitialized(ServletContextEvent sce) {
      /* This method is called when the servlet context is
         initialized(when the Web application is deployed). 
         You can initialize servlet context related data here.
      */
    Configuration.getInstance(sce.getServletContext().getResourceAsStream("/WEB-INF/config/config.yaml"));
  }
}
