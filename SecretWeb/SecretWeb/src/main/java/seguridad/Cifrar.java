/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguridad;

import config.Configuration;

/**
 *
 * @author andre
 */
public class Cifrar {

    public String cifrarString(String s, int numeroCesar) {
        StringBuffer result = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            if (Character.isLetter(s.charAt(i))) {
                if (Character.isUpperCase(s.charAt(i))) {
                    char ch = (char) (((int) s.charAt(i) + numeroCesar - 65) % 26 + 65);
                    result.append(ch);
                } else {
                    char ch = (char) (((int) s.charAt(i) + numeroCesar - 97) % 26 + 97);
                    result.append(ch);
                }
            } else {
                char ch = (char) (((int) s.charAt(i)));
                result.append(ch);
            }
        }
        return result.toString();
    }

    public int descifrarInteger(int numeroIndex, int numeroCesar) {
        int informesListSize = Configuration.getInstance().getInformes().size();
        int numeroDescifrado = (numeroIndex - numeroCesar) % informesListSize;
        if (numeroDescifrado < 0) {
            numeroDescifrado = informesListSize + numeroDescifrado;
        }
        if (numeroDescifrado == 0) {
            numeroDescifrado = informesListSize;
        }
        return numeroDescifrado;
    }

}
