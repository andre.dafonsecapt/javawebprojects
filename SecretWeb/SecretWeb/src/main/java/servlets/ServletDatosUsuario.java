/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import config.Configuration;
import config.Usuario;
import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andre
 */
@WebServlet(name = "ServletDatosUsuario", urlPatterns = {"/ServletDatosUsuario"})
public class ServletDatosUsuario extends HttpServlet {

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        List<Usuario> usuarios = Configuration.getInstance().getUsuarios();
        String username = request.getSession().getAttribute("username").toString();
        Usuario usuarioTemp = usuarios.stream().filter((usuario) -> {
                return usuario.getUsername().equals(username);
            }).findAny().orElse(null);
        request.setAttribute("usuario", usuarioTemp);
        request.setAttribute("password", usuarioTemp.getPassword());
        request.setAttribute("ubicacion", usuarioTemp.getUbicacion());
        request.setAttribute("edad", usuarioTemp.getEdad());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
