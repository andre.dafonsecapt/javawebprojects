/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import config.Configuration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import config.Usuario;

@WebServlet(name = "ServletLogin", urlPatterns = "/login")
public class ServletLogin extends HttpServlet {

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<Usuario> usuarios = Configuration.getInstance().getUsuarios();
        Usuario usuarioLogin = new Usuario();
        usuarioLogin.setUsername(request.getParameter("user"));
        usuarioLogin.setPassword(request.getParameter("password"));

        if (usuarios.contains(usuarioLogin)) {
            Usuario usuarioTemp = usuarios.stream().filter((usuario) -> {
                return usuario.getUsername().equals(usuarioLogin.getUsername());
            }).findAny().orElse(null);
            if (usuarioLogin.getPassword().equals(usuarioTemp.getPassword())) {
                try {
                    if (null != request.getParameter("numeroCesar") && !request.getParameter("numeroCesar").isBlank()) {
                        request.getSession().setAttribute("username", usuarioLogin.getUsername());
                        request.getSession().setAttribute("numeroCesar", request.getParameter("numeroCesar"));
                        request.getSession().setAttribute("login", "OK");
                        request.getRequestDispatcher("index.html").forward(request, response);
                    } else {
                        request.setAttribute("error", "Error: Número de César inválido.");
                    }
                } catch (Exception e) {
                    request.setAttribute("error", "Error: Número de César inválido.");
                }
            } else {
                request.setAttribute("error", "Error: Password inválida.");
            }
        } else {
            request.setAttribute("error", "Error: Usuario inválido.");
        }
        request.getRequestDispatcher("jsp/login.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
