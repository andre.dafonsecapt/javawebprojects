/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filtros;

import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author andre
 */
@WebFilter(filterName = "FilterLogin", urlPatterns = {"/*"})
public class FilterLogin implements Filter {
    
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        try {
            HttpServletRequest request = (HttpServletRequest) req;
            boolean loggedIn = null != request.getSession().getAttribute("login") && request.getSession().getAttribute("login").equals("OK");
            boolean loginRequest = request.getRequestURI().equals(request.getContextPath() + "/login");
            if (loggedIn || loginRequest) {
                chain.doFilter(request, res);
            } else {
                request.getRequestDispatcher("jsp/login.jsp").forward(request, res);
            }
        } catch (Throwable t) {
            Logger.getLogger("Main").info(t.getMessage());
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
