/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filtros;

import config.Usuario;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import seguridad.Cifrar;

/**
 *
 * @author andre
 */
@WebFilter(filterName = "FilterCifrado", urlPatterns = {"/ServletDatosUsuario", "/ServletPisos", "/ServletInformes"})
public class FilterCifrado implements Filter {

    private void doBeforeProcessing(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (null != request.getParameter("informeIndex")) {
            Cifrar c = new Cifrar();
            int numeroCesar = Integer.parseInt(request.getSession().getAttribute("numeroCesar").toString());
            int informeIndexDescifrado = c.descifrarInteger(Integer.parseInt(request.getParameter("informeIndex")), numeroCesar);
            request.setAttribute("informeIndexDescifrado", informeIndexDescifrado);
        }
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        doBeforeProcessing((HttpServletRequest) req,(HttpServletResponse) resp);

        chain.doFilter(req, resp);

        doAfterProcessing((HttpServletRequest) req, (HttpServletResponse) resp);
    }

    private void doAfterProcessing(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cifrar c = new Cifrar();
        int numeroCesar = Integer.parseInt(request.getSession().getAttribute("numeroCesar").toString());

        if (null != request.getAttribute("usuario")) {
            Usuario usuarioTemp = (Usuario) request.getAttribute("usuario");
            String usernameCifrado = c.cifrarString(usuarioTemp.getUsername(), numeroCesar);
            String passwordCifrada = c.cifrarString(usuarioTemp.getPassword(), numeroCesar);
            String ubicacionCifrada = c.cifrarString(usuarioTemp.getUbicacion(), numeroCesar);
            String edadCifrada = c.cifrarString(Integer.toString(usuarioTemp.getEdad()), numeroCesar);
            request.setAttribute("usuario", usernameCifrado);
            request.setAttribute("password", passwordCifrada);
            request.setAttribute("ubicacion", ubicacionCifrada);
            request.setAttribute("edad", edadCifrada);
            request.getRequestDispatcher("jsp/datosUsuario.jsp").forward(request, response);
        }

        if (null != request.getAttribute("pisos")) {
            List<String> pisosList = (List<String>) request.getAttribute("pisos");
            List<String> pisosCifrados = new ArrayList<>();
            pisosList.stream().forEach((piso) -> {
                String pisoCifrado = c.cifrarString(piso, numeroCesar);
                pisosCifrados.add(pisoCifrado);
            });
            request.setAttribute("pisosCifrados", pisosCifrados);
            request.getRequestDispatcher("jsp/pisos.jsp").forward(request, response);
        }
        
        if (null != request.getAttribute("informes") && null != request.getAttribute("informeIndexDescifrado")){
             List<String> informes = (List<String>)request.getAttribute("informes");
             int informeIndexDescifrado = Integer.parseInt(request.getAttribute("informeIndexDescifrado").toString());
             request.setAttribute("informe", informes.get(informeIndexDescifrado-1));
             request.getRequestDispatcher("jsp/informes.jsp").forward(request, response);
        }

    }

    @Override
    public void init(FilterConfig config) throws ServletException {

    }

    @Override
    public void destroy() {
    }

}
