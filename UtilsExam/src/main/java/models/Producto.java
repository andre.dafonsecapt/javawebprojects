package models;

import java.util.Objects;

public class Producto {


  private String nombre;
  private String faccion;
  private double precio;


  public Producto(String nombre, String faccion, double precio) {
    this.nombre = nombre;
    this.faccion = faccion;
    this.precio = precio;
  }

  public Producto() {
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getFaccion() {
    return faccion;
  }

  public void setFaccion(String faccion) {
    this.faccion = faccion;
  }

  public double getPrecio() {
    return precio;
  }

  public void setPrecio(double precio) {
    this.precio = precio;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Producto producto = (Producto) o;
    return getNombre().equals(producto.getNombre());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getNombre());
  }

  @Override
  public String toString() {
    return "Producto{" +
        "nombre='" + nombre + '\'' +
        ", faccion='" + faccion + '\'' +
        ", precio=" + precio +
        '}';
  }
}
