package controllers;

import fj.data.Either;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import models.ApiError;
import models.MusicaGetAndPostDTO;
import models.MusicaLiveGetDTO;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLMusicaController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    @FXML
    private ListView<MusicaGetAndPostDTO> fxListMusicas;

    @FXML
    private ListView<String> fxListLive;

    @FXML
    private TextField tfNombre;

    @FXML
    private TextField tfArtista;

    @FXML
    private DatePicker dpFecha;

    @FXML
    private TextField fxTxtBusqueda;

    private Alert alert;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
    }

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    public void clearFields() {
        tfNombre.clear();
        tfArtista.clear();
        fxTxtBusqueda.clear();
        dpFecha.setValue(null);
        fxListMusicas.getItems().clear();
    }

    public void fxAddMusica() {
        Task<Either<ApiError, String>> tarea = new Task<Either<ApiError, String>>() {

            @Override
            protected Either<ApiError, String> call() throws Exception {
                tfNombre.getScene().setCursor(Cursor.WAIT);
                return mainController.getMusicaServices().addMusica(new MusicaGetAndPostDTO(0, tfNombre.getText(), tfArtista.getText(), dpFecha.valueProperty().getValue(), LocalDateTime.now()));
            }
        };

        tarea.setOnSucceeded(cadena -> {
                    if (tarea.getValue().isRight()) {
                        alert.setContentText(tarea.getValue().right().value());
                    } else {
                        alert.setContentText(tarea.getValue().left().value().getMessage());
                    }
                    alert.showAndWait();
                    clearFields();
                    loadMusicas();
                    tfNombre.getScene().setCursor(Cursor.DEFAULT);
                }
        );
        tarea.setOnFailed(workerStateEvent -> {
            Logger.getLogger("FXMAIN")
                    .log(Level.SEVERE, "error ",
                            workerStateEvent.getSource().getException());
            tfNombre.getScene().setCursor(Cursor.DEFAULT);
        });
        new Thread(tarea).start();
    }

    public void fxUpdateMusica() {
        if (fxListMusicas.getSelectionModel().getSelectedItem() != null) {

            Task<Either<ApiError, String>> tarea = new Task<Either<ApiError, String>>() {

                @Override
                protected Either<ApiError, String> call() throws Exception {
                    tfNombre.getScene().setCursor(Cursor.WAIT);
                    MusicaGetAndPostDTO m = new MusicaGetAndPostDTO(0, tfNombre.getText(), tfArtista.getText(), dpFecha.valueProperty().getValue(), LocalDateTime.now());
                    m.setIdMusica((fxListMusicas.getSelectionModel().getSelectedItem()).getIdMusica());
                    return mainController.getMusicaServices().updateMusica(m);
                }
            };

            tarea.setOnSucceeded(cadena -> {
                        if (tarea.getValue().isRight()) {
                            alert.setContentText(tarea.getValue().right().value());
                        } else {
                            alert.setContentText(tarea.getValue().left().value().getMessage());
                        }
                        alert.showAndWait();
                        clearFields();
                        loadMusicas();
                        tfNombre.getScene().setCursor(Cursor.DEFAULT);
                    }
            );
            tarea.setOnFailed(workerStateEvent -> {
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException());
                tfNombre.getScene().setCursor(Cursor.DEFAULT);
            });
            new Thread(tarea).start();
        } else {
            alert.setContentText("Error - Selecciona una Música primero.");
            alert.showAndWait();
        }
    }

    public void fxBorrarMusica() {

        if (fxListMusicas.getSelectionModel().getSelectedItem() != null) {

            Task<Either<ApiError, String>> tarea = new Task<Either<ApiError, String>>() {

                @Override
                protected Either<ApiError, String> call() throws Exception {
                    tfNombre.getScene().setCursor(Cursor.WAIT);
                    MusicaGetAndPostDTO m = fxListMusicas.getSelectionModel().getSelectedItem();
                    return mainController.getMusicaServices().deleteMusica(m.getIdMusica());
                }
            };

            tarea.setOnSucceeded(cadena -> {
                        if (tarea.getValue().isRight()) {
                            alert.setContentText(tarea.getValue().right().value());
                        } else {
                            alert.setContentText(tarea.getValue().left().value().getMessage());
                        }
                        alert.showAndWait();
                        clearFields();
                        loadMusicas();
                        tfNombre.getScene().setCursor(Cursor.DEFAULT);
                    }
            );
            tarea.setOnFailed(workerStateEvent -> {
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException());
                tfNombre.getScene().setCursor(Cursor.DEFAULT);
            });
            new Thread(tarea).start();

        } else {
            alert.setContentText("Error - Selecciona una Música primero.");
            alert.showAndWait();
        }
    }

    private void loadMusicas() {
        Task<Either<ApiError, List<MusicaGetAndPostDTO>>> tarea = new Task<Either<ApiError, List<MusicaGetAndPostDTO>>>() {

            @Override
            protected Either<ApiError, List<MusicaGetAndPostDTO>> call() throws Exception {
                tfNombre.getScene().setCursor(Cursor.WAIT);
                return mainController.getMusicaServices().getMusicas();
            }
        };
        tarea.setOnSucceeded(cadena -> {
            if (tarea.getValue().isRight()) {
                fxListMusicas.getItems().clear();
                fxListMusicas.getItems().addAll(tarea.getValue().right().value());
            } else {
                alert.setContentText(tarea.getValue().left().value().getMessage());
                alert.showAndWait();
            }
            tfNombre.getScene().setCursor(Cursor.DEFAULT);
        });
        tarea.setOnFailed(workerStateEvent -> {
            Logger.getLogger("FXMAIN")
                    .log(Level.SEVERE, "error ",
                            workerStateEvent.getSource().getException());
            tfNombre.getScene().setCursor(Cursor.DEFAULT);
        });
        new Thread(tarea).start();
    }

    public void buscarMusicaPorID() {

        try {
            if (fxTxtBusqueda.getText().equals("")) {

                Task<Either<ApiError, List<MusicaGetAndPostDTO>>> tarea = new Task<Either<ApiError, List<MusicaGetAndPostDTO>>>() {

                    @Override
                    protected Either<ApiError, List<MusicaGetAndPostDTO>> call() throws Exception {
                        tfNombre.getScene().setCursor(Cursor.WAIT);
                        return mainController.getMusicaServices().getMusicas();
                    }
                };

                tarea.setOnSucceeded(cadena -> {
                    if (tarea.getValue().isRight()) {
                        fxListMusicas.getItems().clear();
                        fxListMusicas.getItems().addAll(tarea.getValue().right().value());
                    } else {
                        alert.setContentText(tarea.getValue().left().value().getMessage());
                        alert.showAndWait();
                    }
                    tfNombre.getScene().setCursor(Cursor.DEFAULT);
                });
                tarea.setOnFailed(workerStateEvent -> {
                    Logger.getLogger("FXMAIN")
                            .log(Level.SEVERE, "error ",
                                    workerStateEvent.getSource().getException());
                    tfNombre.getScene().setCursor(Cursor.DEFAULT);
                });
                new Thread(tarea).start();

            } else {
                int idMusica = Integer.parseInt(fxTxtBusqueda.getText());

                Task<Either<ApiError, MusicaGetAndPostDTO>> tarea = new Task<Either<ApiError, MusicaGetAndPostDTO>>() {

                    @Override
                    protected Either<ApiError, MusicaGetAndPostDTO> call() throws Exception {
                        tfNombre.getScene().setCursor(Cursor.WAIT);
                        return mainController.getMusicaServices().getMusica(idMusica);
                    }
                };

                tarea.setOnSucceeded(cadena -> {
                    if (tarea.getValue().isRight()) {
                        fxListMusicas.getItems().clear();
                        fxListMusicas.getItems().add(tarea.getValue().right().value());
                    } else {
                        alert.setContentText(tarea.getValue().left().value().getMessage());
                        alert.showAndWait();
                    }
                    tfNombre.getScene().setCursor(Cursor.DEFAULT);
                });
                tarea.setOnFailed(workerStateEvent -> {
                    Logger.getLogger("FXMAIN")
                            .log(Level.SEVERE, "error ",
                                    workerStateEvent.getSource().getException());
                    tfNombre.getScene().setCursor(Cursor.DEFAULT);
                });
                new Thread(tarea).start();
            }
        } catch (Exception e) {
            alert.setContentText("Error - Formato inválido de ID");
            alert.showAndWait();
        }

    }

    public void setListeners() {
        fxListMusicas.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                tfNombre.setText(newValue.getNombre());
                tfArtista.setText(newValue.getArtista());
                dpFecha.valueProperty().setValue(newValue.getFechaGrabacion());

                Task<Either<ApiError, MusicaLiveGetDTO>> tarea = new Task<Either<ApiError, MusicaLiveGetDTO>>() {

                    @Override
                    protected Either<ApiError, MusicaLiveGetDTO> call() throws Exception {
                        tfNombre.getScene().setCursor(Cursor.WAIT);
                        return mainController.getMusicaServices().getLiveActs(newValue.getIdMusica());
                    }
                };

                tarea.setOnSucceeded(cadena -> {
                    if (tarea.getValue().isRight()) {
                        fxListLive.getItems().clear();
                        MusicaLiveGetDTO m = tarea.getValue().right().value();
                        fxListLive.getItems().addAll(m.getLiveActs());
                    } else {
                        alert.setContentText(tarea.getValue().left().value().getMessage());
                        alert.showAndWait();
                    }
                    tfNombre.getScene().setCursor(Cursor.DEFAULT);
                });
                tarea.setOnFailed(workerStateEvent -> {
                    Logger.getLogger("FXMAIN")
                            .log(Level.SEVERE, "error ",
                                    workerStateEvent.getSource().getException());
                    tfNombre.getScene().setCursor(Cursor.DEFAULT);
                });
                new Thread(tarea).start();
            }
        });
    }

}
