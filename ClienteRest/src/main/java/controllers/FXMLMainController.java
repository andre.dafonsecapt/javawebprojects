package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import models.Agenda;
import services.*;
import utils.Constants;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLMainController implements Initializable {

    @FXML
    private BorderPane fxRoot;

    @FXML
    private Menu fxMenuUsuario;

    @FXML
    private Menu fxMenuMusica;

    @FXML
    private Menu fxMenuAgenda;

    @FXML
    private MenuItem fxMenuItemLogout;

    @FXML
    private MenuItem fxMenuItemLogin;

    private ClientServices clientServices;
    private MusicaServices musicaServices;
    private UsuarioServices usuarioServices;
    private AgendaServices agendaServices;
    private InvitacionServices invitacionServices;
    private AnchorPane pagLogin;
    private FXMLLoginController loginController;
    private AnchorPane pagUsuarios;
    private FXMLUsuariosController usuariosController;
    private AnchorPane pagMusica;
    private FXMLMusicaController musicaController;
    private AnchorPane pagAgenda;
    private FXMLAgendaController agendaController;
    private AnchorPane pagInvitaciones;
    private FXMLInvitacionesController invitacionesController;
    private String username;

    ClientServices getClientServices() {
        return clientServices;
    }

    public MusicaServices getMusicaServices() {
        return musicaServices;
    }

    public UsuarioServices getUsuarioServices() {
        return usuarioServices;
    }

    public AgendaServices getAgendaServices(){return agendaServices;}

    public InvitacionServices getInvitacionServices() {
        return invitacionServices;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        clientServices = new ClientServices();
        musicaServices = new MusicaServices();
        usuarioServices = new UsuarioServices();
        agendaServices = new AgendaServices();
        invitacionServices = new InvitacionServices();
        menuLogin();

    }

    void enableMenu(String username) {
        fxMenuUsuario.setDisable(false);
        fxMenuMusica.setDisable(false);
        fxMenuAgenda.setDisable(false);
        fxMenuItemLogin.setDisable(true);
        fxMenuItemLogout.setDisable(false);
        this.username = username;
    }

    @FXML
    private void menuLogin() {

        try {
            if (pagLogin == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(Constants.FXML_LOGIN));
                pagLogin = loaderMenu.load();
                loginController = loaderMenu.getController();
                loginController.setBorderPane(this);
            }
            loginController.enableFields();
            fxRoot.setCenter(pagLogin);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void logout() {
        Alert alertConfirm = new Alert(Alert.AlertType.CONFIRMATION);
        alertConfirm.setTitle("Confirmation Dialog");
        alertConfirm.setHeaderText(null);
        ButtonType buttonTypeYes = new ButtonType("Yes");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alertConfirm.getButtonTypes().setAll(buttonTypeYes, buttonTypeCancel);
        alertConfirm.setContentText("Are you sure you want to logout?");
        Optional<ButtonType> result = alertConfirm.showAndWait();
        if (result.isPresent() && result.get() == buttonTypeYes) {
            fxMenuUsuario.setDisable(true);
            fxMenuMusica.setDisable(true);
            fxMenuAgenda.setDisable(true);
            fxMenuItemLogin.setDisable(false);
            fxMenuItemLogout.setDisable(true);
            menuLogin();
        }
    }

    @FXML
    private void menuGestionUsuario() {
        try {
            if (pagUsuarios == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(Constants.FXML_GESTION_USUARIO));
                pagUsuarios = loaderMenu.load();
                usuariosController = loaderMenu.getController();
                usuariosController.setBorderPane(this);
            }
            usuariosController.setListeners();
            usuariosController.clearFields();
            fxRoot.setCenter(pagUsuarios);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void menuGestionMusica() {
        try {
            if (pagMusica == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(Constants.FXML_GESTION_MUSICA));
                pagMusica = loaderMenu.load();
                musicaController = loaderMenu.getController();
                musicaController.setBorderPane(this);
            }
            musicaController.setListeners();
            musicaController.clearFields();
            fxRoot.setCenter(pagMusica);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void menuGestionAgenda() {
        try {
            if (pagAgenda == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(Constants.FXML_GESTION_AGENDA));
                pagAgenda = loaderMenu.load();
                agendaController = loaderMenu.getController();
                agendaController.setBorderPane(this);
            }
            agendaController.setListeners();
            agendaController.clearFields();
            fxRoot.setCenter(pagAgenda);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void menuInvitacionesAgenda() {
        try {
            if (pagInvitaciones == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(Constants.FXML_INVITACIONES_AGENDA));
                pagInvitaciones = loaderMenu.load();
                invitacionesController = loaderMenu.getController();
                invitacionesController.setBorderPane(this);
            }
            invitacionesController.clearFields();
            invitacionesController.loadComboBoxes(usuarioServices.getUsuarios().right().value(), agendaServices.getAgendas(username).right().value());
            fxRoot.setCenter(pagInvitaciones);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
