package controllers;

import fj.data.Either;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import jfxtras.scene.control.LocalDateTimeTextField;
import models.Agenda;
import models.ApiError;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLAgendaController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    @FXML
    private ListView<Agenda> fxListAgendas;

    @FXML
    private TextField tfTitulo;

    @FXML
    private Spinner<Integer> spinnerDuracion;

    @FXML
    private LocalDateTimeTextField dpFecha;

    private Alert alert;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
    }

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    public void clearFields() {
        tfTitulo.clear();
        spinnerDuracion.getValueFactory().setValue(0);
        dpFecha.setLocalDateTime(null);
        fxListAgendas.getItems().clear();
    }

    public void fxAddAgenda() {
        Task<Either<ApiError, String>> tarea = new Task<Either<ApiError, String>>() {

            @Override
            protected Either<ApiError, String> call() throws Exception {
                tfTitulo.getScene().setCursor(Cursor.WAIT);
                Agenda agenda = new Agenda();
                agenda.setTitulo(tfTitulo.getText());
                agenda.setFecha(dpFecha.getLocalDateTime());
                agenda.setDuracion(spinnerDuracion.getValue());
                return mainController.getAgendaServices().addAgenda(agenda, mainController.getUsername());
            }
        };

        tarea.setOnSucceeded(cadena -> {
                    if (tarea.getValue().isRight()) {
                        alert.setContentText(tarea.getValue().right().value());
                    } else {
                        alert.setContentText(tarea.getValue().left().value().getMessage());
                    }
                    alert.showAndWait();
                    clearFields();
                    buscarAgendas();
                    tfTitulo.getScene().setCursor(Cursor.DEFAULT);
                }
        );
        tarea.setOnFailed(workerStateEvent -> {
            Logger.getLogger("FXMAIN")
                    .log(Level.SEVERE, "error ",
                            workerStateEvent.getSource().getException());
            tfTitulo.getScene().setCursor(Cursor.DEFAULT);
        });
        new Thread(tarea).start();
    }

    public void fxUpdateAgenda() {
        if (fxListAgendas.getSelectionModel().getSelectedItem() != null) {

            Task<Either<ApiError, String>> tarea = new Task<Either<ApiError, String>>() {

                @Override
                protected Either<ApiError, String> call() throws Exception {
                    tfTitulo.getScene().setCursor(Cursor.WAIT);
                    Agenda agenda = new Agenda();
                    agenda.setTitulo(tfTitulo.getText());
                    agenda.setFecha(dpFecha.getLocalDateTime());
                    agenda.setDuracion(spinnerDuracion.getValue());
                    agenda.setId((fxListAgendas.getSelectionModel().getSelectedItem()).getId());
                    return mainController.getAgendaServices().updateAgenda(agenda, mainController.getUsername());
                }
            };

            tarea.setOnSucceeded(cadena -> {
                        if (tarea.getValue().isRight()) {
                            alert.setContentText(tarea.getValue().right().value());
                        } else {
                            alert.setContentText(tarea.getValue().left().value().getMessage());
                        }
                        alert.showAndWait();
                        clearFields();
                        buscarAgendas();
                        tfTitulo.getScene().setCursor(Cursor.DEFAULT);
                    }
            );
            tarea.setOnFailed(workerStateEvent -> {
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException());
                tfTitulo.getScene().setCursor(Cursor.DEFAULT);
            });
            new Thread(tarea).start();
        } else {
            alert.setContentText("Error - Selecciona una Agenda primero.");
            alert.showAndWait();
        }
    }

    public void fxBorrarAgenda() {

        if (fxListAgendas.getSelectionModel().getSelectedItem() != null) {

            Task<Either<ApiError, String>> tarea = new Task<Either<ApiError, String>>() {

                @Override
                protected Either<ApiError, String> call() throws Exception {
                    tfTitulo.getScene().setCursor(Cursor.WAIT);
                    Agenda a = fxListAgendas.getSelectionModel().getSelectedItem();
                    return mainController.getAgendaServices().deleteAgenda(a.getId());
                }
            };

            tarea.setOnSucceeded(cadena -> {
                        if (tarea.getValue().isRight()) {
                            alert.setContentText(tarea.getValue().right().value());
                        } else {
                            alert.setContentText(tarea.getValue().left().value().getMessage());
                        }
                        alert.showAndWait();
                        clearFields();
                        buscarAgendas();
                        tfTitulo.getScene().setCursor(Cursor.DEFAULT);
                    }
            );
            tarea.setOnFailed(workerStateEvent -> {
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException());
                tfTitulo.getScene().setCursor(Cursor.DEFAULT);
            });
            new Thread(tarea).start();

        } else {
            alert.setContentText("Error - Selecciona una Agenda primero.");
            alert.showAndWait();
        }
    }

    public void buscarAgendas() {

        Task<Either<ApiError, List<Agenda>>> tarea = new Task<Either<ApiError, List<Agenda>>>() {

            @Override
            protected Either<ApiError, List<Agenda>> call() throws Exception {
                tfTitulo.getScene().setCursor(Cursor.WAIT);
                return mainController.getAgendaServices().getAgendas(mainController.getUsername());
            }
        };

        tarea.setOnSucceeded(cadena -> {
            if (tarea.getValue().isRight()) {
                fxListAgendas.getItems().clear();
                if ((tarea.getValue().right().value().isEmpty())) {
                    alert.setContentText("Este usuario no tiene agendas.");
                    alert.showAndWait();
                } else {
                    fxListAgendas.getItems().addAll(tarea.getValue().right().value());
                }
            } else {
                alert.setContentText(tarea.getValue().left().value().getMessage());
                alert.showAndWait();
            }
            tfTitulo.getScene().setCursor(Cursor.DEFAULT);
        });
        tarea.setOnFailed(workerStateEvent -> {
            Logger.getLogger("FXMAIN")
                    .log(Level.SEVERE, "error ",
                            workerStateEvent.getSource().getException());
            tfTitulo.getScene().setCursor(Cursor.DEFAULT);
        });
        new Thread(tarea).start();
    }

    public void setListeners() {
        fxListAgendas.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                tfTitulo.setText(newValue.getTitulo());
                spinnerDuracion.getValueFactory().setValue(newValue.getDuracion());
                dpFecha.setLocalDateTime(newValue.getFecha());
            }
        });
    }
}
