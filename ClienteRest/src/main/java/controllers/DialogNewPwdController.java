/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import fj.data.Either;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.ApiError;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author andre
 */
public class DialogNewPwdController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    private Alert alert;

    @FXML
    private TextField tfEmail;

    @FXML
    private Button fxButton;

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
    }

    @FXML
    public void sendPwd(ActionEvent event) {

        if (!tfEmail.getText().equals("")) {

            Task<Either<ApiError, String>> tarea = new Task<Either<ApiError, String>>() {

                @Override
                protected Either<ApiError, String> call() throws Exception {
                    tfEmail.getScene().setCursor(Cursor.WAIT);
                    return mainController.getClientServices().requestPwd(tfEmail.getText());
                }
            };

            tarea.setOnSucceeded(cadena -> {
                        if (tarea.getValue().isRight()) {
                            alert.setContentText(tarea.getValue().right().value());
                        } else {
                            alert.setContentText(tarea.getValue().left().value().toString());
                        }
                        alert.showAndWait();
                        tfEmail.getScene().setCursor(Cursor.DEFAULT);
                        closeStage(event);
                    }
            );
            tarea.setOnFailed(workerStateEvent -> {
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException());
                tfEmail.getScene().setCursor(Cursor.DEFAULT);
                closeStage(event);
            });
            new Thread(tarea).start();

        } else {
            alert.setContentText("Error - Campo de email vacio.");
            alert.showAndWait();
        }
    }


    @FXML
    public void changeCursorHand() {
        fxButton.getScene().setCursor(Cursor.HAND);
    }

    @FXML
    public void changeCursorArrow() {
        fxButton.getScene().setCursor(Cursor.DEFAULT);
    }

    private void closeStage(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
