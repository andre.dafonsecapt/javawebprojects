package controllers;

import fj.data.Either;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import models.ApiError;
import models.UsuarioGetDTO;
import models.UsuarioPostDTO;
import services.EncryptionServices;

import java.net.URL;
import java.util.Base64;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLUsuariosController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    @FXML
    private ListView<UsuarioGetDTO> fxListViewUsuarios;

    @FXML
    private TextField tfUsername;

    @FXML
    private TextField tfPassword;

    @FXML
    private TextField fxTxtBusqueda;

    private Alert alert;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
    }

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    public void clearFields() {
        tfUsername.clear();
        tfPassword.clear();
        fxTxtBusqueda.clear();
        fxListViewUsuarios.getItems().clear();
    }

    public void fxAddUsuario() {
        if (!tfUsername.getText().equals("") && !tfPassword.getText().equals("")) {

            Task<Either<ApiError, String>> tarea = new Task<Either<ApiError, String>>() {

                @Override
                protected Either<ApiError, String> call() throws Exception {
                    tfUsername.getScene().setCursor(Cursor.WAIT);
                    UsuarioPostDTO u = new UsuarioPostDTO();
                    u.setUsername(tfUsername.getText());
                    u.setPassword(tfPassword.getText());
                    EncryptionServices.crearKeys(tfUsername.getText());
                    u.setClavePublica(Base64.getUrlEncoder().encodeToString(EncryptionServices.getPublicKey(tfUsername.getText()).getEncoded()));
                    return mainController.getUsuarioServices().addUsuario(u);
                }
            };

            tarea.setOnSucceeded(cadena -> {
                        if (tarea.getValue().isRight()) {
                            alert.setContentText(tarea.getValue().right().value());
                            clearFields();
                            loadUsuarios();
                        } else {
                            alert.setContentText(tarea.getValue().left().value().getMessage());
                        }
                        alert.showAndWait();
                        tfUsername.getScene().setCursor(Cursor.DEFAULT);
                    }
            );
            tarea.setOnFailed(workerStateEvent -> {
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException());
                tfUsername.getScene().setCursor(Cursor.DEFAULT);
            });
            new Thread(tarea).start();

        } else {
            alert.setContentText("Error - Uno o más campos vacios.");
            alert.showAndWait();
        }
    }

    public void fxUpdateUsuario() {
        if (fxListViewUsuarios.getSelectionModel().getSelectedItem() != null) {
            if (!tfUsername.getText().equals("")) {

                Task<Either<ApiError, String>> tarea = new Task<Either<ApiError, String>>() {

                    @Override
                    protected Either<ApiError, String> call() throws Exception {
                        tfUsername.getScene().setCursor(Cursor.WAIT);
                        UsuarioPostDTO u = new UsuarioPostDTO(0, tfUsername.getText(), tfPassword.getText(), null);
                        u.setIdUsuario((fxListViewUsuarios.getSelectionModel().getSelectedItem()).getIdUsuario());
                        return mainController.getUsuarioServices().updateUsuario(u);
                    }
                };

                tarea.setOnSucceeded(cadena -> {
                            if (tarea.getValue().isRight()) {
                                alert.setContentText(tarea.getValue().right().value());
                                clearFields();
                                loadUsuarios();
                            } else {
                                alert.setContentText(tarea.getValue().left().value().getMessage());
                            }
                            alert.showAndWait();
                            tfUsername.getScene().setCursor(Cursor.DEFAULT);
                        }
                );
                tarea.setOnFailed(workerStateEvent -> {
                    Logger.getLogger("FXMAIN")
                            .log(Level.SEVERE, "error ",
                                    workerStateEvent.getSource().getException());
                    tfUsername.getScene().setCursor(Cursor.DEFAULT);
                });
                new Thread(tarea).start();

            } else {
                alert.setContentText("Error - El campo de Usuario no puede estar vacio.");
                alert.showAndWait();
            }
        } else {
            alert.setContentText("Error - Selecciona un Usuario primero.");
            alert.showAndWait();
        }
    }

    public void fxBorrarUsuario() {

        if (fxListViewUsuarios.getSelectionModel().getSelectedItem() != null) {

            Task<Either<ApiError, String>> tarea = new Task<Either<ApiError, String>>() {

                @Override
                protected Either<ApiError, String> call() throws Exception {
                    tfUsername.getScene().setCursor(Cursor.WAIT);
                    UsuarioGetDTO u = fxListViewUsuarios.getSelectionModel().getSelectedItem();
                    return mainController.getUsuarioServices().deleteUsuario(u.getIdUsuario());
                }
            };

            tarea.setOnSucceeded(cadena -> {
                        if (tarea.getValue().isRight()) {
                            alert.setContentText(tarea.getValue().right().value());
                            loadUsuarios();
                            clearFields();
                        } else {
                            alert.setContentText(tarea.getValue().left().value().getMessage());
                        }
                        alert.showAndWait();
                        tfUsername.getScene().setCursor(Cursor.DEFAULT);
                    }
            );
            tarea.setOnFailed(workerStateEvent -> {
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException());
                tfUsername.getScene().setCursor(Cursor.DEFAULT);
            });
            new Thread(tarea).start();

        } else {
            alert.setContentText("Error - Selecciona un Usuario primero.");
            alert.showAndWait();
        }
    }

    private void loadUsuarios() {
        Task<Either<ApiError, List<UsuarioGetDTO>>> tarea = new Task<Either<ApiError, List<UsuarioGetDTO>>>() {

            @Override
            protected Either<ApiError, List<UsuarioGetDTO>> call() throws Exception {
                tfUsername.getScene().setCursor(Cursor.WAIT);
                return mainController.getUsuarioServices().getUsuarios();
            }
        };
        tarea.setOnSucceeded(cadena -> {
            if (tarea.getValue().isRight()) {
                fxListViewUsuarios.getItems().clear();
                fxListViewUsuarios.getItems().addAll(tarea.getValue().right().value());
            } else {
                alert.setContentText(tarea.getValue().left().value().getMessage());
                alert.showAndWait();
            }
            tfUsername.getScene().setCursor(Cursor.DEFAULT);
        });
        tarea.setOnFailed(workerStateEvent -> {
            Logger.getLogger("FXMAIN")
                    .log(Level.SEVERE, "error ",
                            workerStateEvent.getSource().getException());
            tfUsername.getScene().setCursor(Cursor.DEFAULT);
        });
        new Thread(tarea).start();
    }

    public void buscarUsuarioPorID() {

        try {
            if (fxTxtBusqueda.getText().equals("")) {

                Task<Either<ApiError, List<UsuarioGetDTO>>> tarea = new Task<Either<ApiError, List<UsuarioGetDTO>>>() {

                    @Override
                    protected Either<ApiError, List<UsuarioGetDTO>> call() throws Exception {
                        tfUsername.getScene().setCursor(Cursor.WAIT);
                        return mainController.getUsuarioServices().getUsuarios();
                    }
                };

                tarea.setOnSucceeded(cadena -> {
                    if (tarea.getValue().isRight()) {
                        fxListViewUsuarios.getItems().clear();
                        fxListViewUsuarios.getItems().addAll(tarea.getValue().right().value());
                    } else {
                        alert.setContentText(tarea.getValue().left().value().getMessage());
                        alert.showAndWait();
                    }
                    tfUsername.getScene().setCursor(Cursor.DEFAULT);
                });
                tarea.setOnFailed(workerStateEvent -> {
                    Logger.getLogger("FXMAIN")
                            .log(Level.SEVERE, "error ",
                                    workerStateEvent.getSource().getException());
                    tfUsername.getScene().setCursor(Cursor.DEFAULT);
                });
                new Thread(tarea).start();

            } else {
                int idUsuario = Integer.parseInt(fxTxtBusqueda.getText());

                Task<Either<ApiError, UsuarioGetDTO>> tarea = new Task<Either<ApiError, UsuarioGetDTO>>() {

                    @Override
                    protected Either<ApiError, UsuarioGetDTO> call() throws Exception {
                        tfUsername.getScene().setCursor(Cursor.WAIT);
                        return mainController.getUsuarioServices().getUsuario(idUsuario);
                    }
                };

                tarea.setOnSucceeded(cadena -> {
                    if (tarea.getValue().isRight()) {
                        fxListViewUsuarios.getItems().clear();
                        fxListViewUsuarios.getItems().add(tarea.getValue().right().value());
                    } else {
                        alert.setContentText(tarea.getValue().left().value().getMessage());
                        alert.showAndWait();
                    }
                    tfUsername.getScene().setCursor(Cursor.DEFAULT);
                });
                tarea.setOnFailed(workerStateEvent -> {
                    Logger.getLogger("FXMAIN")
                            .log(Level.SEVERE, "error ",
                                    workerStateEvent.getSource().getException());
                    tfUsername.getScene().setCursor(Cursor.DEFAULT);
                });
                new Thread(tarea).start();
            }
        } catch (Exception e) {
            alert.setContentText("Error - Formato inválido de ID");
            alert.showAndWait();
        }

    }

    public void setListeners() {
        fxListViewUsuarios.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                tfUsername.setText(newValue.getUsername());
            }
        });
    }

}
