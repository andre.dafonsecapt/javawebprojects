package controllers;

import fj.data.Either;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import models.Agenda;
import models.ApiError;
import models.UsuarioGetDTO;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLInvitacionesController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    @FXML
    private ListView fxList;

    @FXML
    private ComboBox<UsuarioGetDTO> fxComboUsuarios;

    @FXML
    private ComboBox<Agenda> fxComboAgendas;

    private Alert alert;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
    }

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    public void clearFields() {
        fxComboUsuarios.getSelectionModel().clearSelection();
        fxComboAgendas.getSelectionModel().clearSelection();
        fxList.getItems().clear();
    }

    public void loadComboBoxes(List<UsuarioGetDTO> usuarios, List<Agenda> agendas) {
        fxComboUsuarios.getItems().clear();
        fxComboUsuarios.getItems().addAll(usuarios);
        fxComboAgendas.getItems().clear();
        fxComboAgendas.getItems().addAll(agendas);
    }

    public void fxVerInvitaciones() {

        Task<Either<ApiError, List<Agenda>>> tarea = new Task<Either<ApiError, List<Agenda>>>() {

            @Override
            protected Either<ApiError, List<Agenda>> call() throws Exception {
                fxComboUsuarios.getScene().setCursor(Cursor.WAIT);
                return mainController.getInvitacionServices().getAgendas(mainController.getUsername());
            }
        };

        tarea.setOnSucceeded(cadena -> {
            if (tarea.getValue().isRight()) {
                fxList.getItems().clear();
                if ((tarea.getValue().right().value().isEmpty())) {
                    alert.setContentText("Este usuario no tiene invitaciones.");
                    alert.showAndWait();
                } else {
                    fxList.getItems().addAll(tarea.getValue().right().value());
                }
            } else {
                alert.setContentText(tarea.getValue().left().value().getMessage());
                alert.showAndWait();
            }
            fxComboUsuarios.getScene().setCursor(Cursor.DEFAULT);
        });
        tarea.setOnFailed(workerStateEvent -> {
            Logger.getLogger("FXMAIN")
                    .log(Level.SEVERE, "error ",
                            workerStateEvent.getSource().getException());
            fxComboUsuarios.getScene().setCursor(Cursor.DEFAULT);
        });
        new Thread(tarea).start();
    }

    public void fxInvitar() {

        if (fxComboUsuarios.getSelectionModel().getSelectedItem() != null && fxComboAgendas.getSelectionModel().getSelectedItem() != null) {
            Task<Either<ApiError, String>> tarea = new Task<Either<ApiError, String>>() {

                @Override
                protected Either<ApiError, String> call() throws Exception {
                    fxComboUsuarios.getScene().setCursor(Cursor.WAIT);
                    Agenda agenda = fxComboAgendas.getSelectionModel().getSelectedItem();
                    UsuarioGetDTO invitado = fxComboUsuarios.getSelectionModel().getSelectedItem();
                    String invitador = mainController.getUsername();
                    return mainController.getInvitacionServices().addAgenda(agenda, invitado, invitador);
                }
            };

            tarea.setOnSucceeded(cadena -> {
                        if (tarea.getValue().isRight()) {
                            alert.setContentText(tarea.getValue().right().value());
                        } else {
                            alert.setContentText(tarea.getValue().left().value().getMessage());
                        }
                        alert.showAndWait();
                        clearFields();
                        fxComboUsuarios.getScene().setCursor(Cursor.DEFAULT);
                    }
            );
            tarea.setOnFailed(workerStateEvent -> {
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException());
                fxComboUsuarios.getScene().setCursor(Cursor.DEFAULT);
            });
            new Thread(tarea).start();
        } else {
            alert.setContentText("Error - Selecciona un Usuario y una Agenda.");
            alert.showAndWait();
        }
    }
}
