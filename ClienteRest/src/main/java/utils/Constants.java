package utils;

public class Constants {

    //---------------------------------------------------------------------------FXML Constants
    public static final String FXML_LOGIN = "/fxml/FXMLLogin.fxml";
    public static final String FXML_GESTION_USUARIO = "/fxml/FXMLUsuarios.fxml";
    public static final String FXML_GESTION_MUSICA = "/fxml/FXMLMusica.fxml";
    public static final String FXML_GESTION_AGENDA = "/fxml/FXMLAgenda.fxml";
    public static final String FXML_INVITACIONES_AGENDA = "/fxml/FXMLInvitaciones.fxml";
    public static final String FXML_PANTALLA_MAIN_FXML = "/fxml/FXMLMain.fxml";
    public static final String FXML_NEWUSUARIO = "/fxml/FXMLNewUsuario.fxml";
    public static final String FXML_NEWPASSWORD = "/fxml/FXMLNewPassword.fxml";

    //---------------------------------------------------------------------------Calls Constants

    public static final String URL_USUARIO = "api/usuario";
    public static final String URL_MUSICA = "api/musica";
    public static final String URL_AGENDA = "api/agenda";
    public static final String URL_INVITACION = "api/invitacion";
    public static final String URL_MUSICA_LIVE = "api/musica/live";
    public static final String URL_LOGIN = "login";
    public static final String URL_MAIL = "api/mail";
    public static final String BASE_URL = "http://localhost:8080/ServidorRest/";

    public Constants() {
    }
}
