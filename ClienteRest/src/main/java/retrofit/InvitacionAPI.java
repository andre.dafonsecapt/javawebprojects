package retrofit;

import models.AgendaInvitacion;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import utils.Constants;

import java.util.List;

public interface InvitacionAPI {

    @GET(Constants.URL_INVITACION)
    Call<List<AgendaInvitacion>> getAgendas();

    @POST(Constants.URL_INVITACION)
    Call<String> addAgendaInvitacion(@Body AgendaInvitacion agendaInvitacion);
}
