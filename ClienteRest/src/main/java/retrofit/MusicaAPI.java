package retrofit;

import models.MusicaGetAndPostDTO;
import models.MusicaLiveGetDTO;
import retrofit2.Call;
import retrofit2.http.*;
import utils.Constants;

import java.util.List;

public interface MusicaAPI {

    @GET(Constants.URL_MUSICA + "/get/{idMusica}")
    Call<MusicaGetAndPostDTO> getMusica(@Path("idMusica") int idMusica);

    @GET(Constants.URL_MUSICA)
    Call<List<MusicaGetAndPostDTO>> getAllMusicas();

    @GET(Constants.URL_MUSICA_LIVE + "/{idMusica}")
    Call<MusicaLiveGetDTO> getLiveActs(@Path("idMusica") int idMusica);

    @POST(Constants.URL_MUSICA)
    Call<String> addMusica(@Body MusicaGetAndPostDTO musica);

    @DELETE(Constants.URL_MUSICA)
    Call<String> deleteMusica(@Query("idMusica") int idMusica);

    @PUT(Constants.URL_MUSICA)
    Call<String> updateMusica(@Body MusicaGetAndPostDTO musica);
}
