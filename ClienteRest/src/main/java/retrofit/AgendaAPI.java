package retrofit;

import models.AgendaSegura;
import models.MusicaGetAndPostDTO;
import models.MusicaLiveGetDTO;
import retrofit2.Call;
import retrofit2.http.*;
import utils.Constants;

import java.util.List;

public interface AgendaAPI {

    @GET(Constants.URL_AGENDA)
    Call<List<AgendaSegura>> getAgendas();

    @POST(Constants.URL_AGENDA)
    Call<String> addAgendaSegura(@Body AgendaSegura agendaSegura);

    @DELETE(Constants.URL_AGENDA)
    Call<String> deleteAgenda(@Query("idAgenda") int idAgenda);

    @PUT(Constants.URL_AGENDA)
    Call<String> updateAgendaSegura(@Body AgendaSegura agendaSegura);
}
