package retrofit;

import models.UsuarioGetDTO;
import models.UsuarioPostDTO;
import retrofit2.Call;
import retrofit2.http.*;
import utils.Constants;

import java.util.List;

public interface UsuarioAPI {

    @GET(Constants.URL_USUARIO + "/get/{idUsuario}")
    Call<UsuarioGetDTO> getUsuario(@Path("idUsuario") int idUsuario);

    @GET(Constants.URL_USUARIO)
    Call<List<UsuarioGetDTO>> getAllUsuarios();

    @POST(Constants.URL_USUARIO)
    Call<String> addUsuario(@Body UsuarioPostDTO usuario);

    @DELETE(Constants.URL_USUARIO)
    Call<String> deleteUsuario(@Query("idUsuario") int idUsuario);

    @PUT(Constants.URL_USUARIO)
    Call<String> updateUsuario(@Body UsuarioPostDTO usuario);
}
