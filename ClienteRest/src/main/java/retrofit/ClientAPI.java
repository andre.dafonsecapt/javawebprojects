package retrofit;


import models.UsuarioPostDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import utils.Constants;

public interface ClientAPI {


    // -----------------------------------------------------------------login servlet queries
    @POST(Constants.URL_LOGIN)
    Call<String> login(@Body UsuarioPostDTO usuario);

    // -----------------------------------------------------------------Mail servlet queries
    @GET(Constants.URL_MAIL)
    Call<String> requestPwd(@Query("email") String mail);
}
