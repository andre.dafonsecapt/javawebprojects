package dao;

import com.google.gson.Gson;
import models.UsuarioGetDTO;
import models.UsuarioPostDTO;
import retrofit.UsuarioAPI;
import retrofit2.Call;
import singleton.RetrofitSingleton;

import java.util.List;

public class UsuarioDAO implements UsuarioAPI {

    private UsuarioAPI usuarioAPI;
    private Gson gson;

    public UsuarioDAO() {
        usuarioAPI = RetrofitSingleton.getInstance().getRetrofit().create(UsuarioAPI.class);
        gson = RetrofitSingleton.getInstance().getGson();
    }

    public Gson getGson() {
        return gson;
    }

    @Override
    public Call<UsuarioGetDTO> getUsuario(int idUsuario) {
        return usuarioAPI.getUsuario(idUsuario);
    }

    @Override
    public Call<List<UsuarioGetDTO>> getAllUsuarios() {
        return usuarioAPI.getAllUsuarios();
    }

    @Override
    public Call<String> addUsuario(UsuarioPostDTO usuario) {
        return usuarioAPI.addUsuario(usuario);
    }

    @Override
    public Call<String> deleteUsuario(int idUsuario) {
        return usuarioAPI.deleteUsuario(idUsuario);
    }

    @Override
    public Call<String> updateUsuario(UsuarioPostDTO usuario) {
        return usuarioAPI.updateUsuario(usuario);
    }
}
