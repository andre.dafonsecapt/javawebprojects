package dao;

import com.google.gson.Gson;
import models.MusicaGetAndPostDTO;
import models.MusicaLiveGetDTO;
import retrofit.MusicaAPI;
import retrofit2.Call;
import singleton.RetrofitSingleton;

import java.util.List;

public class MusicaDAO implements MusicaAPI {

    private MusicaAPI musicaAPI;
    private Gson gson;

    public MusicaDAO() {
        musicaAPI = RetrofitSingleton.getInstance().getRetrofit().create(MusicaAPI.class);
        gson = RetrofitSingleton.getInstance().getGson();
    }

    public Gson getGson() {
        return gson;
    }

    @Override
    public Call<MusicaGetAndPostDTO> getMusica(int idMusica) {
        return musicaAPI.getMusica(idMusica);
    }

    @Override
    public Call<List<MusicaGetAndPostDTO>> getAllMusicas() {
        return musicaAPI.getAllMusicas();
    }

    @Override
    public Call<MusicaLiveGetDTO> getLiveActs(int idMusica) {
        return musicaAPI.getLiveActs(idMusica);
    }

    @Override
    public Call<String> addMusica(MusicaGetAndPostDTO musica) {
        return musicaAPI.addMusica(musica);
    }

    @Override
    public Call<String> deleteMusica(int idMusica) {
        return musicaAPI.deleteMusica(idMusica);
    }

    @Override
    public Call<String> updateMusica(MusicaGetAndPostDTO musica) {
        return musicaAPI.updateMusica(musica);
    }
}
