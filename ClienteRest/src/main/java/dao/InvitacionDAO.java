package dao;

import com.google.gson.Gson;
import models.AgendaInvitacion;
import retrofit.InvitacionAPI;
import retrofit2.Call;
import singleton.RetrofitSingleton;

import java.util.List;

public class InvitacionDAO implements InvitacionAPI {

    private InvitacionAPI invitacionAPI;
    private Gson gson;

    public InvitacionDAO() {
        invitacionAPI = RetrofitSingleton.getInstance().getRetrofit().create(InvitacionAPI.class);
        gson = RetrofitSingleton.getInstance().getGson();
    }

    @Override
    public Call<List<AgendaInvitacion>> getAgendas() {
        return invitacionAPI.getAgendas();
    }

    @Override
    public Call<String> addAgendaInvitacion(AgendaInvitacion agendaInvitacion) {
        return invitacionAPI.addAgendaInvitacion(agendaInvitacion);
    }

    public Gson getGson() {
        return gson;
    }
}
