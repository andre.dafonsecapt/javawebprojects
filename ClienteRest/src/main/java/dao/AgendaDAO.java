package dao;

import com.google.gson.Gson;
import models.AgendaSegura;
import retrofit.AgendaAPI;
import retrofit2.Call;
import singleton.RetrofitSingleton;

import java.util.List;

public class AgendaDAO implements AgendaAPI {

    private AgendaAPI agendaAPI;
    private Gson gson;

    public AgendaDAO() {
        agendaAPI = RetrofitSingleton.getInstance().getRetrofit().create(AgendaAPI.class);
        gson = RetrofitSingleton.getInstance().getGson();
    }

    public Gson getGson() {
        return gson;
    }


    @Override
    public Call<List<AgendaSegura>> getAgendas() {
        return agendaAPI.getAgendas();
    }

    @Override
    public Call<String> addAgendaSegura(AgendaSegura agendaSegura) {
        return agendaAPI.addAgendaSegura(agendaSegura);
    }

    @Override
    public Call<String> deleteAgenda(int idAgenda) {
        return agendaAPI.deleteAgenda(idAgenda);
    }

    @Override
    public Call<String> updateAgendaSegura(AgendaSegura agendaSegura) {
        return agendaAPI.updateAgendaSegura(agendaSegura);
    }
}
