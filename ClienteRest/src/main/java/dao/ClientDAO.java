package dao;

import com.google.gson.Gson;
import models.UsuarioPostDTO;
import retrofit.ClientAPI;
import retrofit2.Call;
import singleton.RetrofitSingleton;

public class ClientDAO implements ClientAPI {

    private Gson gson;
    private ClientAPI clientAPI;

    public ClientDAO() {
        clientAPI = RetrofitSingleton.getInstance().getRetrofit().create(ClientAPI.class);
        gson = RetrofitSingleton.getInstance().getGson();
    }

    public Gson getGson() {
        return gson;
    }

    @Override
    public Call<String> login(UsuarioPostDTO usuario) {
        return clientAPI.login(usuario);
    }


    @Override
    public Call<String> requestPwd(String mail) {
        return clientAPI.requestPwd(mail);
    }
}
