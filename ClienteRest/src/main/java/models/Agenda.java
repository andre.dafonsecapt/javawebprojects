package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Agenda {

    private int id;
    private String titulo;
    private LocalDateTime fecha;
    private int duracion;
    private String invitador;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("Id: ").append(id);
        sb.append(" - Titulo: ").append(titulo);
        sb.append(" - Fecha: ").append(fecha);
        sb.append(" - Duracion: ").append(duracion);
        if (invitador != null && !invitador.isEmpty()){
            sb.append(" - Invitador: ").append(invitador);
        }
        return sb.toString();
    }
}
