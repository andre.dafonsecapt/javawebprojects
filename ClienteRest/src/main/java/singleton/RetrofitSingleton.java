package singleton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import gson.LocalDateDeserializer;
import gson.LocalDateSerializer;
import gson.LocalDateTimeDeserializer;
import gson.LocalDateTimeSerializer;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import utils.Constants;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class RetrofitSingleton {

    public static RetrofitSingleton retrofitConnection = null;
    private Retrofit retrofit;
    private Gson gson;
    private String jwtString = "";

    private RetrofitSingleton() {
        gson = new GsonBuilder()
                .setLenient()
                .setPrettyPrinting()
                .registerTypeAdapter(LocalDate.class, new LocalDateSerializer())
                .registerTypeAdapter(LocalDate.class, new LocalDateDeserializer())
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeSerializer())
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeDeserializer())
                .create();
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        OkHttpClient clientOK = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .addInterceptor(chain -> {
                            Request original = chain.request();
                            Request.Builder builder = original.newBuilder()
                                    .header("JWT", jwtString)
                                    .url(original.url().newBuilder().addQueryParameter("headToken", "adfsdf").build());
                            Request request = builder.build();
                            return chain.proceed(request);
                        }
                )
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create()) // For parsing JSONP or String
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(clientOK)
                .build();
    }

    public static RetrofitSingleton getInstance() {
        if (retrofitConnection == null) {
            retrofitConnection = new RetrofitSingleton();
        }
        return retrofitConnection;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public Gson getGson() {
        return gson;
    }

    public void setJwtString(String jwtString) {
        this.jwtString = jwtString;
    }
}
