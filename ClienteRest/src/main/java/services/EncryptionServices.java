package services;

import com.google.gson.Gson;
import models.Agenda;
import models.AgendaInvitacion;
import models.AgendaSegura;
import singleton.RetrofitSingleton;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EncryptionServices {

    public static AgendaSegura encryptAgenda(Agenda agenda, String username) {
        try {
            byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            byte[] salt = new byte[16];
            SecureRandom sr = new SecureRandom();
            sr.nextBytes(iv);
            sr.nextBytes(salt);

            // crear agenda segura y string to encrypt
            AgendaSegura agendaSegura = new AgendaSegura();
            agendaSegura.setIv(Base64.getUrlEncoder().encodeToString(iv));
            agendaSegura.setSalt(Base64.getUrlEncoder().encodeToString(salt));
            agendaSegura.setIteraciones(65536);
            agendaSegura.setKeySize(128);
            Gson gson = RetrofitSingleton.getInstance().getGson();
            String agendaStringToEncrypt = gson.toJson(agenda);

            // encrypt agenda with random generated key
            String randomGeneratedKey = generateKey(10);
            byte[] agendaEncrypted = encryptWithRandomKey(agendaStringToEncrypt, randomGeneratedKey, iv, salt, agendaSegura.getIteraciones(), agendaSegura.getKeySize());
            agendaSegura.setAgenda(Base64.getUrlEncoder().encodeToString(agendaEncrypted));

            // encrypt random generated key with users public key
            byte[] randomKeyEncrypted = encryptRandomKeyWithPublicKey(username, randomGeneratedKey);
            agendaSegura.setEncryptKey(Base64.getUrlEncoder().encodeToString(randomKeyEncrypted));

            return agendaSegura;
        } catch (Exception e) {
            Logger.getLogger("Error while encrypting").log(Level.SEVERE, null, e);
        }
        return null;
    }

    public static AgendaInvitacion encryptAgendaInvitacion(Agenda agenda, String invitado) {
        try {
            byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            byte[] salt = new byte[16];
            SecureRandom sr = new SecureRandom();
            sr.nextBytes(iv);
            sr.nextBytes(salt);

            // crear agenda invitacion y string to encrypt
            AgendaInvitacion agendaInvitacion = new AgendaInvitacion();
            agendaInvitacion.setIv(Base64.getUrlEncoder().encodeToString(iv));
            agendaInvitacion.setSalt(Base64.getUrlEncoder().encodeToString(salt));
            agendaInvitacion.setIteraciones(65536);
            agendaInvitacion.setKeySize(128);
            Gson gson = RetrofitSingleton.getInstance().getGson();
            String agendaStringToEncrypt = gson.toJson(agenda);

            // encrypt agenda with random generated key
            String randomGeneratedKey = generateKey(10);
            byte[] agendaEncrypted = encryptWithRandomKey(agendaStringToEncrypt, randomGeneratedKey, iv, salt, agendaInvitacion.getIteraciones(), agendaInvitacion.getKeySize());
            agendaInvitacion.setAgenda(Base64.getUrlEncoder().encodeToString(agendaEncrypted));

            // encrypt random generated key with invited users public key
            byte[] randomKeyEncrypted = encryptRandomKeyWithPublicKey(invitado, randomGeneratedKey);
            agendaInvitacion.setEncryptKey(Base64.getUrlEncoder().encodeToString(randomKeyEncrypted));

            return agendaInvitacion;
        } catch (Exception e) {
            Logger.getLogger("Error while encrypting").log(Level.SEVERE, null, e);
        }
        return null;
    }

    private static byte[] encryptWithRandomKey(String stringToEncrypt, String randomGeneratedKey, byte[] iv, byte[] salt, int iteraciones, int keySize) throws Exception {

        GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        // en el jdk8 esta limitado a 128 bits, desde el 9 puede ser de 256
        KeySpec spec = new PBEKeySpec(randomGeneratedKey.toCharArray(), salt, iteraciones, keySize);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");
        Cipher cipher = Cipher.getInstance("AES/GCM/noPadding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);
        return cipher.doFinal(stringToEncrypt.getBytes(StandardCharsets.UTF_8));
    }

    private static byte[] encryptRandomKeyWithPublicKey(String username, String randomGeneratedKey) throws Exception {
        PublicKey clavePublica = getPublicKey(username);
        Cipher cifrador = Cipher.getInstance("RSA");
        cifrador.init(Cipher.ENCRYPT_MODE, clavePublica);
        return cifrador.doFinal(randomGeneratedKey.getBytes());
    }

    public static String firmarAgenda(Agenda agenda, String invitador) throws Exception {

        Gson gson = RetrofitSingleton.getInstance().getGson();
        String agendaStringToSign = gson.toJson(agenda);

        PrivateKey clavePrivada = getPrivateKey(invitador);
        Signature sign = Signature.getInstance("SHA256WithRSA");
        sign.initSign(clavePrivada);
        sign.update(agendaStringToSign.getBytes());
        byte[] firma = sign.sign();

        return Base64.getUrlEncoder().encodeToString(firma);
    }



    public static boolean firmaOK(AgendaInvitacion agendaInvitacion, String invitador, String invitado) throws Exception {
        Gson gson = RetrofitSingleton.getInstance().getGson();
        Agenda agenda = decryptAgendaInvitacion(agendaInvitacion, invitado);
        String agendaStringToCheckSign = gson.toJson(agenda);
        byte[] firma = Base64.getUrlDecoder().decode(agendaInvitacion.getFirma());

        PublicKey clavePublica = getPublicKey(invitador);
        Signature sign = Signature.getInstance("SHA256WithRSA");
        sign.initVerify(clavePublica);
        sign.update(agendaStringToCheckSign.getBytes());

        return sign.verify(firma);
    }

    public static Agenda decryptAgendaSegura(AgendaSegura agendaSegura, String username) {
        try {
            byte[] agendaDecoded = Base64.getUrlDecoder().decode(agendaSegura.getAgenda());

            byte[] iv = Base64.getUrlDecoder().decode(agendaSegura.getIv());
            byte[] salt = Base64.getUrlDecoder().decode(agendaSegura.getSalt());
            byte[] encryptKeyDecoded = Base64.getUrlDecoder().decode(agendaSegura.getEncryptKey());

            // decrypt encryptKey with users private key
            String encryptKeyDecrypted = decryptEncryptionKeyWithPrivateKey(encryptKeyDecoded, username);

            // decrypt agendaSegura with decrypted encryptKey
            String agendaDecrypted = decryptWithEncryptionKey(agendaDecoded, encryptKeyDecrypted, iv, salt, agendaSegura.getIteraciones(), agendaSegura.getKeySize());
            Gson gson = RetrofitSingleton.getInstance().getGson();

            return gson.fromJson(agendaDecrypted, Agenda.class);

        } catch (Exception e) {
            Logger.getLogger("Error while decrypting").log(Level.SEVERE, null, e);
        }
        return null;
    }

    public static Agenda decryptAgendaInvitacion(AgendaInvitacion agendaInvitacion, String invitadoUsername) {
        try {
            byte[] agendaDecoded = Base64.getUrlDecoder().decode(agendaInvitacion.getAgenda());

            byte[] iv = Base64.getUrlDecoder().decode(agendaInvitacion.getIv());
            byte[] salt = Base64.getUrlDecoder().decode(agendaInvitacion.getSalt());
            byte[] encryptKeyDecoded = Base64.getUrlDecoder().decode(agendaInvitacion.getEncryptKey());

            // decrypt encryptKey with invited users private key
            String encryptKeyDecrypted = decryptEncryptionKeyWithPrivateKey(encryptKeyDecoded, invitadoUsername);

            // decrypt agendaInvitacion with decrypted encryptKey
            String agendaDecrypted = decryptWithEncryptionKey(agendaDecoded, encryptKeyDecrypted, iv, salt, agendaInvitacion.getIteraciones(), agendaInvitacion.getKeySize());
            Gson gson = RetrofitSingleton.getInstance().getGson();

            return gson.fromJson(agendaDecrypted, Agenda.class);

        } catch (Exception e) {
            Logger.getLogger("Error while decrypting").log(Level.SEVERE, null, e);
        }
        return null;
    }

    public static String decryptEncryptionKeyWithPrivateKey(byte[] encryptKeyDecoded, String username) throws Exception {

        // decrypt encryptKey with username´s private key
        PrivateKey clavePrivada = getPrivateKey(username);
        Cipher cifrador = Cipher.getInstance("RSA");
        cifrador.init(Cipher.DECRYPT_MODE, clavePrivada);
        byte[] bytesKey = cifrador.doFinal(encryptKeyDecoded);
        return new String(bytesKey);
    }

    public static String decryptWithEncryptionKey(byte[] bytesToDecrypt, String encryptKeyDecrypted, byte[] iv, byte[] salt, int iteraciones, int keySize) throws Exception {
        GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(encryptKeyDecrypted.toCharArray(), salt, iteraciones, keySize);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKeySpec secretKey = new SecretKeySpec(tmp.getEncoded(), "AES");

        Cipher cipher = Cipher.getInstance("AES/GCM/noPADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);
        byte [] bytesAgendaDecrypted = cipher.doFinal(bytesToDecrypt);
        return new String(bytesAgendaDecrypted);
    }

    public static String generateKey(int length) {

        //minimum length of 6
        if (length < 4) {
            length = 6;
        }
        final char[] allAllowed = "abcdefghijklmnopqrstuvwxyzABCDEFGJKLMNPRSTUVWXYZ0123456789".toCharArray();

        //Use cryptographically secure random number generator
        Random random = new SecureRandom();
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < length; i++) {
            password.append(allAllowed[random.nextInt(allAllowed.length)]);
        }
        return password.toString();

    }

    public static void crearKeys(String nombre) throws Exception {

        /**
         * * Crear claves RSA 1024 bits
         */
        KeyPairGenerator generadorRSA = KeyPairGenerator.getInstance("RSA"); // Hace uso del provider BC
        generadorRSA.initialize(2048);
        KeyPair clavesRSA = generadorRSA.generateKeyPair();
        PrivateKey clavePrivada = clavesRSA.getPrivate();
        PublicKey clavePublica = clavesRSA.getPublic();

        /**
         * * 1 Volcar clave privada a fichero
         */
        // 1.1 Codificar clave privada en formato PKCS8 (necesario para escribirla a disco)
        PKCS8EncodedKeySpec pkcs8Spec = new PKCS8EncodedKeySpec(clavePrivada.getEncoded());

        // 1.2 Escribirla a fichero binario
        FileOutputStream out = new FileOutputStream(nombre + ".privada");
        out.write(pkcs8Spec.getEncoded());
        out.close();

        /**
         * * 3 Volcar clave publica a fichero
         */
        // 3.1 Codificar clave publica en formato X509 (necesario para escribirla a disco)
        X509EncodedKeySpec x509Spec = new X509EncodedKeySpec(clavePublica.getEncoded());


        // 3.2 Escribirla a fichero binario
        out = new FileOutputStream(nombre + ".publica");
        out.write(x509Spec.getEncoded());
        out.close();
    }

    public static PublicKey getPublicKey(String nombre) throws Exception {
        /**
         * * 4 Recuperar clave PUBLICA del fichero
         */
        // 4.1 Leer datos binarios x809
        byte[] bufferPub = new byte[5000];
        FileInputStream in = new FileInputStream(nombre + ".publica");
        int chars = in.read(bufferPub, 0, 5000);
        in.close();

        byte[] bufferPub2 = new byte[chars];
        System.arraycopy(bufferPub, 0, bufferPub2, 0, chars);
        // 4.2 Recuperar clave publica desde datos codificados en formato X509
        KeyFactory keyFactoryRSA = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec clavePublicaSpec = new X509EncodedKeySpec(bufferPub);
        PublicKey clavePublica = keyFactoryRSA.generatePublic(clavePublicaSpec);

        return clavePublica;

    }

    public static PrivateKey getPrivateKey(String nombre) throws Exception {

        /**
         * * 2 Recuperar clave Privada del fichero
         */
        // 2.1 Leer datos binarios PKCS8
        byte[] bufferPriv = new byte[5000];
        FileInputStream in = new FileInputStream(nombre + ".privada");
        int chars = in.read(bufferPriv, 0, 5000);
        in.close();

        byte[] bufferPriv2 = new byte[chars];
        System.arraycopy(bufferPriv, 0, bufferPriv2, 0, chars);

        // 2.2 Recuperar clave privada desde datos codificados en formato PKCS8
        PKCS8EncodedKeySpec clavePrivadaSpec = new PKCS8EncodedKeySpec(bufferPriv2);
        /**
         * * Crear KeyFactory (depende del provider) usado para las
         * transformaciones de claves
         */
        KeyFactory keyFactoryRSA = KeyFactory.getInstance("RSA");

        PrivateKey clavePrivada = keyFactoryRSA.generatePrivate(clavePrivadaSpec);

        return clavePrivada;

    }
}
