package services;

import dao.AgendaDAO;
import fj.data.Either;
import models.Agenda;
import models.AgendaSegura;
import models.ApiError;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AgendaServices {

    private AgendaDAO agendaDAO;

    public AgendaServices() {
        agendaDAO = new AgendaDAO();
    }

    public Either<ApiError, List<Agenda>> getAgendas(String username) throws IOException {
        Call<List<AgendaSegura>> call = agendaDAO.getAgendas();
        Response<List<AgendaSegura>> resp = call.execute();
        if (resp.isSuccessful()) {
            List<AgendaSegura> agendaSeguraList = resp.body();
            List<Agenda> agendaList = new ArrayList<>();
            agendaSeguraList.forEach(agendaSegura -> {
                Agenda agenda = EncryptionServices.decryptAgendaSegura(agendaSegura, username);
                agenda.setId(agendaSegura.getIdAgenda());
                agendaList.add(agenda);
            });
            return Either.right(agendaList);
        } else {
            ApiError error = agendaDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }

    public Either<ApiError, String> addAgenda(Agenda agenda, String username) throws IOException {

        Call<String> call = agendaDAO.addAgendaSegura(EncryptionServices.encryptAgenda(agenda, username));
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = agendaDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }

    public Either<ApiError, String> deleteAgenda(int id) throws IOException {
        Call<String> call = agendaDAO.deleteAgenda(id);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = agendaDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }

    public Either<ApiError, String> updateAgenda(Agenda agenda, String username) throws IOException {

        AgendaSegura agendaSegura = EncryptionServices.encryptAgenda(agenda, username);
        agendaSegura.setIdAgenda(agenda.getId());
        Call<String> call = agendaDAO.updateAgendaSegura(agendaSegura);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = agendaDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }
}
