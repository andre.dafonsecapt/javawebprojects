package services;

import models.MusicaGetAndPostDTO;
import models.UsuarioPostDTO;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

public class ValidatorServices {

    private Validator val;

    public ValidatorServices() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        val = factory.getValidator();
    }

    public String validarUsuarioPostDTO(UsuarioPostDTO u) {
        StringBuffer errorStrings = new StringBuffer();
        val.validate(u).forEach(usuarioPostDTOConstraintViolation ->
                errorStrings.append(usuarioPostDTOConstraintViolation.getMessage()).append('\n'));
        return errorStrings.toString();
    }

    public String validarMusicaGetAndPostDTO(MusicaGetAndPostDTO m) {
        StringBuffer errorStrings = new StringBuffer();
        val.validate(m).forEach(musicaGetAndPostDTOConstraintViolation ->
                errorStrings.append(musicaGetAndPostDTOConstraintViolation.getMessage()).append('\n'));
        return errorStrings.toString();
    }
}
