package services;

import dao.InvitacionDAO;
import dao.UsuarioDAO;
import fj.data.Either;
import models.Agenda;
import models.AgendaInvitacion;
import models.ApiError;
import models.UsuarioGetDTO;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InvitacionServices {

    private InvitacionDAO invitacionDAO;
    private UsuarioDAO usuarioDAO;

    public InvitacionServices() {
        invitacionDAO = new InvitacionDAO();
        usuarioDAO = new UsuarioDAO();
    }

    public Either<ApiError, List<Agenda>> getAgendas(String username) throws Exception {
        AtomicBoolean firmaSuccess = new AtomicBoolean(true);
        // get all usuarios
        Either<ApiError, List<UsuarioGetDTO>> eitherUsuarios = getUsuarios();
        if (eitherUsuarios.isRight()) {
            List<UsuarioGetDTO> usuarios = eitherUsuarios.right().value();
            Call<List<AgendaInvitacion>> call = invitacionDAO.getAgendas();
            Response<List<AgendaInvitacion>> resp = call.execute();
            if (resp.isSuccessful()) {
                    // get el usuario invitador y el invitado
                    List<AgendaInvitacion> agendaInvitacionList = resp.body();
                    List<Agenda> agendaList = new ArrayList<>();
                    agendaInvitacionList.forEach(agendaInvitacion -> {
                        try {
                            UsuarioGetDTO invitador = usuarios.stream()
                                    .filter(usuarioGetDTO -> usuarioGetDTO.getIdUsuario() == agendaInvitacion.getFk_idInvitador())
                                    .findAny().orElse(null);
                            UsuarioGetDTO invitado = usuarios.stream()
                                    .filter(usuarioGetDTO -> usuarioGetDTO.getIdUsuario() == agendaInvitacion.getFk_idInvitado())
                                    .findAny().orElse(null);
                            String invitadorUsername = invitador.getUsername();
                            String invitadoUsername = invitado.getUsername();
                            // verificar firma y si OK decrypt la agenda invitada
                            if (EncryptionServices.firmaOK(agendaInvitacion, invitadorUsername, invitadoUsername)) {
                                Agenda agenda = EncryptionServices.decryptAgendaInvitacion(agendaInvitacion, username);
                                agenda.setId(agendaInvitacion.getIdagenda_invitacion());
                                agenda.setInvitador(invitadorUsername);
                                agendaList.add(agenda);
                            } else {
                                firmaSuccess.set(false);
                            }
                        } catch (Exception e){
                            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                        }
                    });
                    if (firmaSuccess.get()) {
                        return Either.right(agendaList);
                    } else {
                        ApiError error = new ApiError("Error - No ha sido podido confirmar la firma de las agendas.");
                        return Either.left(error);
                    }
            } else {
                ApiError error = invitacionDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
                return Either.left(error);
            }
        } else {
            ApiError error = invitacionDAO.getGson().fromJson(eitherUsuarios.left().value().getMessage(), ApiError.class);
            return Either.left(error);
        }
    }

    private Either<ApiError, List<UsuarioGetDTO>> getUsuarios() throws IOException {
        Call<List<UsuarioGetDTO>> call = usuarioDAO.getAllUsuarios();
        Response<List<UsuarioGetDTO>> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = usuarioDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }

    public Either<ApiError, String> addAgenda(Agenda agenda, UsuarioGetDTO invitado, String invitador) throws Exception {

        AgendaInvitacion agendaInvitacion = EncryptionServices.encryptAgendaInvitacion(agenda, invitado.getUsername());
        agendaInvitacion.setFirma(EncryptionServices.firmarAgenda(agenda, invitador));
        agendaInvitacion.setFk_idInvitado(invitado.getIdUsuario());
        Call<String> call = invitacionDAO.addAgendaInvitacion(agendaInvitacion);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = invitacionDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }
}
