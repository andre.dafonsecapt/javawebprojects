package services;

import dao.MusicaDAO;
import fj.data.Either;
import models.ApiError;
import models.MusicaGetAndPostDTO;
import models.MusicaLiveGetDTO;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;

public class MusicaServices {

    private MusicaDAO musicaDAO;
    private ValidatorServices vs;


    public MusicaServices() {
        musicaDAO = new MusicaDAO();
        vs = new ValidatorServices();
    }

    public Either<ApiError, MusicaGetAndPostDTO> getMusica(int idMusica) throws IOException {
        Call<MusicaGetAndPostDTO> call = musicaDAO.getMusica(idMusica);
        Response<MusicaGetAndPostDTO> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = musicaDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }

    public Either<ApiError, List<MusicaGetAndPostDTO>> getMusicas() throws IOException {
        Call<List<MusicaGetAndPostDTO>> call = musicaDAO.getAllMusicas();
        Response<List<MusicaGetAndPostDTO>> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = musicaDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }

    public Either<ApiError, MusicaLiveGetDTO> getLiveActs(int idMusica) throws IOException {
        Call<MusicaLiveGetDTO> call = musicaDAO.getLiveActs(idMusica);
        Response<MusicaLiveGetDTO> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error;
            try {
                error = musicaDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            } catch (Exception e) {
                error = new ApiError(e.getMessage());
            }
            return Either.left(error);
        }
    }

    public Either<ApiError, String> addMusica(MusicaGetAndPostDTO musica) throws IOException {
        String errorsString = vs.validarMusicaGetAndPostDTO(musica);
        if (errorsString.isEmpty()) {
            Call<String> call = musicaDAO.addMusica(musica);
            Response<String> resp = call.execute();
            if (resp.isSuccessful()) {
                return Either.right(resp.body());
            } else {
                ApiError error = musicaDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
                return Either.left(error);
            }
        } else {
            ApiError error = new ApiError(errorsString);
            return Either.left(error);
        }
    }

    public Either<ApiError, String> deleteMusica(int idMusica) throws IOException {
        Call<String> call = musicaDAO.deleteMusica(idMusica);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = musicaDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }

    public Either<ApiError, String> updateMusica(MusicaGetAndPostDTO musica) throws IOException {
        String errorsString = vs.validarMusicaGetAndPostDTO(musica);
        if (errorsString.isEmpty()) {
            Call<String> call = musicaDAO.updateMusica(musica);
            Response<String> resp = call.execute();
            if (resp.isSuccessful()) {
                return Either.right(resp.body());
            } else {
                ApiError error = musicaDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
                return Either.left(error);
            }
        } else {
            ApiError error = new ApiError(errorsString);
            return Either.left(error);
        }
    }
}
