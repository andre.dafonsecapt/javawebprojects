package services;

import dao.ClientDAO;
import fj.data.Either;
import models.ApiError;
import models.UsuarioPostDTO;
import retrofit2.Response;
import singleton.RetrofitSingleton;

import java.io.IOException;

public class ClientServices {

    private ClientDAO clientDao;
    private ValidatorServices vs;

    public ClientServices() {
        clientDao = new ClientDAO();
        vs = new ValidatorServices();
    }


    public Either<ApiError, String> loginUsuario(UsuarioPostDTO usuario) throws IOException {
        String errorsString = vs.validarUsuarioPostDTO(usuario);
        if (errorsString.isEmpty()) {
            Response<String> resp = clientDao.login(usuario).execute();
            if (resp.isSuccessful()) {
                RetrofitSingleton.getInstance().setJwtString(resp.headers().get("JWT"));
                return Either.right(resp.body());
            } else {
                ApiError error = clientDao.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
                return Either.left(error);
            }
        } else {
            ApiError error = new ApiError(errorsString);
            return Either.left(error);
        }
    }

    public Either<ApiError, String> requestPwd(String mail) throws IOException {
        Response<String> resp = clientDao.requestPwd(mail).execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = clientDao.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }
}
