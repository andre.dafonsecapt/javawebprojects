package services;

import dao.UsuarioDAO;
import fj.data.Either;
import models.ApiError;
import models.UsuarioGetDTO;
import models.UsuarioPostDTO;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;

public class UsuarioServices {

    private UsuarioDAO usuarioDAO;
    private ValidatorServices vs;


    public UsuarioServices() {
        usuarioDAO = new UsuarioDAO();
        vs = new ValidatorServices();
    }

    public Either<ApiError, UsuarioGetDTO> getUsuario(int idUsuario) throws IOException {
        Call<UsuarioGetDTO> call = usuarioDAO.getUsuario(idUsuario);
        Response<UsuarioGetDTO> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = usuarioDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }

    public Either<ApiError, List<UsuarioGetDTO>> getUsuarios() throws IOException {
        Call<List<UsuarioGetDTO>> call = usuarioDAO.getAllUsuarios();
        Response<List<UsuarioGetDTO>> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = usuarioDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }

    public Either<ApiError, String> addUsuario(UsuarioPostDTO usuario) throws IOException {
        String errorsString = vs.validarUsuarioPostDTO(usuario);
        if (errorsString.isEmpty()) {
            Call<String> call = usuarioDAO.addUsuario(usuario);
            Response<String> resp = call.execute();
            if (resp.isSuccessful()) {
                return Either.right(resp.body());
            } else {
                ApiError error = usuarioDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
                return Either.left(error);
            }
        } else {
            ApiError error = new ApiError(errorsString);
            return Either.left(error);
        }
    }

    public Either<ApiError, String> deleteUsuario(int idUsuario) throws IOException {
        Call<String> call = usuarioDAO.deleteUsuario(idUsuario);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            return Either.right(resp.body());
        } else {
            ApiError error = usuarioDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
            return Either.left(error);
        }
    }

    public Either<ApiError, String> updateUsuario(UsuarioPostDTO usuario) throws IOException {
        String errorsString = vs.validarUsuarioPostDTO(usuario);
        if (errorsString.isEmpty()) {
            Call<String> call = usuarioDAO.updateUsuario(usuario);
            Response<String> resp = call.execute();
            if (resp.isSuccessful()) {
                return Either.right(resp.body());
            } else {
                ApiError error = usuarioDAO.getGson().fromJson(resp.errorBody().charStream(), ApiError.class);
                return Either.left(error);
            }
        } else {
            ApiError error = new ApiError(errorsString);
            return Either.left(error);
        }
    }
}
