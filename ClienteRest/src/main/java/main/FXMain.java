package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import utils.Constants;

import java.io.IOException;

public class FXMain extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader loaderMenu = new FXMLLoader(getClass().getResource(Constants.FXML_PANTALLA_MAIN_FXML));
        BorderPane root = loaderMenu.load();

        Scene scene = new Scene(root);

        primaryStage.setTitle("Cliente Rest");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
