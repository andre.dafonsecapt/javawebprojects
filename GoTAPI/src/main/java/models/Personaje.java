package models;

import java.util.List;

public class Personaje {
    private String url;
    private String name;
    private String gender;
    private String culture;
    private String born;
    private String died;
    private List<String> titles;
    private List<String> aliases;
    private String father;
    private String mother;
    private String spouse;
    private List<String> allegiances;
    private List<String> books;
    private List<Object> povBooks;
    private List<String> tvSeries;
    private List<String> playedBy;

    public String getURL() {
        return url;
    }

    public void setURL(String value) {
        this.url = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String value) {
        this.gender = value;
    }

    public String getCulture() {
        return culture;
    }

    public void setCulture(String value) {
        this.culture = value;
    }

    public String getBorn() {
        return born;
    }

    public void setBorn(String value) {
        this.born = value;
    }

    public String getDied() {
        return died;
    }

    public void setDied(String value) {
        this.died = value;
    }

    public List<String> getTitles() {
        return titles;
    }

    public void setTitles(List<String> value) {
        this.titles = value;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public void setAliases(List<String> value) {
        this.aliases = value;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String value) {
        this.father = value;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String value) {
        this.mother = value;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String value) {
        this.spouse = value;
    }

    public List<String> getAllegiances() {
        return allegiances;
    }

    public void setAllegiances(List<String> value) {
        this.allegiances = value;
    }

    public List<String> getBooks() {
        return books;
    }

    public void setBooks(List<String> value) {
        this.books = value;
    }

    public List<Object> getPovBooks() {
        return povBooks;
    }

    public void setPovBooks(List<Object> value) {
        this.povBooks = value;
    }

    public List<String> getTvSeries() {
        return tvSeries;
    }

    public void setTvSeries(List<String> value) {
        this.tvSeries = value;
    }

    public List<String> getPlayedBy() {
        return playedBy;
    }

    public void setPlayedBy(List<String> value) {
        this.playedBy = value;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Personaje:\n");
        sb.append("url=").append(url).append('\n');
        sb.append("name=").append(name).append('\n');
        sb.append("gender=").append(gender).append('\n');
        sb.append("culture=").append(culture).append('\n');
        sb.append("born=").append(born).append('\n');
        sb.append("died=").append(died).append('\n');
        sb.append("titles=").append(titles).append('\n');
        sb.append("aliases=").append(aliases).append('\n');
        sb.append("father=").append(father).append('\n');
        sb.append("mother=").append(mother).append('\n');
        sb.append("spouse=").append(spouse).append('\n');
        sb.append("allegiances=").append(allegiances).append('\n');
        sb.append("books=").append(books).append('\n');
        sb.append("povBooks=").append(povBooks).append('\n');
        sb.append("tvSeries=").append(tvSeries).append('\n');
        sb.append("playedBy=").append(playedBy).append('\n');
        sb.append("----\n");
        return sb.toString();
    }
}
