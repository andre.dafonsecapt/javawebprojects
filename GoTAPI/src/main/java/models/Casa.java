package models;

import java.util.List;

public class Casa {
    private String url;
    private String name;
    private String region;
    private String coatOfArms;
    private String words;
    private List<String> titles;
    private List<String> seats;
    private String currentLord;
    private String heir;
    private String overlord;
    private String founded;
    private String founder;
    private String diedOut;
    private List<String> ancestralWeapons;
    private List<String> cadetBranches;
    private List<String> swornMembers;

    public String getURL() { return url; }
    public void setURL(String value) { this.url = value; }

    public String getName() { return name; }
    public void setName(String value) { this.name = value; }

    public String getRegion() { return region; }
    public void setRegion(String value) { this.region = value; }

    public String getCoatOfArms() { return coatOfArms; }
    public void setCoatOfArms(String value) { this.coatOfArms = value; }

    public String getWords() { return words; }
    public void setWords(String value) { this.words = value; }

    public List<String> getTitles() { return titles; }
    public void setTitles(List<String> value) { this.titles = value; }

    public List<String> getSeats() { return seats; }
    public void setSeats(List<String> value) { this.seats = value; }

    public String getCurrentLord() { return currentLord; }
    public void setCurrentLord(String value) { this.currentLord = value; }

    public String getHeir() { return heir; }
    public void setHeir(String value) { this.heir = value; }

    public String getOverlord() { return overlord; }
    public void setOverlord(String value) { this.overlord = value; }

    public String getFounded() { return founded; }
    public void setFounded(String value) { this.founded = value; }

    public String getFounder() { return founder; }
    public void setFounder(String value) { this.founder = value; }

    public String getDiedOut() { return diedOut; }
    public void setDiedOut(String value) { this.diedOut = value; }

    public List<String> getAncestralWeapons() { return ancestralWeapons; }
    public void setAncestralWeapons(List<String> value) { this.ancestralWeapons = value; }

    public List<String> getCadetBranches() { return cadetBranches; }
    public void setCadetBranches(List<String> value) { this.cadetBranches = value; }

    public List<String> getSwornMembers() { return swornMembers; }
    public void setSwornMembers(List<String> value) { this.swornMembers = value; }
}
