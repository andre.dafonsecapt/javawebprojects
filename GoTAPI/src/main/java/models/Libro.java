package models;

import java.util.List;

public class Libro {
    private String url;
    private String name;
    private String isbn;
    private List<String> authors;
    private long numberOfPages;
    private String publisher;
    private String country;
    private String mediaType;
    private String released;
    private List<String> characters;
    private List<String> povCharacters;

    public String getURL() { return url; }
    public void setURL(String value) { this.url = value; }

    public String getName() { return name; }
    public void setName(String value) { this.name = value; }

    public String getIsbn() { return isbn; }
    public void setIsbn(String value) { this.isbn = value; }

    public List<String> getAuthors() { return authors; }
    public void setAuthors(List<String> value) { this.authors = value; }

    public long getNumberOfPages() { return numberOfPages; }
    public void setNumberOfPages(long value) { this.numberOfPages = value; }

    public String getPublisher() { return publisher; }
    public void setPublisher(String value) { this.publisher = value; }

    public String getCountry() { return country; }
    public void setCountry(String value) { this.country = value; }

    public String getMediaType() { return mediaType; }
    public void setMediaType(String value) { this.mediaType = value; }

    public String getReleased() { return released; }
    public void setReleased(String value) { this.released = value; }

    public List<String> getCharacters() { return characters; }
    public void setCharacters(List<String> value) { this.characters = value; }

    public List<String> getPovCharacters() { return povCharacters; }
    public void setPovCharacters(List<String> value) { this.povCharacters = value; }

}
