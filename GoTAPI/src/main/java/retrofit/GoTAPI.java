package retrofit;

import models.Casa;
import models.Libro;
import models.Personaje;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.io.IOException;
import java.util.List;

public interface GoTAPI {

    @GET("characters")
    Call<List<Personaje>> loadPersonajes(@Query("page") int page, @Query("pageSize") Integer pageSize) throws IOException;

    @GET("characters")
    Call<List<Personaje>> loadPersonajesFiltrado(@Query("name") String nombre, @Query("culture") String cultura, @Query("isAlive") String vivoOMuerto, @Query("page") Integer page, @Query("pageSize") Integer pageSize);

    @GET("books")
    Call<List<Libro>> loadLibros(@Query("pageSize") int pageSize);

    @GET("books/{book}")
    Call<Libro> loadLibro(@Path("book") int numLibro);

    @GET("houses")
    Call<List<Casa>> loadCasas(@Query("page") Integer page, @Query("pageSize") Integer pageSize);

    @GET("houses/{house}")
    Call<Casa> loadCasa(@Path("house") int numCasa);
}
