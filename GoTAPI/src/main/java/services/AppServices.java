package services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.Casa;
import models.Libro;
import models.Personaje;
import retrofit.GoTAPI;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import utils.Constantes;

import java.io.IOException;
import java.util.List;

public class AppServices implements GoTAPI {

    private Gson gson;
    private GoTAPI goTAPI;

    public AppServices() {
        gson = new GsonBuilder()
                .setLenient()
                .setPrettyPrinting()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constantes.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        goTAPI = retrofit.create(GoTAPI.class);
    }

    public Gson getGson() {
        return gson;
    }

    @Override
    public Call<List<Personaje>> loadPersonajes(int page, Integer pageSize) throws IOException {

        return goTAPI.loadPersonajes(page, pageSize);
    }

    @Override
    public Call<List<Personaje>> loadPersonajesFiltrado(String nombre, String cultura, String vivoOMuerto, Integer page, Integer pageSize) {
        return goTAPI.loadPersonajesFiltrado(nombre, cultura, vivoOMuerto, page, pageSize);
    }

    @Override
    public Call<List<Libro>> loadLibros(int pageSize) {
        return goTAPI.loadLibros(pageSize);
    }

    @Override
    public Call<Libro> loadLibro(int numLibro) {
        return goTAPI.loadLibro(numLibro);
    }

    @Override
    public Call<List<Casa>> loadCasas(Integer page, Integer pageSize) {
        return goTAPI.loadCasas(page, pageSize);
    }

    @Override
    public Call<Casa> loadCasa(int numCasa) {
        return goTAPI.loadCasa(numCasa);
    }
}
