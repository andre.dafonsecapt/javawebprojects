package utils;

public class ConstantesFxml {

    private ConstantesFxml() {
    }

    public static final String FXML_PANTALLA_MAIN_FXML = "/fxml/FXMLMain.fxml";
    public static final String FXML_PANTALLA_Personajes_FXML = "/fxml/FXMLPersonajes.fxml";
    public static final String FXML_PANTALLA_PersonajesFiltrado_FXML = "/fxml/FXMLPersonajesFiltrado.fxml";
    public static final String FXML_PANTALLA_LIBROS_FXML = "/fxml/FXMLLibros.fxml";
    public static final String FXML_PANTALLA_CASAS_FXML = "/fxml/FXMLCasas.fxml";



}
