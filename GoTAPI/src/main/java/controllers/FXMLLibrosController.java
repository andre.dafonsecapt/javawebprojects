package controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import models.Libro;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLLibrosController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    private Alert alert;

    @FXML
    private TextArea textArea;

    @FXML
    private TextField tfNumLibro;

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    @FXML
    public void getLibros() {
        try {
            if (tfNumLibro.getText().isBlank()) {

                Task<List<Libro>> tarea = new Task<>() {

                    @Override
                    protected List<Libro> call() throws Exception {

                        return mainController.getAppServices().loadLibros(12).execute().body();
                    }
                };

                tarea.setOnSucceeded(cadena -> textArea.setText(mainController.getAppServices().getGson().toJson(tarea.getValue())));
                tarea.setOnFailed(workerStateEvent ->
                        Logger.getLogger("FXMAIN")
                                .log(Level.SEVERE, "error ",
                                        workerStateEvent.getSource().getException()));
                new Thread(tarea).start();

            } else {
                int numLibro = Integer.parseInt(tfNumLibro.getText());

                Task<Libro> tarea = new Task<>() {

                    @Override
                    protected Libro call() throws Exception {

                        return mainController.getAppServices().loadLibro(numLibro).execute().body();
                    }
                };

                tarea.setOnSucceeded(cadena -> textArea.setText(mainController.getAppServices().getGson().toJson(tarea.getValue())));
                tarea.setOnFailed(workerStateEvent ->
                        Logger.getLogger("FXMAIN")
                                .log(Level.SEVERE, "error ",
                                        workerStateEvent.getSource().getException()));
                new Thread(tarea).start();
            }
        } catch (Exception e) {
            alert.setContentText("Error - Formato de datos introducidos inválido");
            alert.showAndWait();
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
    }
}
