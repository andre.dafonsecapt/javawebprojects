package controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import services.AppServices;
import utils.ConstantesFxml;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLMainController implements Initializable {

    @FXML
    private BorderPane fxRoot;

    private AppServices appServices;

    @FXML
    private ImageView imageView;

    AppServices getAppServices() {
        return appServices;
    }

    private AnchorPane pagPersonajes;
    private FXMLPersonajesController personajesController;

    private AnchorPane pagPersonajesFiltrado;
    private FXMLPersonajesFiltradoController personajesFiltradoController;

    private AnchorPane pagLibros;
    private FXMLLibrosController librosController;

    private AnchorPane pagCasas;
    private FXMLCasasController casasController;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        appServices = new AppServices();
        String path = "https://documentdesignfall17.files.wordpress.com/2017/08/game_of_thrones_logo_logotype_wordmark.png?w=616";
        Image image = new Image(path);
        imageView.setImage(image);
        imageView.setFitHeight(300);
        imageView.setFitWidth(300);

    }

    @FXML
    private void exit() {
        Alert alertConfirm = new Alert(Alert.AlertType.CONFIRMATION);
        alertConfirm.setTitle("Confirmation Dialog");
        alertConfirm.setHeaderText(null);
        ButtonType buttonTypeYes = new ButtonType("Yes");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alertConfirm.getButtonTypes().setAll(buttonTypeYes, buttonTypeCancel);
        alertConfirm.setContentText("Estás seguro que quieres salir?");
        Optional<ButtonType> result = alertConfirm.showAndWait();
        if (result.get() == buttonTypeYes) {
            Platform.exit();
        }
    }

    @FXML
    private void menuPersonajes() {
        try {
            if (pagPersonajes == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(ConstantesFxml.FXML_PANTALLA_Personajes_FXML));
                pagPersonajes = loaderMenu.load();
                personajesController = loaderMenu.getController();
                personajesController.setBorderPane(this);
            }
            personajesController.loadPagInicial(1, 10);
            fxRoot.setCenter(pagPersonajes);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void menuPersonajesFiltrado() {
        try {
            if (pagPersonajesFiltrado == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(ConstantesFxml.FXML_PANTALLA_PersonajesFiltrado_FXML));
                pagPersonajesFiltrado = loaderMenu.load();
                personajesFiltradoController = loaderMenu.getController();
                personajesFiltradoController.setBorderPane(this);
            }
            personajesFiltradoController.loadCombo();
            fxRoot.setCenter(pagPersonajesFiltrado);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void menuLibros() {
        try {
            if (pagLibros == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(ConstantesFxml.FXML_PANTALLA_LIBROS_FXML));
                pagLibros = loaderMenu.load();
                librosController = loaderMenu.getController();
                librosController.setBorderPane(this);
            }
            fxRoot.setCenter(pagLibros);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void menuCasas() {
        try {
            if (pagCasas == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(ConstantesFxml.FXML_PANTALLA_CASAS_FXML));
                pagCasas = loaderMenu.load();
                casasController = loaderMenu.getController();
                casasController.setBorderPane(this);
            }
            fxRoot.setCenter(pagCasas);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
