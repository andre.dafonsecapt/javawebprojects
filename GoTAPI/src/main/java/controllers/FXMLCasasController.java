package controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import models.Casa;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLCasasController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    private Alert alert;

    @FXML
    private TextArea textArea;

    @FXML
    private TextField tfNumCasa;

    @FXML
    private TextField tfNumPag;

    @FXML
    private TextField tfTamPag;

    @FXML
    private Label labelNumPag;

    @FXML
    private Label labelTamPag;

    @FXML
    private CheckBox cbMultiCasas;

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    @FXML
    public void enablePagElements() {
        if (cbMultiCasas.isSelected()) {
            tfNumPag.setDisable(false);
            tfNumPag.setVisible(true);
            tfTamPag.setDisable(false);
            tfTamPag.setVisible(true);
            labelNumPag.setDisable(false);
            labelNumPag.setVisible(true);
            labelTamPag.setDisable(false);
            labelTamPag.setVisible(true);
            tfNumCasa.setDisable(true);
        } else {
            tfNumPag.setDisable(true);
            tfNumPag.setVisible(false);
            tfTamPag.setDisable(true);
            tfTamPag.setVisible(false);
            labelNumPag.setDisable(true);
            labelNumPag.setVisible(false);
            labelTamPag.setDisable(true);
            labelTamPag.setVisible(false);
            tfNumCasa.setDisable(false);
        }
    }

    @FXML
    public void getCasas() {
        try {
            if (cbMultiCasas.isSelected()) {
                Integer numPag = null;
                Integer tamPag = null;
                if (!tfNumPag.getText().isBlank()) {
                    numPag = Integer.parseInt(tfNumPag.getText());
                }
                if (!tfTamPag.getText().isBlank()) {
                    tamPag = Integer.parseInt(tfTamPag.getText());
                }

                Integer finalNumPag = numPag;
                Integer finalTamPag = tamPag;

                Task<List<Casa>> tarea = new Task<>() {

                    @Override
                    protected List<Casa> call() throws Exception {

                        return mainController.getAppServices().loadCasas(finalNumPag, finalTamPag).execute().body();
                    }
                };

                tarea.setOnSucceeded(cadena -> textArea.setText(mainController.getAppServices().getGson().toJson(tarea.getValue())));
                tarea.setOnFailed(workerStateEvent ->
                        Logger.getLogger("FXMAIN")
                                .log(Level.SEVERE, "error ",
                                        workerStateEvent.getSource().getException()));
                new Thread(tarea).start();

            } else {
                int numCasa = Integer.parseInt(tfNumCasa.getText());

                Task<Casa> tarea = new Task<>() {

                    @Override
                    protected Casa call() throws Exception {

                        return mainController.getAppServices().loadCasa(numCasa).execute().body();
                    }
                };

                tarea.setOnSucceeded(cadena -> textArea.setText(mainController.getAppServices().getGson().toJson(tarea.getValue())));
                tarea.setOnFailed(workerStateEvent ->
                        Logger.getLogger("FXMAIN")
                                .log(Level.SEVERE, "error ",
                                        workerStateEvent.getSource().getException()));
                new Thread(tarea).start();
            }
        } catch (Exception e) {
            alert.setContentText("Error - Formato de datos introducidos inválido");
            alert.showAndWait();
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
    }
}