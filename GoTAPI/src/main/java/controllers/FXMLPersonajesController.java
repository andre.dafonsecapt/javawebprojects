package controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import models.Personaje;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLPersonajesController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    private Alert alert;

    @FXML
    private TextArea textArea;

    @FXML
    private TextField tfTamPag;

    @FXML
    private Label labelPag;

    private int numPag;
    private int tamPagInt;

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    void loadPagInicial(int numPag, int tamPagInt) {

        Task<List<Personaje>> tarea = new Task<>() {

            @Override
            protected List<Personaje> call() throws Exception {

                return mainController.getAppServices().loadPersonajes(numPag, tamPagInt).execute().body();
            }
        };

        tarea.setOnSucceeded(cadena -> {
            textArea.setText(mainController.getAppServices().getGson().toJson(tarea.getValue()));
            labelPag.setText("Pagina: " + numPag + "/" + ((2130 / tamPagInt) + 1));
        });
        tarea.setOnFailed(workerStateEvent ->
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException()));
        new Thread(tarea).start();
    }

    @FXML
    public void proximaPag() {
        try {
            Integer tamPag;
            if (!tfTamPag.getText().isBlank()) {
                tamPag = Integer.parseInt(tfTamPag.getText());
                tamPagInt = tamPag;
            }
            if (!textArea.getText().equals("[]")) {
                numPag++;
                loadPagInicial(numPag, tamPagInt);
            }
        } catch (Exception e) {
            alert.setContentText("Error - Formato de datos introducidos inválido");
            alert.showAndWait();
        }
    }

    @FXML
    public void anteriorPag() {
        try {
            Integer tamPag;
            if (!tfTamPag.getText().isBlank()) {
                tamPag = Integer.parseInt(tfTamPag.getText());
                tamPagInt = tamPag;
            }
            if (numPag != 1) {
                numPag--;
                loadPagInicial(numPag, tamPagInt);
            }
        } catch (Exception e) {
            alert.setContentText("Error - Formato de datos introducidos inválido");
            alert.showAndWait();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        numPag = 1;
        tamPagInt = 10;
    }
}
