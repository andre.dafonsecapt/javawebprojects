package controllers;

import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import models.Personaje;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class FXMLPersonajesFiltradoController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    private Alert alert;

    @FXML
    private TextArea textArea;

    @FXML
    private TextField tfTamPag;

    @FXML
    private TextField tfNombre;

    @FXML
    private TextField tfCultura;

    @FXML
    private ComboBox comboTemporada;

    @FXML
    private CheckBox cbVivo;

    @FXML
    private CheckBox cbMuerto;

    private int numPag;


    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    void loadCombo() {
        comboTemporada.getItems().clear();
        comboTemporada.getItems().addAll(FXCollections.observableArrayList("Season 1", "Season 2", "Season 3", "Season 4", "Season 5", "Season 6", "Season 7", "Season 8"));
        textArea.clear();
    }

    @FXML
    public void getPersonajes() {

        try {
            Integer tamPag = null;
            if (!tfTamPag.getText().isBlank()) {
                tamPag = Integer.parseInt(tfTamPag.getText());
            }
            String nombre = tfNombre.getText();
            String cultura = tfCultura.getText();
            String temporada = (String) comboTemporada.getSelectionModel().getSelectedItem();
            String vivoOMuerto = "";
            if (cbVivo.isSelected() && !cbMuerto.isSelected()) {
                vivoOMuerto = "true";
            } else if (!cbVivo.isSelected() && cbMuerto.isSelected()) {
                vivoOMuerto = "false";
            }

            String finalVivoOMuerto = vivoOMuerto;
            Integer finalTamPag = tamPag;

            Task<List<Personaje>> tarea = new Task<>() {

                @Override
                protected List<Personaje> call() throws Exception {

                    return mainController.getAppServices().loadPersonajesFiltrado(nombre, cultura, finalVivoOMuerto, numPag, finalTamPag).execute().body();
                }
            };

            tarea.setOnSucceeded(cadena -> {
                List<Personaje> personajesFiltradas = tarea.getValue();
                if (temporada != null) {
                    personajesFiltradas = tarea.getValue().stream().filter(personaje -> personaje.getTvSeries().contains(temporada)).collect(Collectors.toList());
                }
                textArea.setText(mainController.getAppServices().getGson().toJson(personajesFiltradas));
            });
            tarea.setOnFailed(workerStateEvent ->
                    Logger.getLogger("FXMAIN")
                            .log(Level.SEVERE, "error ",
                                    workerStateEvent.getSource().getException()));
            new Thread(tarea).start();
        } catch (Exception e) {
            alert.setContentText("Error - Formato de datos introducidos inválido");
            alert.showAndWait();
        }
    }

    @FXML
    public void proximaPag() {
        if (!textArea.getText().isBlank() && !textArea.getText().equals("[]")) {
            numPag++;
            getPersonajes();
        }
    }

    @FXML
    public void anteriorPag() {
        if (numPag >= 1) {
            numPag--;
            getPersonajes();
        }
    }

    @FXML
    public void quitarCombo() {
        comboTemporada.getSelectionModel().clearSelection();
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        numPag = 1;
    }
}
