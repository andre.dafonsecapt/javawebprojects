/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import okhttp3.HttpUrl;
import utils.Constantes;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author andre
 */
public class FXMLLoginController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    private Alert alert;

    @FXML
    private Label fxLabelTitle;

    @FXML
    private Label fxLabelUser;

    @FXML
    private Label fxLabelPass;

    @FXML
    private Label fxLabelCesar;

    @FXML
    private TextField tfUser;

    @FXML
    private PasswordField tfPassword;

    @FXML
    private TextField tfNumeroCesar;

    @FXML
    private Button fxButton;

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    @FXML
    public void validateLogin() {


        HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(Constantes.BASE_URL + Constantes.URL_LOGIN)).newBuilder();
        urlBuilder.addQueryParameter("user", tfUser.getText());
        urlBuilder.addQueryParameter("password", tfPassword.getText());
        urlBuilder.addQueryParameter("numeroCesar", tfNumeroCesar.getText());

        Task<String> tarea = new Task<>() {

            @Override
            protected String call() throws Exception {


                return mainController.getAppServices().getResponseString(urlBuilder);
            }
        };

        tarea.setOnSucceeded(cadena -> {
            String responseString = tarea.getValue().trim();
            if (responseString.equals("loginOK")) {
                alert.setContentText(tfUser.getText() + " ha iniciado la session!");
                alert.showAndWait();
                mainController.setNumeroCesar(Integer.parseInt(tfNumeroCesar.getText()));
                disableFields();
                mainController.enableMenu();
            } else {
                alert.setContentText(responseString);
                alert.showAndWait();
            }
        });
        tarea.setOnFailed(workerStateEvent ->
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException()));
        new Thread(tarea).start();

    }

    private void disableFields() {
        fxLabelTitle.setVisible(false);
        fxLabelUser.setVisible(false);
        fxLabelPass.setVisible(false);
        fxLabelCesar.setVisible(false);
        tfUser.setVisible(false);
        tfUser.setDisable(true);
        tfPassword.setVisible(false);
        tfPassword.setDisable(true);
        tfNumeroCesar.setVisible(false);
        tfNumeroCesar.setDisable(true);
        fxButton.setVisible(false);
        fxButton.setDisable(true);
    }

    void enableFields() {
        fxLabelTitle.setVisible(true);
        fxLabelUser.setVisible(true);
        fxLabelPass.setVisible(true);
        fxLabelCesar.setVisible(true);
        tfUser.setVisible(true);
        tfUser.setDisable(false);
        tfUser.clear();
        tfPassword.setVisible(true);
        tfPassword.setDisable(false);
        tfPassword.clear();
        tfNumeroCesar.setVisible(true);
        tfNumeroCesar.setDisable(false);
        tfNumeroCesar.clear();
        fxButton.setVisible(true);
        fxButton.setDisable(false);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
    }
}
