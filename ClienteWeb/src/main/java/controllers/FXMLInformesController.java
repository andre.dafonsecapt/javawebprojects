/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import okhttp3.HttpUrl;
import utils.CifraCesar;
import utils.Constantes;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author andre
 */
public class FXMLInformesController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    @FXML
    private ComboBox<String> fxCombo;

    @FXML
    private TextArea textArea;

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // metodo vacio pero necesario
    }

    void loadInformes() {
        textArea.clear();
        fxCombo.getItems().clear();
        fxCombo.getItems().addAll(FXCollections.observableArrayList("1", "2", "3", "4", "5"));
    }

    @FXML
    public void getInforme() {

        if (!fxCombo.getSelectionModel().isEmpty()) {

            HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(Constantes.BASE_URL + Constantes.URL_INFORMES)).newBuilder();
            String informeIndexCifrado = CifraCesar.cifrarString(fxCombo.getSelectionModel().getSelectedItem(), mainController.getNumeroCesar());
            urlBuilder.addQueryParameter("informeIndex", informeIndexCifrado);

            Task<String> tarea = new Task<>() {

                @Override
                protected String call() throws Exception {

                    return mainController.getAppServices().getResponseString(urlBuilder);
                }
            };

            tarea.setOnSucceeded(cadena -> {
                String responseString = tarea.getValue();
                textArea.setText(CifraCesar.descifrarString(responseString, mainController.getNumeroCesar()));
            });
            tarea.setOnFailed(workerStateEvent ->
                    Logger.getLogger("FXMAIN")
                            .log(Level.SEVERE, "error ",
                                    workerStateEvent.getSource().getException()));
            new Thread(tarea).start();

        }
    }
}
