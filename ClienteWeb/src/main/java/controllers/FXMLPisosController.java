/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import okhttp3.HttpUrl;
import utils.CifraCesar;
import utils.Constantes;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author andre
 */
public class FXMLPisosController implements Initializable {

    @FXML
    private FXMLMainController mainController;
    @FXML
    private TextArea fxTextArea;

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    void loadPisos(int numeroCesar) {

        HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(Constantes.BASE_URL + Constantes.URL_PISOS)).newBuilder();

        Task<String> tarea = new Task<>() {

            @Override
            protected String call() throws Exception {

                return mainController.getAppServices().getResponseString(urlBuilder);
            }
        };

        tarea.setOnSucceeded(cadena -> {
            String responseString = tarea.getValue().replace("[", "- ").replace("]", "").replace(",", "\r\n-");
            fxTextArea.setText(CifraCesar.descifrarString(responseString, numeroCesar));
        });
        tarea.setOnFailed(workerStateEvent ->
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException()));
        new Thread(tarea).start();

    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // metodo vacio pero necesario
    }

}
