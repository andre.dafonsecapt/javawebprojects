package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import services.AppServices;
import utils.ConstantesFxml;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLMainController implements Initializable {

    @FXML
    private BorderPane fxRoot;

    @FXML
    private Menu fxMenuTools;

    @FXML
    private MenuItem fxMenuItemLogout;

    @FXML
    private MenuItem fxMenuItemLogin;

    private int numeroCesar;
    private AppServices appServices;

    AppServices getAppServices() {
        return appServices;
    }

    private AnchorPane pagLogin;
    private FXMLLoginController loginController;
    private AnchorPane pagPisos;
    private FXMLPisosController pisosController;
    private AnchorPane pagDatosUsuario;
    private FXMLDatosUsuarioController datosUsuarioController;
    private AnchorPane pagInformes;
    private FXMLInformesController informesController;


    int getNumeroCesar() {
        return numeroCesar;
    }

    void setNumeroCesar(int numeroCesar) {
        this.numeroCesar = numeroCesar;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        appServices = new AppServices();
        menuLogin();

    }

    void enableMenu() {
        fxMenuTools.setDisable(false);
        fxMenuItemLogin.setDisable(true);
        fxMenuItemLogout.setDisable(false);
    }

    @FXML
    private void menuLogin() {

        try {
            if (pagLogin == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(ConstantesFxml.FXML_FXMLLOGIN_FXML));
                pagLogin = loaderMenu.load();
                loginController = loaderMenu.getController();
                loginController.setBorderPane(this);
            }
            loginController.enableFields();
            fxRoot.setCenter(pagLogin);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void logout() {
        Alert alertConfirm = new Alert(Alert.AlertType.CONFIRMATION);
        alertConfirm.setTitle("Confirmation Dialog");
        alertConfirm.setHeaderText(null);
        ButtonType buttonTypeYes = new ButtonType("Yes");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alertConfirm.getButtonTypes().setAll(buttonTypeYes, buttonTypeCancel);
        alertConfirm.setContentText("Are you sure you want to logout?");
        Optional<ButtonType> result = alertConfirm.showAndWait();
        if (result.get() == buttonTypeYes) {
            fxMenuTools.setDisable(true);
            fxMenuItemLogin.setDisable(false);
            fxMenuItemLogout.setDisable(true);
            menuLogin();
        }
    }

    @FXML
    private void menuPisos() {
        try {
            if (pagPisos == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(ConstantesFxml.FXML_FXMLPISOS_FXML));
                pagPisos = loaderMenu.load();
                pisosController = loaderMenu.getController();
                pisosController.setBorderPane(this);
            }
            pisosController.loadPisos(numeroCesar);
            fxRoot.setCenter(pagPisos);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void menuDatosUsuario() {
        try {
            if (pagDatosUsuario == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(ConstantesFxml.FXML_FXMLDATOSUSUARIO_FXML));
                pagDatosUsuario = loaderMenu.load();
                datosUsuarioController = loaderMenu.getController();
                datosUsuarioController.setBorderPane(this);
            }
            datosUsuarioController.loadDatosUsuario(numeroCesar);
            fxRoot.setCenter(pagDatosUsuario);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void menuInformes() {
        try {
            if (pagInformes == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(ConstantesFxml.FXML_FXMLINFORMES_FXML));
                pagInformes = loaderMenu.load();
                informesController = loaderMenu.getController();
                informesController.setBorderPane(this);
            }
            informesController.loadInformes();
            fxRoot.setCenter(pagInformes);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
