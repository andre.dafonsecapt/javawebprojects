/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import okhttp3.HttpUrl;
import utils.CifraCesar;
import utils.Constantes;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author andre
 */
public class FXMLDatosUsuarioController implements Initializable {

    @FXML
    private FXMLMainController mainController;
    @FXML
    private TextField tfUsuario;
    @FXML
    private TextField tfPassword;
    @FXML
    private TextField tfUbicacion;
    @FXML
    private TextField tfEdad;

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    void loadDatosUsuario(int numeroCesar) {

        HttpUrl.Builder urlBuilder = Objects.requireNonNull(HttpUrl.parse(Constantes.BASE_URL + Constantes.URL_DATOSUSUARIO)).newBuilder();

        Task<String> tarea = new Task<>() {

            @Override
            protected String call() throws Exception {

                return mainController.getAppServices().getResponseString(urlBuilder);
            }
        };

        tarea.setOnSucceeded(cadena -> {
            String[] responseString = tarea.getValue().split(",");
            tfUsuario.setText(CifraCesar.descifrarString(responseString[0], numeroCesar));
            tfPassword.setText(CifraCesar.descifrarString(responseString[1], numeroCesar));
            tfUbicacion.setText(CifraCesar.descifrarString(responseString[2], numeroCesar));
            tfEdad.setText(CifraCesar.descifrarString(responseString[3], numeroCesar));
        });
        tarea.setOnFailed(workerStateEvent ->
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException()));
        new Thread(tarea).start();
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // metodo vacio pero necesario
    }

}
