package services;

import okhttp3.*;

import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.Objects;

public class AppServices {

    private OkHttpClient clientOK;

    public AppServices() {
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        clientOK = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .build();
    }

    public String getResponseString(HttpUrl.Builder urlBuilder) throws IOException {

        String url = urlBuilder.build().toString();
        Request request = new Request.Builder()
                .url(url)
                .build();

        Call call = clientOK.newCall(request);
        Response response = call.execute();
        return Objects.requireNonNull(response.body()).string();
    }

}
