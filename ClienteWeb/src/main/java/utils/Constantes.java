package utils;

public class Constantes {

    private Constantes() {
    }

    public static final String URL_LOGIN = "/login";
    public static final String URL_PISOS = "/cifrado/ServletPisos";
    public static final String URL_DATOSUSUARIO = "/cifrado/ServletDatosUsuario";
    public static final String URL_INFORMES = "/cifrado/ServletInformes";
    public static final String BASE_URL = "http://localhost:8080/WebSecretaV2";
}
