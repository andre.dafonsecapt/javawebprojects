package utils;

public class ConstantesFxml {

    private ConstantesFxml() {
    }

    public static final String FXML_FXMLLOGIN_FXML = "/fxml/FXMLLogin.fxml";
    public static final String FXML_FXMLPISOS_FXML = "/fxml/FXMLPisos.fxml";
    public static final String FXML_FXMLDATOSUSUARIO_FXML = "/fxml/FXMLDatosUsuario.fxml";
    public static final String FXML_FXMLINFORMES_FXML = "/fxml/FXMLInformes.fxml";
    public static final String FXML_PANTALLA_MAIN_FXML = "/fxml/FXMLMain.fxml";



}
