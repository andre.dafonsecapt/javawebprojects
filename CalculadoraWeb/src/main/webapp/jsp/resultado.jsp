<%-- 
    Document   : resultado
    Created on : 22-Sep-2019, 14:50:24
    Author     : andre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultado</title>
    </head>
    <body>
        <h1>Calculadora Web</h1>
        <form name="myform" action="/miCalculadora/calculadora" method="POST">
            Introduce el 1º número: <input type="text" name="num1" /><br>
            Introduce el 2º número: <input type="text" name="num2" /><br>
            <br><input type="submit" value="+" name="op"/>
            <input type="submit" value="-" name="op"/>
            <input type="submit" value="/" name="op"/>
            <input type="submit" value="x" name="op"/><br>
            Resultado: <input type="text" value="<c:out value="${resultado}"/>" />
        </form>
    </body>
</html>
