package servlets;


import servicios.ServiciosProducto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Login", urlPatterns = {"/login"})
public class ServletLogin extends javax.servlet.http.HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");

        if(request.getParameter("faccionLogin") != null) {
            ServiciosProducto sp = new ServiciosProducto();
            String faccionLogin = request.getParameter("faccionLogin");
            String responseString;
            if (sp.faccionValida(faccionLogin)) {
                request.getSession().setAttribute("login", "OK");
                request.getSession().setAttribute("faccion", faccionLogin);
                responseString = "loginOK";
            } else {
                responseString = "Error: Faccion inválida.";
            }
            request.setAttribute("response", responseString);
        }

    }
}
