package servlets;

import models.Producto;
import servicios.ServiciosProducto;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Productos", urlPatterns = {"/private/productos"})
public class ServletProductos extends javax.servlet.http.HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServiciosProducto sp = new ServiciosProducto();
        if (req.getParameter("nombre") != null && req.getParameter("faccion") != null && req.getParameter("precio") != null) {
            String response = null;
            if (req.getSession().getAttribute("faccion").equals("DeathWatch")) {
                Producto producto = (Producto) req.getAttribute("producto");
                if (sp.update(producto)) {
                    response = "Producto actualizado con exito.";
                } else {
                    resp.setStatus(500);
                }
            } else {
                resp.setStatus(403);
            }
            req.setAttribute("response", response);
        } else {
            String nombre = "";
            if (req.getParameter("productoNombre") != null) {
                nombre = req.getParameter("productoNombre");
            }
            Double precio = null;
            if (req.getParameter("productoPrecio") != null) {
                precio = Double.parseDouble(req.getParameter("productoPrecio"));
            }
            String faccion = (String)req.getSession().getAttribute("faccion");
            req.setAttribute("response", sp.get(nombre, precio, faccion));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");
        Producto response = null;
            Producto producto = (Producto) req.getAttribute("producto");
            if (producto != null) {
                response = producto;
            } else {
                resp.setStatus(500);
            }
        req.setAttribute("response", response);

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");
        String response = null;
        if (req.getSession().getAttribute("faccion").equals("DeathWatch")) {
            ServiciosProducto sp = new ServiciosProducto();
            Producto producto = (Producto) req.getAttribute("producto");
            if (sp.update(producto)) {
                response = "Producto actualizado con exito.";
            } else {
                resp.setStatus(500);
            }
        } else {
            resp.setStatus(403);
        }
        req.setAttribute("response", response);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String response = null;
        if (req.getSession().getAttribute("faccion").equals("DeathWatch")) {
            ServiciosProducto sp = new ServiciosProducto();
            String nombre = req.getParameter("nombre");
            String faccion = req.getParameter("faccion");

            if (sp.delete(nombre, faccion)) {
                response = "Producto borrado con exito.";
            } else {
                resp.setStatus(500);
            }
        } else {
            resp.setStatus(403);
        }
        req.setAttribute("response", response);
    }

}
