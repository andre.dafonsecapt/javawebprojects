package dao;

import models.Producto;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProductosDAO {

    private static Map<String, List<Producto>> productosFacciones = new LinkedHashMap<>();

    static {
        List<Producto> productosRebeldes = new ArrayList<>();
        List<Producto> productosImperio = new ArrayList<>();
        List<Producto> productosNewOrder = new ArrayList<>();
        List<Producto> productosGuild = new ArrayList<>();

        productosGuild.add(new Producto("rastreador","guild",250));
        productosGuild.add(new Producto("rifle","guild",2500));
        productosGuild.add(new Producto("beskar","guild",25000));

        productosImperio.add(new Producto("tie","imperio",9999));
        productosImperio.add(new Producto("blaster","imperio",99));

        productosNewOrder.add(new Producto("casco","newOrder",50));
        productosNewOrder.add(new Producto("sombrero","newOrder",99));

        productosRebeldes.add(new Producto("Xwing","rebeldes",500000));
        productosRebeldes.add(new Producto("ion","rebledes",999));

        productosFacciones.put("guild",productosGuild);
        productosFacciones.put("rebeldes",productosRebeldes);
        productosFacciones.put("imperio",productosImperio);
        productosFacciones.put("newOrder",productosNewOrder);

    }

    public boolean faccionValida(String faccion)
    {
        return faccion.equals("DeathWatch") || productosFacciones.containsKey(faccion);
    }

    public List<Producto> get(String nombre, Double precio, String faccion){
        List<Producto> productosFiltrados = new ArrayList<>();
        if (!faccion.equals("DeathWatch")) {
            productosFiltrados = productosFacciones.get(faccion).stream().filter(producto -> null == precio || producto.getPrecio() < precio)
                    .filter(producto -> null == nombre || nombre.isEmpty() || producto.getNombre().equals(nombre))
                    .collect(Collectors.toList());
        }
        return productosFiltrados;
    }

    public boolean add(Producto producto){
        boolean succeso = false;
        List<Producto> productos = productosFacciones.get(producto.getFaccion());
        if (productos != null) {
            productos.add(producto);
            if (productosFacciones.replace(producto.getFaccion(), productosFacciones.get(producto.getFaccion()), productos)) {
                succeso = true;
            }
        }
        return succeso;
    }

    public boolean update(Producto producto){
        boolean succeso = false;
        List<Producto> productos = productosFacciones.get(producto.getFaccion());
        if (productos != null) {
            if (productos.contains(producto)) {
                productos.stream().map(producto1 -> {
                    if (producto1.equals(producto)){
                        producto1.setPrecio(producto.getPrecio());
                    }
                    return producto1;
                }).collect(Collectors.toList());
                if (productosFacciones.replace(producto.getFaccion(), productosFacciones.get(producto.getFaccion()), productos)) {
                    succeso = true;
                }
            }
        }
        return succeso;
    }

    public boolean delete(String nombre, String faccion){
        boolean succeso = false;
        Producto p = new Producto();
        p.setNombre(nombre);
        List<Producto> productosFaccion = productosFacciones.get(faccion);
        if (productosFaccion.remove(p)){
            productosFacciones.replace(faccion, productosFaccion);
            succeso = true;
        }
        return succeso;
    }
}
