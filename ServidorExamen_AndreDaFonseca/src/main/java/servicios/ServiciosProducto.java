package servicios;

import dao.ProductosDAO;
import models.Producto;

import java.util.List;

public class ServiciosProducto {

    private ProductosDAO dao;

    public ServiciosProducto() {
        dao = new ProductosDAO();
    }

    public boolean faccionValida(String faccion)
    {
        return dao.faccionValida(faccion);
    }

    public List<Producto> get(String nombre, Double precio, String faccion){
        return dao.get(nombre, precio, faccion);
    }

    public boolean add(Producto producto){
        return dao.add(producto);
    }

    public boolean update(Producto producto){
        return dao.update(producto);
    }

    public boolean delete(String nombre, String faccion){
        return dao.delete(nombre, faccion);
    }
}
