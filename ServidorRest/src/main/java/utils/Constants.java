package utils;

public class Constants {

    //---------------------------------------------------------------------------DB Constants

    //---------------------------------------------------------------------------Usuario Queries
    public static final String SELECT_USUARIOS_QUERY = "SELECT * FROM usuario WHERE isActivated = 1;";
    public static final String SELECT_USUARIO_QUERY = "SELECT * FROM usuario WHERE idUsuario = ?;";
    public static final String SELECT_USUARIOID_ByUsername_QUERY = "SELECT idUsuario FROM usuario WHERE username = ?;";
    public static final String SELECT_USUARIOCode_QUERY = "SELECT * FROM usuario WHERE code = ?;";
    public static final String SELECT_USUARIOName_QUERY = "SELECT * FROM usuario WHERE username = ?;";
    public static final String SELECT_DateByCode_QUERY = "SELECT date FROM usuario WHERE code = ?;";
    public static final String SELECT_USUARIOActivated_QUERY = "SELECT isActivated FROM usuario WHERE username = ?;";
    public static final String INSERT_USUARIO_QUERY = "INSERT INTO usuario (`username`, `password`, `code`, `isActivated`, `date`, `publicKey`) VALUES (?, ?, ?, 0, ?, ?);";
    public static final String DELETE_USUARIO_QUERY = "DELETE FROM usuario WHERE (`idUsuario` = ?);";
    public static final String UPDATE_USUARIO_QUERY = "UPDATE usuario SET `username` = ?, `password` = ? WHERE (`idUsuario` = ?);";
    public static final String UPDATE_USUARIOUsername_QUERY = "UPDATE usuario SET `username` = ? WHERE (`idUsuario` = ?);";
    public static final String UPDATE_USUARIOPassword_QUERY = "UPDATE usuario SET `password` = ? WHERE (`username` = ?);";
    public static final String UPDATE_USUARIOCode_Date_QUERY = "UPDATE usuario SET `code` = ? , `date` = ? WHERE (`username` = ?);";
    public static final String UPDATE_USUARIOActivated_QUERY = "UPDATE usuario SET `isActivated` = '1' WHERE (`code` = ?);";
    public static final String ALTER_AutoIncrement_USUARIO_QUERY = "ALTER TABLE usuario AUTO_INCREMENT = 1;";

    //---------------------------------------------------------------------------Musica Queries
    public static final String SELECT_MUSICAS_QUERY = "SELECT * FROM musica;";
    public static final String SELECT_MUSICA_QUERY = "SELECT * FROM musica WHERE idMusica = ?;";
    public static final String SELECT_MUSICA_LiveActs_QUERY = "SELECT * FROM liveact WHERE idMusica = ?;";
    public static final String INSERT_MUSICA_QUERY = "INSERT INTO musica (`nombre`, `artista`, `fechaGrabacion`, `fechaDB`) VALUES (?, ?, ?, ?);";
    public static final String DELETE_MUSICA_QUERY = "DELETE FROM musica WHERE (`idMusica` = ?);";
    public static final String UPDATE_MUSICA_QUERY = "UPDATE musica SET `nombre` = ?, `artista` = ?, `fechaGrabacion` = ?, `fechaDB` = ? WHERE (`idMusica` = ?);";
    public static final String ALTER_AutoIncrement_MUSICA_QUERY = "ALTER TABLE musica AUTO_INCREMENT = 1;";

    //---------------------------------------------------------------------------Agenda Queries
    public static final String SELECT_AGENDAS_QUERY = "SELECT * FROM agendasegura WHERE fk_idUsuario = ?;";
    public static final String INSERT_AGENDA_QUERY = "INSERT INTO agendasegura (`iv`, `salt`, `iteraciones`, `keySize`, `agenda`, `encryptKey`, `fk_idUsuario`) VALUES (?, ?, ?, ?, ?, ?, ?);";
    public static final String DELETE_AGENDA_QUERY = "DELETE FROM agendasegura WHERE (`idAgenda` = ?);";
    public static final String UPDATE_AGENDA_QUERY = "UPDATE agendasegura SET `iv` = ?, `salt` = ?, `iteraciones` = ?, `keySize` = ?, `agenda` = ? WHERE (`idAgenda` = ?);";
    public static final String ALTER_AutoIncrement_AGENDA_QUERY = "ALTER TABLE agendasegura AUTO_INCREMENT = 1;";

    //---------------------------------------------------------------------------Invitacion Queries
    public static final String SELECT_INVITACION_QUERY = "SELECT * FROM agenda_invitacion WHERE fk_idInvitado = ?;";
    public static final String INSERT_INVITACION_QUERY = "INSERT INTO agenda_invitacion (`iv`, `salt`, `iteraciones`, `keySize`, `agenda`, `encryptKey`, `firma`, `fk_idInvitado`, `fk_idInvitador`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
    public static final String ALTER_AutoIncrement_INVITACION_QUERY = "ALTER TABLE agenda_invitacion AUTO_INCREMENT = 1;";


    //---------------------------------------------------------------------------URL Constants
    public static final String URL_Code = "/code/";
    public static final String URL_MAIL = "api/mail";
    public static final String BASE_URL = "http://localhost:8080/ServidorRest/";


    public static final String UTF_8 = "UTF-8";
    public static final String REQ_MENSAJE = "mensaje";

    public Constants() {
    }
}
