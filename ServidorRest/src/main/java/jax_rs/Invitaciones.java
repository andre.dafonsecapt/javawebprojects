package jax_rs;

import models.AgendaInvitacion;
import models.ApiError;
import rest.FilterLogin;
import services.ServicesInvitacion;
import services.ServicesServidor;
import utils.Constants;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("invitacion")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@FilterLogin
public class Invitaciones {

    @Inject
    private ServicesInvitacion si;

    @Inject
    private ServicesServidor ss;

    @GET
    public List<AgendaInvitacion> getAgendas(@Context HttpServletResponse response, @Context HttpServletRequest request) {
        int idUsuario = (int) ss.getClaim(request, "idUsuario");
        return si.getAgendas(String.valueOf(idUsuario));
    }

    @POST
    public Response addAgenda(@NotNull AgendaInvitacion agenda, @Context HttpServletResponse resp, @Context HttpServletRequest request) {
        resp.setCharacterEncoding(Constants.UTF_8);
        String response;
        int idUsuario = (int) ss.getClaim(request, "idUsuario");
        agenda.setFk_idInvitador(Integer.parseInt(String.valueOf(idUsuario)));
        if (si.add(agenda)) {
            response = "Invitacion hecha con exito.";
        } else {
            ApiError apiError = new ApiError("ERROR INSERT AgendaInvitacion");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }
}
