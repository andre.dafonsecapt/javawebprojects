package jax_rs;

import models.AgendaSegura;
import models.ApiError;
import rest.FilterLogin;
import services.ServicesAgenda;
import services.ServicesServidor;
import utils.Constants;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("agenda")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@FilterLogin
public class Agendas {

    @Inject
    private ServicesAgenda sa;

    @Inject
    private ServicesServidor ss;

    @GET
    public List<AgendaSegura> getAgendas(@Context HttpServletResponse response, @Context HttpServletRequest request) {
        int idUsuario = (int) ss.getClaim(request, "idUsuario");
        return sa.getAgendas(String.valueOf(idUsuario));
    }

    @POST
    public Response addAgenda(@NotNull AgendaSegura agenda, @Context HttpServletResponse resp, @Context HttpServletRequest request) {
        resp.setCharacterEncoding(Constants.UTF_8);
        String response;
        int idUsuario = (int) ss.getClaim(request, "idUsuario");
        agenda.setFk_idUsuario(String.valueOf(idUsuario));
        if (sa.add(agenda)) {
            response = "Agenda añadida con exito.";
        } else {
            ApiError apiError = new ApiError("ERROR INSERT Agenda");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

    @PUT
    public Response updateMusica(@NotNull AgendaSegura agenda, @Context HttpServletResponse resp) {
        resp.setCharacterEncoding(Constants.UTF_8);
        String response;
            if (sa.update(agenda)) {
                response = "Agenda actualizada con exito.";
            } else {
                ApiError apiError = new ApiError("ERROR UPDATE Agenda");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(apiError)
                        .type(MediaType.APPLICATION_JSON_TYPE).build();
            }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

    @DELETE
    public Response deleteMusica(@NotNull @QueryParam("idAgenda") Integer idAgenda, @Context HttpServletResponse resp) {
        resp.setCharacterEncoding(Constants.UTF_8);
        String response;
        if (sa.delete(idAgenda)) {
            response = "Agenda borrada con exito";
        } else {
            ApiError apiError = new ApiError("ERROR DELETE Agenda");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }
}
