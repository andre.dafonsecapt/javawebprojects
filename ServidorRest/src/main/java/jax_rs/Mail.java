package jax_rs;


import config.Configuration;
import models.ApiError;
import models.Usuario;
import services.ServicesMail;
import services.ServicesUsuario;
import utils.Constants;
import utils.PasswordHash;
import utils.Utils;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("mail")
public class Mail {

    @Inject
    private ServicesUsuario servUsuario;

    @Inject
    private ServicesMail servMail;

    @GET
    @Path("/code/{code}")
    @Produces(MediaType.APPLICATION_JSON)
    public void activarCuenta(@NotBlank @PathParam("code") String code, @Context HttpServletRequest req, @Context HttpServletResponse resp, @Context UriInfo uriInfo) throws ServletException, IOException {
        resp.setCharacterEncoding(Constants.UTF_8);
        LocalDateTime codeDate = servUsuario.getDateByCode(code);
        int tiempoActivacion = Configuration.getInstance().getTiempoActivacion();
        if (ChronoUnit.SECONDS.between(codeDate, LocalDateTime.now()) <= tiempoActivacion) {
            if (servUsuario.activateUsuario(code)) {
                req.setAttribute(Constants.REQ_MENSAJE, "Tu cuenta ha sido correctamente activada");
            } else {
                req.setAttribute(Constants.REQ_MENSAJE, "Error - No se ha podido activar la cuenta.");
            }
        } else {
            req.setAttribute(Constants.REQ_MENSAJE, "<p>El codigo de activación ha expirado.</p> " +
                    "   <form name=\"myform\" action=\"/ServidorRest/api/mail\" method=\"POST\">\n" +
                    "       <input type=\"submit\" value=\"Reenviar código\" name=\"reset\"/>\n" +
                    "   </form>");
            req.getSession().setAttribute("code", code);
        }
        req.getRequestDispatcher("/autenticacion.jsp").forward(req, resp);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response resetPassword(@NotBlank @QueryParam("email") String email, @Context HttpServletRequest req, @Context HttpServletResponse resp) {
        resp.setCharacterEncoding(Constants.UTF_8);
        String response;
        Usuario u = new Usuario();
        u.setUsername(email);
        PasswordHash ph = new PasswordHash();
        try {
            String newPassword = Utils.randomAlphaNumeric(10);
            u.setPassword(ph.createHash(newPassword));
            if (servUsuario.updatePassword(u)) {
                if (servMail.mandarMail(u.getUsername(), "<html>Tu nueva contraseña: <copy><b>" + newPassword + "</b><copy></html>", "Password Reset")) {
                    response = "Tu nueva contraseña ha sido enviada para este correo.";
                } else {
                    ApiError apiError = new ApiError("ERROR SENDING MAIL");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                            .entity(apiError)
                            .type(MediaType.APPLICATION_JSON_TYPE).build();
                }
            } else {
                ApiError apiError = new ApiError("ERROR UPDATING DB PWD");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(apiError)
                        .type(MediaType.APPLICATION_JSON_TYPE).build();
            }
        } catch (Exception e) {
            ApiError apiError = new ApiError(e.getMessage());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

    @POST
    public void resendCode(@Context HttpServletRequest req, @Context HttpServletResponse resp) {
        resp.setCharacterEncoding(Constants.UTF_8);
        String code = (String) req.getSession().getAttribute("code");
        String userName = servUsuario.findUserByCode(code).getUsername();
        String newCode = Utils.randomBytes();

        if (servMail.mandarMail(userName, "<html><p>Has recibido un nuevo código de activación</p>Para activar la cuenta hace clique <a href=" + Configuration.getInstance().getBaseURL() + Constants.URL_MAIL + Constants.URL_Code + newCode + ">aqui</a></html>", "Reenvio de Código de Autenticación")) {

            servUsuario.updateCode_Date(newCode, userName);

            try (PrintWriter out = resp.getWriter()) {
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<meta http-equiv=\"Content-Type\" content=\"text/html\" charset=\"utf-8\"/>");
                out.println("<title>Reset Código</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Reset de código</h1>");
                out.println("<p>Un nuevo código de activación ha sido enviado a tu correo.</p>");
                out.println("</body>");
                out.println("</html>");
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
