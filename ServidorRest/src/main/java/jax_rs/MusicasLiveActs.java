package jax_rs;


import models.MusicaLiveGetDTO;
import rest.FilterLogin;
import services.ServicesMusica;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("musica/live")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@FilterLogin
public class MusicasLiveActs {


    @Inject
    private ServicesMusica sm;


    @GET
    @Path("/{idMusica}")
    public MusicaLiveGetDTO getMusica(@NotNull @PathParam("idMusica") Integer idMusica) {
        return sm.converterMusicaToMusicaLiveGetDTO(sm.get(idMusica));
    }
}
