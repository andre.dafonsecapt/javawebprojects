package jax_rs;


import models.ApiError;
import models.Musica;
import models.MusicaGetAndPostDTO;
import rest.FilterLogin;
import services.ServicesMusica;
import utils.Constants;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("musica")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@FilterLogin
public class Musicas {


    @Inject
    private ServicesMusica sm;

    @Inject
    private Validator validator;


    @GET
    public List<MusicaGetAndPostDTO> getMusicas(@Context HttpServletResponse response, @Context HttpServletRequest request) {
        return sm.converterListMusicaToListMusicaGetDTO(sm.getAll());
    }


    @GET
    @Path("/get/{idMusica}")
    public Response getMusica(@NotNull @PathParam("idMusica") Integer idMusica) {

        Musica m = sm.get(idMusica);
        if (m != null) {
            MusicaGetAndPostDTO mDTO = sm.converterMusicaToMusicaGetDTO(m);
            return Response.status(Response.Status.CREATED)
                    .entity(mDTO)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        } else {
            ApiError apiError = new ApiError("Error 404 - Musica not Found!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
    }

    @POST
    public Response addMusica(@NotNull Musica musica, @Context HttpServletResponse resp) {
        resp.setCharacterEncoding(Constants.UTF_8);
        String response;
        String errorString = validator.validate(musica).stream().map(
                testDtoConstraintViolation ->
                        testDtoConstraintViolation.getMessage())
                .collect(Collectors.joining("\n"));
        if (errorString.isEmpty()) {
            if (sm.add(musica)) {
                response = "Música añadida con exito.";
            } else {
                ApiError apiError = new ApiError("ERROR INSERT Musica");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(apiError)
                        .type(MediaType.APPLICATION_JSON_TYPE).build();
            }
        } else {
            ApiError apiError = new ApiError(errorString);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

    @PUT
    public Response updateMusica(@NotNull Musica musica, @Context HttpServletResponse resp) {
        resp.setCharacterEncoding(Constants.UTF_8);
        String response;
        String errorString = validator.validate(musica).stream().map(
                testDtoConstraintViolation ->
                        testDtoConstraintViolation.getMessage())
                .collect(Collectors.joining("\n"));
        if (errorString.isEmpty()) {
            if (sm.update(musica)) {
                response = "Música actualizada con exito.";
            } else {
                ApiError apiError = new ApiError("ERROR UPDATE Musica");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(apiError)
                        .type(MediaType.APPLICATION_JSON_TYPE).build();
            }
        } else {
            ApiError apiError = new ApiError(errorString);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

    @DELETE
    public Response deleteMusica(@NotNull @QueryParam("idMusica") Integer idMusica, @Context HttpServletResponse resp) {
        resp.setCharacterEncoding(Constants.UTF_8);
        String response;
        if (sm.delete(idMusica)) {
            response = "Música borrada con exito";
        } else {
            ApiError apiError = new ApiError("ERROR DELETE Musica");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

}
