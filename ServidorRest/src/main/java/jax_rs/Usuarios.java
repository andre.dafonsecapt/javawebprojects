package jax_rs;


import models.ApiError;
import models.Usuario;
import models.UsuarioGetDTO;
import rest.FilterLogin;
import services.ServicesMail;
import services.ServicesUsuario;
import utils.Constants;
import utils.PasswordHash;
import utils.Utils;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("usuario")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Usuarios {


    @Inject
    private ServicesUsuario su;

    @Inject
    private PasswordHash ph;

    @Inject
    private Validator validator;

    @GET
    @FilterLogin
    public List<UsuarioGetDTO> getUsers(@Context HttpServletResponse response, @Context HttpServletRequest request) {
        return su.converterListUsuarioToListUsuarioGetDTO(su.getAll());
    }


    @GET
    @Path("/get/{idUsuario}")
    @FilterLogin
    public Response getUser(@NotNull @PathParam("idUsuario") Integer idUsuario) {

        Usuario u = su.get(idUsuario);
        UsuarioGetDTO uDTO = su.converterUsuarioToGetDTO(u);
        if (uDTO != null) {
            return Response.status(Response.Status.CREATED)
                    .entity(uDTO)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        } else {
            ApiError apiError = new ApiError("Error 404 - Usuario not Found!");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
    }

    @POST
    public Response addUsuario(@NotNull Usuario usuario, @Context HttpServletRequest req, @Context HttpServletResponse resp) {
        resp.setCharacterEncoding(Constants.UTF_8);
        ServicesMail servicesMail = new ServicesMail();
        String response;
        String errorString = validator.validate(usuario).stream().map(
                testDtoConstraintViolation ->
                        testDtoConstraintViolation.getMessage())
                .collect(Collectors.joining("\n"));
        if (errorString.isEmpty()) {
            try {
                usuario.setPassword(ph.createHash(usuario.getPassword()));
                String code = Utils.randomBytes();
                usuario.setCode(code);
                if (su.add(usuario)) {
                    if (servicesMail.mandarMail(usuario.getUsername(), "<html>Para activar la cuenta hace clique <a href=" + Constants.BASE_URL + Constants.URL_MAIL + Constants.URL_Code + code + ">aqui</a></html>", "Código de Autenticación")) {
                        response = "Un codigo de activación ha sido enviado a tu correo.";
                    } else {
                        Usuario usuarioDB = su.findUser(usuario);
                        su.delete(usuarioDB.getIdUsuario());
                        ApiError apiError = new ApiError("ERROR SEND MAIL");
                        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                                .entity(apiError)
                                .type(MediaType.APPLICATION_JSON_TYPE).build();
                    }
                } else {
                    ApiError apiError = new ApiError("ERROR BD - No se ha podido añadir el usuario");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                            .entity(apiError)
                            .type(MediaType.APPLICATION_JSON_TYPE).build();
                }
            } catch (Exception e) {
                ApiError apiError = new ApiError("ERROR ADD USUARIO");
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                        .entity(apiError)
                        .type(MediaType.APPLICATION_JSON_TYPE).build();
            }
        } else {
            ApiError apiError = new ApiError(errorString);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

    @PUT
    @FilterLogin
    public Response updateUsuario(@NotNull Usuario usuario, @Context HttpServletResponse resp) {
        resp.setCharacterEncoding("UTF-8");
        String response;
        String errorString = validator.validate(usuario).stream().map(
                testDtoConstraintViolation ->
                        testDtoConstraintViolation.getMessage())
                .collect(Collectors.joining("\n"));
        if (errorString.isEmpty()) {
            if (!usuario.getPassword().equals("")) {
                //////////////////////////////////////////// actualiza la password del usuario
                try {
                    usuario.setPassword(ph.createHash(usuario.getPassword()));
                } catch (Exception e) {
                    ApiError apiError = new ApiError("ERROR SETTING PASSWORD");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                            .entity(apiError)
                            .type(MediaType.APPLICATION_JSON_TYPE).build();
                }
                if (su.update(usuario)) {
                    response = "Usuario actualizado con exito.";
                } else {
                    ApiError apiError = new ApiError("ERROR UPDATE USUARIO");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                            .entity(apiError)
                            .type(MediaType.APPLICATION_JSON_TYPE).build();
                }
            } else {
                //////////////////////////////////////////// actualiza el username del usuario
                if (su.updateUsername(usuario)) {
                    response = "Usuario actualizado con exito.";
                } else {
                    ApiError apiError = new ApiError("ERROR UPDATE USERNAME");
                    return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                            .entity(apiError)
                            .type(MediaType.APPLICATION_JSON_TYPE).build();
                }
            }
        } else {
            ApiError apiError = new ApiError(errorString);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

    @DELETE
    @FilterLogin
    public Response delUsuario(@NotNull @QueryParam("idUsuario") Integer idUsuario, @Context HttpServletResponse resp) {
        resp.setCharacterEncoding("UTF-8");
        String response;
        if (su.delete(idUsuario)) {
            response = "Usuario borrado con exito.";
        } else {
            ApiError apiError = new ApiError("ERROR DELETE Usuario");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build();
        }
        return Response.status(Response.Status.CREATED).entity(response).build();
    }

}
