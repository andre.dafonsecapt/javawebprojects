package dao;

import models.LiveAct;
import models.Musica;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.Constants;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RequestScoped
@Named
public class DaoMusica {


    public DaoMusica() {
        //empty constructor
    }

    public Musica get(int idMusica) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        Musica m;
        try {
            m = jtm.query(Constants.SELECT_MUSICA_QUERY,
                    new Object[]{idMusica},
                    BeanPropertyRowMapper.newInstance(Musica.class)).get(0);
        } catch (Exception e) {
            m = null;
        }
        if (m != null) {
            List<LiveAct> liveActs = jtm.query(Constants.SELECT_MUSICA_LiveActs_QUERY,
                    new Object[]{idMusica},
                    BeanPropertyRowMapper.newInstance(LiveAct.class));
            List<String> liveActsString = new ArrayList<>();
            if (liveActs != null && !liveActs.isEmpty()) {
                liveActs.forEach(liveAct -> liveActsString.add(liveAct.toString()));
            } else {
                liveActsString.add("Live Acts serán añadidos en un futuro cercano.");
            }
            m.setLiveActs(liveActsString);
        }
        return m;
    }

    public List<Musica> getAll() {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.query(Constants.SELECT_MUSICAS_QUERY,
                BeanPropertyRowMapper.newInstance(Musica.class));
    }

    public boolean add(Musica m) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        jtm.execute(Constants.ALTER_AutoIncrement_MUSICA_QUERY);
        boolean success = true;
        try {
            jtm.update(Constants.INSERT_MUSICA_QUERY, m.getNombre(), m.getArtista(), m.getFechaGrabacion(), m.getFechaDB());
        } catch (DuplicateKeyException e) {
            success = false;
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            success = false;
        }
        return success;
    }

    public boolean delete(int idMusica) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constants.DELETE_MUSICA_QUERY, idMusica) == 1;
    }

    public boolean update(Musica m) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constants.UPDATE_MUSICA_QUERY, m.getNombre(), m.getArtista(), m.getFechaGrabacion(), m.getFechaDB(), m.getIdMusica()) == 1;
    }
}
