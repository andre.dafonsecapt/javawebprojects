package dao;

import models.AgendaInvitacion;
import models.AgendaSegura;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOInvitacion {

    public DAOInvitacion() {
        // empty constructor
    }

    public List<AgendaInvitacion> getAgendas(String idInvitado) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        List<AgendaInvitacion> agendaInvitacionList = new ArrayList<>();
        try {
            agendaInvitacionList = jtm.query(Constants.SELECT_INVITACION_QUERY,
                    new Object[]{idInvitado},
                    BeanPropertyRowMapper.newInstance(AgendaInvitacion.class));
        } catch (Exception e){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return agendaInvitacionList;
    }

    public boolean add(AgendaInvitacion ai) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        jtm.execute(Constants.ALTER_AutoIncrement_INVITACION_QUERY);
        boolean success = true;
        try {
            jtm.update(Constants.INSERT_INVITACION_QUERY, ai.getIv(), ai.getSalt(), ai.getIteraciones(), ai.getKeySize(), ai.getAgenda(), ai.getEncryptKey(), ai.getFirma(), ai.getFk_idInvitado(), ai.getFk_idInvitador());
        } catch (DuplicateKeyException e) {
            success = false;
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            success = false;
        }
        return success;
    }
}
