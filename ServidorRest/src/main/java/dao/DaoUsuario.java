package dao;

import models.Usuario;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.Constants;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

@RequestScoped
@Named
public class DaoUsuario {


    public Usuario get(int idUsuario) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        Usuario u;
        try {
            u = jtm.query(Constants.SELECT_USUARIO_QUERY,
                    new Object[]{idUsuario},
                    BeanPropertyRowMapper.newInstance(Usuario.class)).get(0);
        } catch (Exception e) {
            u = null;
        }
        return u;
    }

    public int getIdUsuarioByUsername(String username){
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        try {
            return jtm.queryForObject(Constants.SELECT_USUARIOID_ByUsername_QUERY,
                    new Object[]{username},
                    Integer.class);
        } catch (Exception e){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            return -1;
        }
    }

    public List<Usuario> getAll() {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.query(Constants.SELECT_USUARIOS_QUERY,
                BeanPropertyRowMapper.newInstance(Usuario.class));
    }

    public LocalDateTime getDateByCode(String code) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return Objects.requireNonNull(jtm.queryForObject(Constants.SELECT_DateByCode_QUERY,
                new Object[]{code},
                Timestamp.class)).toLocalDateTime();
    }

    public boolean add(Usuario u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        jtm.execute(Constants.ALTER_AutoIncrement_USUARIO_QUERY);
        boolean success;
        try {
            jtm.update(Constants.INSERT_USUARIO_QUERY, u.getUsername(), u.getPassword(), u.getCode(), LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), u.getClavePublica());
            success = true;
        } catch (DuplicateKeyException e) {
            success = false;
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            success = false;
        }
        return success;
    }

    public boolean delete(int idUsuario) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constants.DELETE_USUARIO_QUERY, idUsuario) == 1;
    }

    public boolean update(Usuario u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constants.UPDATE_USUARIO_QUERY, u.getUsername(), u.getPassword(), u.getIdUsuario()) == 1;
    }

    public boolean updateUsername(Usuario u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constants.UPDATE_USUARIOUsername_QUERY, u.getUsername(), u.getIdUsuario()) == 1;
    }

    public boolean updatePassword(Usuario u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constants.UPDATE_USUARIOPassword_QUERY, u.getPassword(), u.getUsername()) == 1;
    }

    public boolean updateCode(String code, String userName) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constants.UPDATE_USUARIOCode_Date_QUERY, code, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), userName) == 1;
    }

    public Usuario findUser(Usuario u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.query(Constants.SELECT_USUARIOName_QUERY,
                new Object[]{u.getUsername()},
                BeanPropertyRowMapper.newInstance(Usuario.class)).stream().findFirst().orElse(null);
    }

    public Usuario findUserByCode(String code) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.query(Constants.SELECT_USUARIOCode_QUERY,
                new Object[]{code},
                BeanPropertyRowMapper.newInstance(Usuario.class)).stream().findFirst().orElse(null);
    }

    public boolean activateUsuario(String code) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constants.UPDATE_USUARIOActivated_QUERY, code) == 1;
    }

    public boolean isActivated(String username) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        int numberActive = Optional.ofNullable(jtm.queryForObject(Constants.SELECT_USUARIOActivated_QUERY, new Object[]{username}, Integer.class)).orElse(0);
        return numberActive > 0;
    }
}
