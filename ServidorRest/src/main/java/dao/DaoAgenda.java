package dao;

import models.AgendaSegura;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.Constants;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RequestScoped
@Named
public class DaoAgenda {


    public DaoAgenda() {
        //empty constructor
    }

    public List<AgendaSegura> getAgendas(String idUsuario) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        List<AgendaSegura> agendaSeguraList = new ArrayList<>();
        try {
            agendaSeguraList = jtm.query(Constants.SELECT_AGENDAS_QUERY,
                    new Object[]{idUsuario},
                    BeanPropertyRowMapper.newInstance(AgendaSegura.class));
        } catch (Exception e){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return agendaSeguraList;
    }

    public boolean add(AgendaSegura as) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        jtm.execute(Constants.ALTER_AutoIncrement_AGENDA_QUERY);
        boolean success = true;
        try {
            jtm.update(Constants.INSERT_AGENDA_QUERY, as.getIv(), as.getSalt(), as.getIteraciones(), as.getKeySize(), as.getAgenda(), as.getEncryptKey(), as.getFk_idUsuario());
        } catch (DuplicateKeyException e) {
            success = false;
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            success = false;
        }
        return success;
    }

    public boolean delete(int idAgenda) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        try {
        return jtm.update(Constants.DELETE_AGENDA_QUERY, idAgenda) == 1;
        } catch(Exception e){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public boolean update(AgendaSegura as) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        try {
            return jtm.update(Constants.UPDATE_AGENDA_QUERY, as.getIv(), as.getSalt(), as.getIteraciones(), as.getKeySize(), as.getAgenda(), as.getIdAgenda()) == 1;
        } catch(Exception e){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }
}
