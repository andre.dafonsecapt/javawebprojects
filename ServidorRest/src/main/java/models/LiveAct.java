package models;

public class LiveAct {

    private String lugar;
    private String pais;

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("Lugar: ").append(lugar);
        sb.append(" - País: ").append(pais);
        return sb.toString();
    }
}
