package models.converters;

import org.modelmapper.ModelMapper;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Singleton;


@RequestScoped
public class ModelMapperProducer {

    @Produces
    @Singleton
    public ModelMapper producesModelMapper() {
        return new ModelMapper();
    }

}
