package models.converters;

import models.Musica;
import models.MusicaGetAndPostDTO;
import models.MusicaLiveGetDTO;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;


@RequestScoped
public class MusicaConverter {

    @Inject
    private ModelMapper modelMapper;

    public MusicaGetAndPostDTO converterMusicaToMusicaGetDTO(Musica musica) {
        if (musica != null) {
            return modelMapper.map(musica, MusicaGetAndPostDTO.class);
        } else {
            return null;
        }
    }

    public Musica converterMusicaDTOToMusica(MusicaGetAndPostDTO musicaDTO) {
        if (musicaDTO != null) {
            return modelMapper.map(musicaDTO, Musica.class);
        } else {
            return null;
        }
    }

    public List<MusicaGetAndPostDTO> converterListMusicaToListMusicaGetDTO(List<Musica> musicas) {
        return musicas.stream()
                .map(musica -> modelMapper.map(musica, MusicaGetAndPostDTO.class))
                .collect(Collectors.toList());
    }

    public MusicaLiveGetDTO converterMusicaToMusicaLiveGetDTO(Musica musica) {
        if (musica != null) {
            return modelMapper.map(musica, MusicaLiveGetDTO.class);
        } else {
            return null;
        }
    }
}
