package models.converters;

import models.Usuario;
import models.UsuarioGetDTO;
import models.UsuarioPostDTO;
import org.modelmapper.ModelMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;


@RequestScoped
public class UsuarioConverter {

    @Inject
    private ModelMapper modelMapper;

    public UsuarioGetDTO converterUsuarioToUsuarioGetDTO(Usuario usuario) {
        if (usuario != null) {
            return modelMapper.map(usuario, UsuarioGetDTO.class);
        } else {
            return null;
        }
    }

    public List<UsuarioGetDTO> converterListUsuarioToListUsuarioGetDTO(List<Usuario> usuarios) {
        return usuarios.stream()
                .map(usuario -> modelMapper.map(usuario, UsuarioGetDTO.class))
                .collect(Collectors.toList());
    }

    public UsuarioPostDTO converterUsuarioToUsuarioPostDTO(Usuario usuario) {
        if (usuario != null) {
            return modelMapper.map(usuario, UsuarioPostDTO.class);
        } else {
            return null;
        }
    }
}
