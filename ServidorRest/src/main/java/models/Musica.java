package models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Musica {

    private int idMusica;
    @NotBlank(message = "Nombre no puede estar vacio.")
    private String nombre;
    @NotBlank(message = "Nombre no puede estar vacio.")
    private String artista;
    @PastOrPresent(message = "Fecha de Grabación no puede estar en el futuro.")
    private LocalDate fechaGrabacion;
    private LocalDateTime fechaDB;
    private List<String> liveActs;

    public Musica(int idMusica, String nombre, String artista, LocalDate fechaGrabacion, LocalDateTime fechaDB) {
        this.idMusica = idMusica;
        this.nombre = nombre;
        this.artista = artista;
        this.fechaGrabacion = fechaGrabacion;
        this.fechaDB = fechaDB;
        liveActs = new ArrayList<>();
    }

    public Musica() {
    }

    public int getIdMusica() {
        return idMusica;
    }

    public void setIdMusica(int idMusica) {
        this.idMusica = idMusica;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getArtista() {
        return artista;
    }

    public void setArtista(String artista) {
        this.artista = artista;
    }

    public LocalDate getFechaGrabacion() {
        return fechaGrabacion;
    }

    public void setFechaGrabacion(LocalDate fechaGrabacion) {
        this.fechaGrabacion = fechaGrabacion;
    }

    public LocalDateTime getFechaDB() {
        return fechaDB;
    }

    public void setFechaDB(LocalDateTime fechaDB) {
        this.fechaDB = fechaDB;
    }

    public List<String> getLiveActs() {
        return liveActs;
    }

    public void setLiveActs(List<String> liveActs) {
        this.liveActs = liveActs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Musica musica = (Musica) o;
        return nombre.equals(musica.nombre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombre);
    }
}
