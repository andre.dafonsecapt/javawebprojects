package servlets;

import models.ApiError;
import models.Usuario;
import services.ServicesServidor;
import services.ServicesUsuario;
import utils.PasswordHash;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.io.IOException;

@WebServlet(name = "Login", urlPatterns = {"/login"})
public class ServletLogin extends javax.servlet.http.HttpServlet {

    @Inject
    @RequestScoped
    private ServicesUsuario su;

    @Inject
    @RequestScoped
    private ServicesServidor ss;

    @Inject
    @RequestScoped
    private PasswordHash ph;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setCharacterEncoding("UTF-8");
        Usuario usuarioLogin;
        Jsonb jsonb = JsonbBuilder.create();
        try {
            usuarioLogin = jsonb.fromJson(req.getReader(), Usuario.class);
        } catch (Exception e) {
            usuarioLogin = null;
        }
        Usuario usuarioDB = su.findUser(usuarioLogin);
        String response = "";
        try {
            if (usuarioDB != null && usuarioLogin != null) {
                if (ph.validatePassword(usuarioLogin.getPassword(), usuarioDB.getPassword())) {
                    if (su.isActivated(usuarioLogin.getUsername())) {
                        int idUsuario = su.getIdUsuarioByUsername(usuarioLogin.getUsername());
                        resp.setHeader("JWT", ss.buildJWT(req, idUsuario));
                        response = "loginOK";
                    } else {
                        ApiError apiError = new ApiError("Error: Este usuario aún no ha sido activado.");
                        resp.setStatus(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
                        resp.getWriter();
                        jsonb.toJson(apiError, resp.getWriter());
                    }
                } else {
                    ApiError apiError = new ApiError("Error: Password inválida.");
                    resp.setStatus(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
                    resp.getWriter();
                    jsonb.toJson(apiError, resp.getWriter());
                }
            } else {
                ApiError apiError = new ApiError("Error: Usuario inválido.");
                resp.setStatus(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
                resp.getWriter();
                jsonb.toJson(apiError, resp.getWriter());
            }
        } catch (Exception e) {
            ApiError apiError = new ApiError(e.getMessage());
            resp.setStatus(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
            resp.getWriter();
            jsonb.toJson(apiError, resp.getWriter());
        }
        jsonb.toJson(response, resp.getWriter());
    }
}

