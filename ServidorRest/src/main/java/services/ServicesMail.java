/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;


import config.Configuration;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author oscar
 */
public class ServicesMail {


    public boolean mandarMail(String to, String msg, String subject) {
        boolean mailEnviado = false;
        try {
            Email email = new SimpleEmail();
            email.setHostName(Configuration.getInstance().getMailHost());
            email.setSmtpPort(Integer.parseInt(Configuration.getInstance().getMailPort()));
            email.setAuthentication(Configuration.getInstance().getMailUsername(), Configuration.getInstance().getMailPwd());
            //email.setSSLOnConnect(true);
            email.setStartTLSEnabled(true);
            email.setFrom(Configuration.getInstance().getMailUsername());
            email.setSubject(subject);
            email.setContent(msg, "text/html");
            email.addTo(to);
//            email.setHostName(Configuration.getInstance().getMailHost());
//            email.setSmtpPort(Configuration.getInstance().getMailPort());
//            email.setAuthentication(Configuration.getInstance().getMailUsername(), Configuration.getInstance().getPassword());
//            email.setSSLOnConnect(true);
////            email.setStartTLSEnabled(true);
//            email.setFrom(Configuration.getInstance().getMailUsername());
//            email.setSubject(subject);
//            email.setContent(msg, "text/html");
//            email.addTo(to);
            email.send();
            mailEnviado = true;
        } catch (EmailException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return mailEnviado;
    }
}