package services;

import dao.DaoMusica;
import models.Musica;
import models.MusicaGetAndPostDTO;
import models.MusicaLiveGetDTO;
import models.converters.MusicaConverter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@RequestScoped
@Named
public class ServicesMusica {

    @Inject
    private DaoMusica daoMusica;

    @Inject
    private MusicaConverter musicaConverter;

    public ServicesMusica() {
        this.daoMusica = new DaoMusica();
    }

    public Musica get(int idMusica) {
        return daoMusica.get(idMusica);
    }

    public List<Musica> getAll() {
        return daoMusica.getAll();
    }

    public boolean add(Musica musica) {
        return daoMusica.add(musica);
    }

    public boolean delete(int idMusica) {
        return daoMusica.delete(idMusica);
    }

    public boolean update(Musica musica) {
        return daoMusica.update(musica);
    }

    public MusicaGetAndPostDTO converterMusicaToMusicaGetDTO(Musica musica) {
        return musicaConverter.converterMusicaToMusicaGetDTO(musica);
    }

    public Musica converterMusicaDTOToMusica(MusicaGetAndPostDTO musicaDTO) {
        return musicaConverter.converterMusicaDTOToMusica(musicaDTO);
    }

    public List<MusicaGetAndPostDTO> converterListMusicaToListMusicaGetDTO(List<Musica> musicas) {
        return musicaConverter.converterListMusicaToListMusicaGetDTO(musicas);
    }

    public MusicaLiveGetDTO converterMusicaToMusicaLiveGetDTO(Musica musica) {
        return musicaConverter.converterMusicaToMusicaLiveGetDTO(musica);
    }
}
