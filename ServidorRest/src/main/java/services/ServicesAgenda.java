package services;

import dao.DaoAgenda;
import models.AgendaSegura;
import models.converters.MusicaConverter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@RequestScoped
@Named
public class ServicesAgenda {

    @Inject
    private DaoAgenda daoAgenda;

    public ServicesAgenda() {
        this.daoAgenda = new DaoAgenda();
    }

    public List<AgendaSegura> getAgendas(String idUsuario) {
        return daoAgenda.getAgendas(idUsuario);
    }

    public boolean add(AgendaSegura agenda) {
        return daoAgenda.add(agenda);
    }

    public boolean delete(int idAgenda) {
        return daoAgenda.delete(idAgenda);
    }

    public boolean update(AgendaSegura agenda) {
        return daoAgenda.update(agenda);
    }

}
