package services;

import dao.DAOInvitacion;
import models.AgendaInvitacion;

import javax.inject.Inject;
import java.util.List;

public class ServicesInvitacion {

    @Inject
    private DAOInvitacion daoInvitacion;

    public ServicesInvitacion() {
        daoInvitacion = new DAOInvitacion();
    }

    public List<AgendaInvitacion> getAgendas(String idUsuario) {
        return daoInvitacion.getAgendas(idUsuario);
    }

    public boolean add(AgendaInvitacion agenda) {
        return daoInvitacion.add(agenda);
    }
}
