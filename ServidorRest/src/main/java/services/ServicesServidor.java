package services;

import io.jsonwebtoken.*;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

@RequestScoped
@Named
public class ServicesServidor {

    public String buildJWT(HttpServletRequest req, int idUsuario) throws Exception {
        //recupera la clave privada y crea el JWT para poner en cabezera
        PrivateKey clavePrivada = getPrivateKey(req);
        return buildJWT(clavePrivada, idUsuario);
    }

    public Object getClaim(HttpServletRequest request, String claimString) {
        Jws<Claims> jws = retrieveJWT(request);
        if (jws != null) {
            return jws.getBody().get(claimString, Object.class);
        } else {
            return null;
        }
    }

    public boolean verifyJWT(HttpServletRequest request) {
        return retrieveJWT(request) != null;
    }

    private Jws<Claims> retrieveJWT(HttpServletRequest request) {
        String jwsString = request.getHeader("JWT");
        PublicKey clavePublica = getPublicKey(request);
        Jws<Claims> jws;
        try {
            jws = Jwts.parserBuilder() // (1)
                    .setSigningKey(clavePublica) // (2)
                    .build()
                    .parseClaimsJws(jwsString); // (3)
            // we can safely trust the JWT
        } catch (JwtException ex) {       // (4)
            return null;
        }
        return jws;
    }

    private PublicKey getPublicKey(HttpServletRequest request) {
        /**
         * * 4 Recuperar clave PUBLICA del fichero
         */
        // 4.1 Leer datos binarios x509
        PublicKey clavePublica = null;
        try {
            byte[] bufferPub = new byte[5000];
            InputStream in = request.getServletContext().getResourceAsStream("/WEB-INF/serverJWT.publica");
            int chars = in.read(bufferPub, 0, 5000);
            in.close();

            byte[] bufferPub2 = new byte[chars];
            System.arraycopy(bufferPub, 0, bufferPub2, 0, chars);
            // 4.2 Recuperar clave publica desde datos codificados en formato X509
            KeyFactory keyFactoryRSA = KeyFactory.getInstance("RSA");
            X509EncodedKeySpec clavePublicaSpec = new X509EncodedKeySpec(bufferPub);
            clavePublica = keyFactoryRSA.generatePublic(clavePublicaSpec);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        return clavePublica;
    }

    private PrivateKey getPrivateKey(HttpServletRequest req) throws Exception {

        /*
         * 2 Recuperar clave Privada del fichero
         */
        // 2.1 Leer datos binarios PKCS8
        PKCS8EncodedKeySpec clavePrivadaSpec = null;
        byte[] bufferPriv = new byte[5000];
        InputStream in = req.getServletContext().getResourceAsStream("/WEB-INF/serverJWT.privada");
        try {
            int chars = in.read(bufferPriv, 0, 5000);

            in.close();
            byte[] bufferPriv2 = new byte[chars];
            System.arraycopy(bufferPriv, 0, bufferPriv2, 0, chars);

            // 2.2 Recuperar clave privada desde datos codificados en formato PKCS8
            clavePrivadaSpec = new PKCS8EncodedKeySpec(bufferPriv2);
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        /*
          * Crear KeyFactory (depende del provider) usado para las
          transformaciones de claves
         */
        KeyFactory keyFactoryRSA = KeyFactory.getInstance("RSA");

        return keyFactoryRSA.generatePrivate(clavePrivadaSpec);
    }

    private String buildJWT(PrivateKey clavePrivada, int idUsuario) {
        return Jwts.builder()
                .setIssuer("AndreDaFonseca")
                .setSubject("myFirstJWT")
                .claim("idUsuario", idUsuario)
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(LocalDate.of(2020, 10, 10).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .signWith(clavePrivada, SignatureAlgorithm.RS256)
                .compact();
    }
}
