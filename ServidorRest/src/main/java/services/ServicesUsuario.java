package services;

import dao.DaoUsuario;
import models.Usuario;
import models.UsuarioGetDTO;
import models.UsuarioPostDTO;
import models.converters.UsuarioConverter;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.List;

@RequestScoped
@Named
public class ServicesUsuario {

    @Inject
    private DaoUsuario daoUsuario;

    @Inject
    private UsuarioConverter usuarioConverter;

    public ServicesUsuario() {
        this.daoUsuario = new DaoUsuario();
    }

    public Usuario get(int idUsuario) {
        return daoUsuario.get(idUsuario);
    }

    public int getIdUsuarioByUsername(String username){
        return daoUsuario.getIdUsuarioByUsername(username);
    }

    public List<Usuario> getAll() {
        return daoUsuario.getAll();
    }

    public LocalDateTime getDateByCode(String code) {
        return daoUsuario.getDateByCode(code);
    }

    public boolean add(Usuario usuario) {
        return daoUsuario.add(usuario);
    }

    public boolean delete(int idUsuario) {
        return daoUsuario.delete(idUsuario);
    }

    public boolean update(Usuario usuario) {
        return daoUsuario.update(usuario);
    }

    public boolean updateUsername(Usuario usuario) {
        return daoUsuario.updateUsername(usuario);
    }

    public boolean updatePassword(Usuario usuario) {
        return daoUsuario.updatePassword(usuario);
    }

    public boolean updateCode_Date(String code, String userName) {
        return daoUsuario.updateCode(code, userName);
    }

    public Usuario findUser(Usuario usuario) {
        return daoUsuario.findUser(usuario);
    }

    public Usuario findUserByCode(String code) {
        return daoUsuario.findUserByCode(code);
    }

    public boolean activateUsuario(String code) {
        return daoUsuario.activateUsuario(code);
    }

    public boolean isActivated(String username) {
        return daoUsuario.isActivated(username);
    }

    public UsuarioGetDTO converterUsuarioToGetDTO(Usuario usuario) {
        return usuarioConverter.converterUsuarioToUsuarioGetDTO(usuario);
    }

    public List<UsuarioGetDTO> converterListUsuarioToListUsuarioGetDTO(List<Usuario> usuarios) {
        return usuarioConverter.converterListUsuarioToListUsuarioGetDTO(usuarios);
    }

    public UsuarioPostDTO converterUsuarioToPostDTO(Usuario usuario) {
        return usuarioConverter.converterUsuarioToUsuarioPostDTO(usuario);
    }
}
