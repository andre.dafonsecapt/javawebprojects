package config;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;

public class Configuration {

    private static Configuration config;

    private Configuration() {

    }

    public static Configuration getInstance() {

        return config;
    }

    public static Configuration getInstance(InputStream file) {
        if (config == null) {
            Yaml yaml = new Yaml();
            config = yaml.loadAs(
                    file,
                    Configuration.class);

        }
        return config;
    }

    private String user;
    private String password;
    private String urlDB;
    private String driver;
    private String baseURL;
    private int tiempoActivacion;
    private String mailUsername;
    private String mailPwd;
    private String mailHost;
    private String mailPort;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrlDB() {
        return urlDB;
    }

    public void setUrlDB(String urlDB) {
        this.urlDB = urlDB;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }

    public int getTiempoActivacion() {
        return tiempoActivacion;
    }

    public void setTiempoActivacion(int tiempoActivacion) {
        this.tiempoActivacion = tiempoActivacion;
    }

    public String getMailUsername() {
        return mailUsername;
    }

    public void setMailUsername(String mailUsername) {
        this.mailUsername = mailUsername;
    }

    public String getMailPwd() {
        return mailPwd;
    }

    public void setMailPwd(String mailPwd) {
        this.mailPwd = mailPwd;
    }

    public String getMailHost() {
        return mailHost;
    }

    public void setMailHost(String mailHost) {
        this.mailHost = mailHost;
    }

    public String getMailPort() {
        return mailPort;
    }

    public void setMailPort(String mailPort) {
        this.mailPort = mailPort;
    }
}
