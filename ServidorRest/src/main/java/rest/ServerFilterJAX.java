package rest;

import lombok.SneakyThrows;
import models.ApiError;
import services.ServicesServidor;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;


@Provider
@FilterLogin
public class ServerFilterJAX implements ContainerRequestFilter {

    @Inject
    private ServicesServidor ss;

    @Context
    private HttpServletRequest request;

    @SneakyThrows
    @Override
    public void filter(ContainerRequestContext containerRequestContext) {

        if (!ss.verifyJWT(request)) {
            ApiError apiError = new ApiError("JWT Exception - FORBIDDEN");
            containerRequestContext.abortWith(Response.status(Response.Status.FORBIDDEN)
                    .entity(apiError)
                    .type(MediaType.APPLICATION_JSON_TYPE).build());
        }
    }
}
