/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author andre
 */
@WebServlet(name = "VisitasServlet", urlPatterns = {"/VisitasServlet"})
public class VisitasServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // contador de visitas
        int visitas = 2;
        if (null != request.getSession().getAttribute("contador")) {
            visitas = (Integer) request.getSession().getAttribute("contador");
            visitas++;
        }
        // tiempo entre visitas
        LocalTime timer = (LocalTime) request.getSession().getAttribute("timer");
        if (null != timer) {
            Duration duration = Duration.between(timer, LocalTime.now());
            request.setAttribute("duration", duration.toMillis() / 1000.0f);
            request.getSession().setAttribute("timer", LocalTime.now());
        } else {
            request.getSession().setAttribute("timer", LocalTime.now());
        }
        // cerrar session y hacer reset a valores
        if (null != request.getParameter("reset")) {
            request.getSession().setAttribute("contador", 1);
            request.getSession().setAttribute("timer", null);
            request.getRequestDispatcher("index.html").forward(request, response);
        } else {
            request.getSession().setAttribute("contador", visitas);
            request.setAttribute("contador", visitas);
            request.getRequestDispatcher("jsp/visitas.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
