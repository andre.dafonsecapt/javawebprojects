<%-- 
    Document   : visitas
    Created on : 25-Sep-2019, 19:41:56
    Author     : andre
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Contador de Visitas</h1>
        <p>Numero de visitas: <c:out value="${contador}"/></p>
        <p>Tiempo entre visitas (segundos): <c:out value="${duration}"/></p>
        <form name="myform" action="/SessionEjercicios/VisitasServlet" method="POST">
            <input type="submit" value="Cargar otra vez"/>
            <input type="submit" value="Cerrar Session" name="reset"/>
        </form>
    </body>
</html>
