<%-- 
    Document   : telefone
    Created on : 26 sept. 2019, 10:18:40
    Author     : andre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form name="myform" action="/SessionEjercicios/TelefonoServlet" method="POST">
            Introduz tu télefono: <input type="text" name="telefono"/>
            <input type="submit" value="Submit"/>
        </form>
        <p><c:out value="${error}"/></p>
    </body>
</html>
