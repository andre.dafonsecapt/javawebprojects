<%-- 
    Document   : print
    Created on : 26-Sep-2019, 19:18:25
    Author     : andre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Formulario</h1>
        <p>Nombre: <c:out value="${nombre}"/></p>
        <p>Telefono: <c:out value="${telefono}"/></p>
        <p>Email: <c:out value="${email}"/></p>
        <a href="../SessionEjercicios/index.html">menu</a>
    </body>
</html>
