package utils;

public class Constantes {

    //---------------------------------------------------------------------------Client Constants
    public static final String URL_USUARIOS = "private/usuarios";
    public static final String URL_LOGIN = "login";
    public static final String URL_MAIL = "mail";
    public static final String URL_NUEVACUENTA = "nuevacuenta";
    public static final String URL_CodeQuery = "?code=";
    public static final String BASE_URL = "http://localhost:8080/ServidorRest/";

    //---------------------------------------------------------------------------DB Constants
    public static final String SELECT_USUARIOS_QUERY = "SELECT * FROM usuario WHERE isActivated = 1;";
    public static final String SELECT_USUARIO_QUERY = "SELECT * FROM usuario WHERE idUsuario = ?;";
    public static final String SELECT_USUARIOCode_QUERY = "SELECT * FROM usuario WHERE code = ?;";
    public static final String SELECT_USUARIOName_QUERY = "SELECT * FROM usuario WHERE username = ?;";
    public static final String SELECT_DateByCode_QUERY = "SELECT date FROM usuario WHERE code = ?;";
    public static final String SELECT_USUARIOActivated_QUERY = "SELECT EXISTS(SELECT * FROM usuario WHERE username = ? AND isActivated = 1);";
    public static final String INSERT_USUARIO_QUERY = "INSERT INTO `andre_fonseca_apidb`.`usuario` (`username`, `password`, `code`, `isActivated`, `date`) VALUES (?, ?, ?, 0, ?);";
    public static final String DELETE_USUARIO_QUERY = "DELETE FROM usuario WHERE (`idUsuario` = ?);";
    public static final String UPDATE_USUARIO_QUERY = "UPDATE usuario SET `username` = ?, `password` = ? WHERE (`idUsuario` = ?);";
    public static final String UPDATE_USUARIOUsername_QUERY = "UPDATE usuario SET `username` = ? WHERE (`idUsuario` = ?);";
    public static final String UPDATE_USUARIOPassword_QUERY = "UPDATE usuario SET `password` = ? WHERE (`username` = ?);";
    public static final String UPDATE_USUARIOCode_Date_QUERY = "UPDATE usuario SET `code` = ? , `date` = ? WHERE (`username` = ?);";
    public static final String UPDATE_USUARIOActivated_QUERY = "UPDATE usuario SET `isActivated` = '1' WHERE (`code` = ?);";
    public static final String ALTER_AutoIncrement_QUERY = "ALTER TABLE usuario AUTO_INCREMENT = 1;";

    //---------------------------------------------------------------------------FXML Constants
    public static final String FXML_LOGIN = "/fxml/FXMLLogin.fxml";
    public static final String FXML_GESTION = "/fxml/FXMLUsuarios.fxml";
    public static final String FXML_PANTALLA_MAIN_FXML = "/fxml/FXMLMain.fxml";
    public static final String FXML_NEWUSUARIO = "/fxml/FXMLNewUsuario.fxml";
    public static final String FXML_NEWPASSWORD = "/fxml/FXMLNewPassword.fxml";

    public Constantes() {
    }

}
