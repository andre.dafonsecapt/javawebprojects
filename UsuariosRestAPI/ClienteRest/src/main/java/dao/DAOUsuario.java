package dao;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import models.Usuario;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import retrofit.RestAPI;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import utils.Constantes;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.time.LocalDateTime;
import java.util.List;

public class DAOUsuario implements RestAPI {

    private Gson gson;
    private RestAPI restAPI;

    public DAOUsuario() {
        gson = new GsonBuilder()
                .setLenient()
                .setPrettyPrinting()
                .create();
        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        OkHttpClient clientOK = new OkHttpClient.Builder()
                .cookieJar(new JavaNetCookieJar(cookieManager))
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constantes.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(clientOK)
                .build();
        restAPI = retrofit.create(RestAPI.class);
    }

    public Gson getGson() {
        return gson;
    }

    // ----------------------------------------------------------------------------------- Usuario Calls
    @Override
    public Call<Usuario> get(int idUsuario) {
        return restAPI.get(idUsuario);
    }

    @Override
    public Call<List<Usuario>> getAll() {
        return restAPI.getAll();
    }

    @Override
    public Call<String> add(Usuario usuario) {
        return restAPI.add(usuario);
    }

    @Override
    public Call<String> delete(int idUsuario) {
        return restAPI.delete(idUsuario);
    }

    @Override
    public Call<String> update(Usuario usuario) {
        return restAPI.update(usuario);
    }

    // ----------------------------------------------------------------------------------- Login Calls
    @Override
    public Call<String> login(Usuario usuario) {
        return restAPI.login(usuario);
    }

    // ----------------------------------------------------------------------------------- Mail Calls
    @Override
    public Call<String> sendMail(String mail) {
        return restAPI.sendMail(mail);
    }
}
