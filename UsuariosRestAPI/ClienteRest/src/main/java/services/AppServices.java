package services;

import com.google.gson.Gson;
import dao.DAOUsuario;
import models.Usuario;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;

public class AppServices {

    private DAOUsuario daoUsuario;

    public AppServices() {
        daoUsuario = new DAOUsuario();
    }

    public Gson getGson() {
        return daoUsuario.getGson();
    }

    // ----------------------------------------------------------------------------------- Usuario Methods
    public Usuario getUsuario(int idUsuario) throws IOException {
        Usuario u;
        Call<Usuario> call = daoUsuario.get(idUsuario);
        Response<Usuario> resp = call.execute();
        if (resp.isSuccessful()) {
            u = resp.body();
        } else {
            u = null;
        }
        return u;
    }

    public List<Usuario> getUsuarios() throws IOException {
        List<Usuario> usuarios;
        Call<List<Usuario>> call = daoUsuario.getAll();
        Response<List<Usuario>> resp = call.execute();
        if (resp.isSuccessful()) {
            usuarios = resp.body();
        } else {
            usuarios = null;
        }
        return usuarios;
    }

    public String addUsuario(Usuario usuario) throws IOException {
        String addUsuario;
        Call<String> call = daoUsuario.add(usuario);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            addUsuario = resp.body();
        } else {
            addUsuario = "Error " + resp.code() + " - Un usuario ya existe con el email introducido.";
        }
        return addUsuario;
    }

    public String deleteUsuario(int idUsuario) throws IOException {
        String deleteUsuario;
        Call<String> call = daoUsuario.delete(idUsuario);
        Response<String> resp = call.execute();
        if (resp.isSuccessful()) {
            deleteUsuario = resp.body();
        } else {
            deleteUsuario = "Error " + resp.code() + " - No se ha podido borrar el usuário.";
        }
        return deleteUsuario;
    }

    public String updateUsuario(Usuario usuario) throws IOException {
        String updateUsuario;
        Response<String> resp = daoUsuario.update(usuario).execute();
        if (resp.isSuccessful()) {
            updateUsuario = resp.body();
        } else {
            updateUsuario = "Error " + resp.code() + " - No se ha podido actualizar el usuário.";
        }
        return updateUsuario;
    }

    // ----------------------------------------------------------------------------------- Login Methods
    public String loginUsuario(Usuario usuario) throws IOException {
        String loginUsuario;
        Response<String> resp = daoUsuario.login(usuario).execute();
        if (resp.isSuccessful()) {
            loginUsuario = resp.body();
        } else {
            loginUsuario = resp.code() + "";
        }
        return loginUsuario;
    }

    // ----------------------------------------------------------------------------------- Mail Methods
    public String sendMail(String mail) throws IOException {
        String sendMail;
        Response<String> resp = daoUsuario.sendMail(mail).execute();
        if (resp.isSuccessful()) {
            sendMail = resp.body();
        } else {
            if ((resp.code()+"").equals("404")){
                sendMail = "Error: "+resp.code() + " - El email introducido no está registrado.";
            } else {
                sendMail = "Error " + resp.code() + " - No se ha podido enviar el correo.";
            }

        }
        return sendMail;
    }
}
