package retrofit;


import models.Usuario;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.*;
import utils.Constantes;

import java.util.List;

public interface RestAPI {


    // -----------------------------------------------------------------Usuario servlet queries
    @GET("api/usuario/get/{idUsuario}")
    Call<Usuario> get(@Path("idUsuario") int idUsuario);

    @GET("api/usuario")
    Call<List<Usuario>> getAll();

    @DELETE("api/usuario")
    Call<String> delete(@Query("idUsuario") int idUsuario);

    @PUT("api/usuario")
    Call<String> update(@Body Usuario usuario);

    // -----------------------------------------------------------------Nueva Cuenta servlet queries
    @POST(Constantes.URL_NUEVACUENTA)
    Call<String> add(@Body Usuario usuario);

    // -----------------------------------------------------------------login servlet queries
    @POST(Constantes.URL_LOGIN)
    Call<String> login(@Body Usuario usuario);

    // -----------------------------------------------------------------Mail servlet queries
    @GET(Constantes.URL_MAIL)
    Call<String> sendMail(@Query("email") String mail);
}
