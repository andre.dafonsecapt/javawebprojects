/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import models.Usuario;
import utils.Constantes;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author andre
 */
public class FXMLLoginController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    private Alert alert;

    @FXML
    private Label fxLabelTitle;

    @FXML
    private Label fxLabelUser;

    @FXML
    private Label fxLabelPass;

    @FXML
    private Label fxLabelNewCuenta;

    @FXML
    private Label fxLabelNewPassword;

    @FXML
    private TextField tfUser;

    @FXML
    private PasswordField tfPassword;

    @FXML
    private Button fxButton;

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
    }

    @FXML
    public void validateLogin() {

        Task<String> tarea = new Task<String>() {
            @Override
            protected String call() throws Exception {
                return mainController.getAppServices().loginUsuario(new Usuario(0, tfUser.getText(), tfPassword.getText()));
            }
        };
        tarea.setOnSucceeded(cadena -> {
            String responseString = tarea.getValue();
            if (responseString.equals("loginOK")) {
                alert.setContentText(tfUser.getText() + " ha iniciado la session!");
                alert.showAndWait();
                disableFields();
                mainController.enableMenu();
            } else {
                alert.setContentText(responseString);
                alert.showAndWait();
            }
        });
        tarea.setOnFailed(workerStateEvent ->
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException()));
        new Thread(tarea).start();
    }

    private void disableFields() {
        fxLabelTitle.setVisible(false);
        fxLabelUser.setVisible(false);
        fxLabelPass.setVisible(false);
        fxLabelNewCuenta.setVisible(false);
        fxLabelNewCuenta.setDisable(true);
        fxLabelNewPassword.setVisible(false);
        fxLabelNewPassword.setDisable(true);
        tfUser.setVisible(false);
        tfUser.setDisable(true);
        tfPassword.setVisible(false);
        tfPassword.setDisable(true);
        fxButton.setVisible(false);
        fxButton.setDisable(true);
    }

    void enableFields() {
        fxLabelTitle.setVisible(true);
        fxLabelUser.setVisible(true);
        fxLabelPass.setVisible(true);
        fxLabelNewCuenta.setVisible(true);
        fxLabelNewCuenta.setDisable(false);
        fxLabelNewPassword.setVisible(true);
        fxLabelNewPassword.setDisable(false);
        tfUser.setVisible(true);
        tfUser.setDisable(false);
        tfUser.clear();
        tfPassword.setVisible(true);
        tfPassword.setDisable(false);
        tfPassword.clear();
        fxButton.setVisible(true);
        fxButton.setDisable(false);
    }

    @FXML
    public void newPassword() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(Constantes.FXML_NEWPASSWORD));
        Parent parent = fxmlLoader.load();
        DialogNewPwdController dialogController = fxmlLoader.getController();
        dialogController.setBorderPane(this.mainController);

        Scene scene = new Scene(parent, 450, 200);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.showAndWait();
    }

    @FXML
    public void nuevaCuentaPag() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(Constantes.FXML_NEWUSUARIO));
        Parent parent = fxmlLoader.load();
        DialogNewUsuarioController dialogController = fxmlLoader.getController();
        dialogController.setBorderPane(this.mainController);

        Scene scene = new Scene(parent, 400, 300);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.showAndWait();
    }

    @FXML
    public void changeCursorHand() {
        fxButton.getScene().setCursor(Cursor.HAND);
    }

    @FXML
    public void changeCursorArrow() {
        fxButton.getScene().setCursor(Cursor.DEFAULT);
    }

}
