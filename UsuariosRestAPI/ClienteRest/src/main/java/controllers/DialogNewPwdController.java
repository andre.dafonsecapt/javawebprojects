/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import models.Usuario;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author andre
 */
public class DialogNewPwdController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    private Alert alert;

    @FXML
    private TextField tfEmail;

    @FXML
    private Button fxButton;

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
    }

    @FXML
    public void sendPwd(ActionEvent event) {

        if (!tfEmail.getText().equals("")) {

            Task<String> tarea = new Task<String>() {

                @Override
                protected String call() throws Exception {

                    return mainController.getAppServices().sendMail(tfEmail.getText());
                }
            };

            tarea.setOnSucceeded(cadena -> {
                        alert.setContentText(tarea.getValue());
                        alert.showAndWait();
                        closeStage(event);
                    }
            );
            tarea.setOnFailed(workerStateEvent -> {
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error ",
                                workerStateEvent.getSource().getException());
                closeStage(event);
            });
            new Thread(tarea).start();

        } else {
            alert.setContentText("Error - Campo de email vacio.");
            alert.showAndWait();
        }
    }


    @FXML
    public void changeCursorHand() {
        fxButton.getScene().setCursor(Cursor.HAND);
    }

    @FXML
    public void changeCursorArrow() {
        fxButton.getScene().setCursor(Cursor.DEFAULT);
    }

    private void closeStage(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

}
