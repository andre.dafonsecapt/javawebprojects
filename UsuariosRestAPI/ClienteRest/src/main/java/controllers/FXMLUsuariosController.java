package controllers;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import models.Usuario;

import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLUsuariosController implements Initializable {

    @FXML
    private FXMLMainController mainController;

    @FXML
    private ListView<Usuario> fxListViewUsuarios;

    @FXML
    private TextField tfUsername;

    @FXML
    private TextField tfPassword;

    @FXML
    private TextField fxTxtBusqueda;

    private Alert alert;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
    }

    @FXML
    void setBorderPane(FXMLMainController borderPane) {
        this.mainController = borderPane;
    }

    public void clearFields() {
        tfUsername.clear();
        tfPassword.clear();
        fxTxtBusqueda.clear();
        fxListViewUsuarios.getItems().clear();
    }

    public void fxAddUsuario() {
        if (!tfUsername.getText().equals("") && !tfPassword.getText().equals("")) {

            Task<String> tarea = new Task<String>() {

                @Override
                protected String call() throws Exception {

                    return mainController.getAppServices().addUsuario(new Usuario(0, tfUsername.getText(), tfPassword.getText()));
                }
            };

            tarea.setOnSucceeded(cadena -> {
                        alert.setContentText(tarea.getValue());
                        alert.showAndWait();
                        clearFields();
                        loadUsuarios();
                    }
            );
            tarea.setOnFailed(workerStateEvent ->
                    Logger.getLogger("FXMAIN")
                            .log(Level.SEVERE, "error ",
                                    workerStateEvent.getSource().getException()));
            new Thread(tarea).start();

        } else {
            alert.setContentText("Error - Uno o más campos vacios.");
            alert.showAndWait();
        }
    }

    public void fxUpdateUsuario() {
        if (fxListViewUsuarios.getSelectionModel().getSelectedItem() != null) {
            if (!tfUsername.getText().equals("")) {

                Task<String> tarea = new Task<String>() {

                    @Override
                    protected String call() throws Exception {

                        Usuario u = new Usuario(0, tfUsername.getText(), tfPassword.getText());
                        u.setIdUsuario((fxListViewUsuarios.getSelectionModel().getSelectedItem()).getIdUsuario());
                        return mainController.getAppServices().updateUsuario(u);
                    }
                };

                tarea.setOnSucceeded(cadena -> {
                            alert.setContentText(tarea.getValue());
                            alert.showAndWait();
                            clearFields();
                            loadUsuarios();
                        }
                );
                tarea.setOnFailed(workerStateEvent ->
                        Logger.getLogger("FXMAIN")
                                .log(Level.SEVERE, "error ",
                                        workerStateEvent.getSource().getException()));
                new Thread(tarea).start();

            } else {
                alert.setContentText("Error - El campo de Usuario no puede estar vacio.");
                alert.showAndWait();
            }
        } else {
            alert.setContentText("Error - Selecciona un Usuario primero.");
            alert.showAndWait();
        }
    }

    public void fxBorrarUsuario() {

        if (fxListViewUsuarios.getSelectionModel().getSelectedItem() != null) {

            Task<String> tarea = new Task<String>() {

                @Override
                protected String call() throws Exception {

                    Usuario u = fxListViewUsuarios.getSelectionModel().getSelectedItem();
                    return mainController.getAppServices().deleteUsuario(u.getIdUsuario());
                }
            };

            tarea.setOnSucceeded(cadena -> {
                        alert.setContentText(tarea.getValue());
                        alert.showAndWait();
                        clearFields();
                        loadUsuarios();
                    }
            );
            tarea.setOnFailed(workerStateEvent ->
                    Logger.getLogger("FXMAIN")
                            .log(Level.SEVERE, "error ",
                                    workerStateEvent.getSource().getException()));
            new Thread(tarea).start();

        } else {
            alert.setContentText("Error - Selecciona un Usuario primero.");
            alert.showAndWait();
        }
    }

    private void loadUsuarios() {
        Task<List<Usuario>> tarea = new Task<List<Usuario>>() {

            @Override
            protected List<Usuario> call() throws Exception {

                return mainController.getAppServices().getUsuarios();
            }
        };
        tarea.setOnSucceeded(cadena -> {
            fxListViewUsuarios.getItems().clear();
            fxListViewUsuarios.getItems().addAll(tarea.getValue());
        });
        tarea.setOnFailed(workerStateEvent ->
                Logger.getLogger("FXMAIN")
                        .log(Level.SEVERE, "error",
                                workerStateEvent.getSource().getException()));
        new Thread(tarea).start();
    }

    public void buscarUsuarioPorID() {

        try {
            if (fxTxtBusqueda.getText().equals("")) {

                Task<List<Usuario>> tarea = new Task<List<Usuario>>() {

                    @Override
                    protected List<Usuario> call() throws Exception {

                        return mainController.getAppServices().getUsuarios();
                    }
                };

                tarea.setOnSucceeded(cadena -> {
                    fxListViewUsuarios.getItems().clear();
                    fxListViewUsuarios.getItems().addAll(tarea.getValue());
                });
                tarea.setOnFailed(workerStateEvent ->
                        Logger.getLogger("FXMAIN")
                                .log(Level.SEVERE, "error ",
                                        workerStateEvent.getSource().getException()));
                new Thread(tarea).start();

            } else {
                int idUsuario = Integer.parseInt(fxTxtBusqueda.getText());

                Task<Usuario> tarea = new Task<Usuario>() {

                    @Override
                    protected Usuario call() throws Exception {

                        return mainController.getAppServices().getUsuario(idUsuario);
                    }
                };

                tarea.setOnSucceeded(cadena -> {
                    fxListViewUsuarios.getItems().clear();
                    fxListViewUsuarios.getItems().add(tarea.getValue());
                });
                tarea.setOnFailed(workerStateEvent ->
                        Logger.getLogger("FXMAIN")
                                .log(Level.SEVERE, "error ",
                                        workerStateEvent.getSource().getException()));
                new Thread(tarea).start();
            }
        } catch (Exception e) {
            alert.setContentText("Error - Formato inválido de ID");
            alert.showAndWait();
        }

    }

    public void setListeners(){
        fxListViewUsuarios.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue!=null){
                tfUsername.setText(newValue.getUsername());
            }
        });
    }

}
