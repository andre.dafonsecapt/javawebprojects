package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import services.AppServices;
import utils.Constantes;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FXMLMainController implements Initializable {

    @FXML
    private BorderPane fxRoot;

    @FXML
    private Menu fxMenuUsuario;

    @FXML
    private MenuItem fxMenuItemLogout;

    @FXML
    private MenuItem fxMenuItemLogin;

    private AppServices appServices;
    private AnchorPane pagLogin;
    private FXMLLoginController loginController;
    private AnchorPane pagUsuarios;
    private FXMLUsuariosController usuariosController;

    AppServices getAppServices() {
        return appServices;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        appServices = new AppServices();
        menuLogin();

    }

    void enableMenu() {
        fxMenuUsuario.setDisable(false);
        fxMenuItemLogin.setDisable(true);
        fxMenuItemLogout.setDisable(false);
    }

    @FXML
    private void menuLogin() {

        try {
            if (pagLogin == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(Constantes.FXML_LOGIN));
                pagLogin = loaderMenu.load();
                loginController = loaderMenu.getController();
                loginController.setBorderPane(this);
            }
            loginController.enableFields();
            fxRoot.setCenter(pagLogin);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void logout() {
        Alert alertConfirm = new Alert(Alert.AlertType.CONFIRMATION);
        alertConfirm.setTitle("Confirmation Dialog");
        alertConfirm.setHeaderText(null);
        ButtonType buttonTypeYes = new ButtonType("Yes");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);
        alertConfirm.getButtonTypes().setAll(buttonTypeYes, buttonTypeCancel);
        alertConfirm.setContentText("Are you sure you want to logout?");
        Optional<ButtonType> result = alertConfirm.showAndWait();
        if (result.isPresent() && result.get() == buttonTypeYes) {
            fxMenuUsuario.setDisable(true);
            fxMenuItemLogin.setDisable(false);
            fxMenuItemLogout.setDisable(true);
            menuLogin();
        }
    }

    @FXML
    private void menuGestion() {
        try {
            if (pagUsuarios == null) {
                FXMLLoader loaderMenu
                        = new FXMLLoader(getClass().getResource(Constantes.FXML_GESTION));
                pagUsuarios = loaderMenu.load();
                usuariosController = loaderMenu.getController();
                usuariosController.setBorderPane(this);
            }
            usuariosController.setListeners();
            usuariosController.clearFields();
            fxRoot.setCenter(pagUsuarios);

        } catch (IOException ex) {
            Logger.getLogger(FXMLMainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
