package rest;


import models.Usuario;
import services.ServicesUsuario;
import utils.PasswordHash;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Path("usuario")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Usuarios {


  @Inject
  private ServicesUsuario su;

  @Inject
  private PasswordHash ph;


  @GET
  public List<Usuario> getUsers(@Context HttpServletResponse response, @Context HttpServletRequest request)
  {
    return su.getAll();
  }


  @GET
  @Path("/get/{idUsuario}")
  public Usuario getUser(@PathParam("idUsuario") int idUsuario)
  {
    return su.get(idUsuario);
  }

//  @GET
//  @Produces(MediaType.APPLICATION_JSON)
//  @Path("/get/{login}")
//  public Usuario getUserLogin(@PathParam("login") String login,@QueryParam("loginId") String loginId)
//  {
//    if (login == null)
//    {
//      login = loginId;
//    }
//    ServiciosUsuarios su = new ServiciosUsuarios();
//    return su.getUser(login);
//  }

  @POST
  @Filtered
  public Response addUsuario(Usuario user)
  {
    return Response.ok(su.add(user)).build();
  }
//
//
//  @POST
//  @Consumes(MediaType.TEXT_PLAIN)
//  public Response addUsuario(String user )
//  {
//    return Response.ok(su.addUser(new Usuario(user,user))).build();
//  }

  @PUT
  public String updateUsuario(Usuario usuario, @Context HttpServletResponse resp) throws IOException {

    String response = null;
    if (!usuario.getPassword().equals("")) {
      //////////////////////////////////////////// actualiza la password del usuario
      try {
        usuario.setPassword(ph.createHash(usuario.getPassword()));
      } catch (Exception e) {
        e.printStackTrace();
        resp.sendError(501,"ERROR SETTING PASSWORD");
      }
      if (su.update(usuario)) {
        response = "Usuario actualizado con exito.";
      } else {
        resp.sendError(502,"ERROR UPDATE USUARIO");
      }
    } else {
      //////////////////////////////////////////// actualiza el username del usuario
      if (su.updateUsername(usuario)) {
        response = "Usuario actualizado con exito.";
      } else {
        resp.sendError(503,"ERROR UPDATE USERNAME");
      }
    }
    return response;
  }

  @DELETE
  public Response delUsuario(@QueryParam("idUsuario") int idUsuario, @Context HttpServletResponse resp) throws IOException {
    String response = null;
    if (su.delete(idUsuario)) {
      response = "Usuario borrado con exito.";
    } else {
      resp.sendError(505,"ERROR DELETE Usuario");
    }
    return Response.ok(response).build();
  }

}
