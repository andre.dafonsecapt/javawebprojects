package servlets;


import models.Usuario;
import services.ServicesMail;
import services.ServicesUsuario;
import utils.Constantes;
import utils.PasswordHash;
import utils.Utils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "Mail", urlPatterns = {"/mail"})
public class ServletMail extends javax.servlet.http.HttpServlet {

    @Inject
    @RequestScoped
    private ServicesUsuario servUsuario;

    @Inject
    @RequestScoped
    private ServicesMail servMail;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        if (req.getParameter("email") == null) {
            try {
                String code = req.getParameter("code");
                LocalDateTime codeDate = servUsuario.getDateByCode(code);
                if (ChronoUnit.SECONDS.between(codeDate, LocalDateTime.now()) <= 50) {
                    if(servUsuario.activateUsuario(code)) {
                        req.setAttribute("mensaje", "Tu cuenta ha sido correctamente activada");
                    } else {
                        req.setAttribute("mensaje", "Error - No se ha podido activar la cuenta.");
                    }
                } else {
                    req.setAttribute("mensaje", "<p>El codigo de activación ha expirado.</p> " +
                            "   <form name=\"myform\" action=\"/ServidorRest/mail\" method=\"POST\">\n" +
                            "       <input type=\"submit\" value=\"Reenviar código\" name=\"reset\"/>\n" +
                            "   </form>");
                    req.getSession().setAttribute("code", code);
                }
                req.getRequestDispatcher("autenticacion.jsp").forward(req, resp);
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            String response = null;
            Usuario u = new Usuario();
            u.setUsername(req.getParameter("email"));
            PasswordHash ph = new PasswordHash();
            try {
                String newPassword = Utils.randomAlphaNumeric(10);
                u.setPassword(ph.createHash(newPassword));

                if (servUsuario.updatePassword(u)) {
                    if (servMail.mandarMail(u.getUsername(), "<html>Tu nueva contraseña: <copy><b>" + newPassword + "</b><copy></html>", "Password Reset")) {
                        response = "Tu nueva contraseña ha sido enviada para este correo.";
                    } else {
                        resp.setStatus(505);
                    }
                } else {
                    resp.setStatus(404);
                }
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
                resp.setStatus(500);
            }
            req.setAttribute("response", response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String code = (String) req.getSession().getAttribute("code");
        String userName = servUsuario.findUserByCode(code).getUsername();
        String newCode = Utils.randomBytes();

        if (servMail.mandarMail(userName, "<html><p>Has recibido un nuevo código de activación</p>Para activar la cuenta hace clique <a href=" + Constantes.BASE_URL + Constantes.URL_MAIL + Constantes.URL_CodeQuery + newCode + ">aqui</a></html>", "Reenvio de Código de Autenticación")) {

            servUsuario.updateCode_Date(newCode, userName);

            try (PrintWriter out = resp.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Reset Código</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Reset de código</h1>");
                out.println("<p>Un nuevo código de activación ha sido enviado a tu correo.</p>");
                out.println("</body>");
                out.println("</html>");
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
