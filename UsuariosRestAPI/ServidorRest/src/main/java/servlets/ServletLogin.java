package servlets;

import models.Usuario;
import services.ServicesUsuario;
import utils.PasswordHash;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "Login", urlPatterns = {"/login"})
public class ServletLogin extends javax.servlet.http.HttpServlet {

    @Inject
    @RequestScoped
    private ServicesUsuario su;

    @Inject
    @RequestScoped
    private PasswordHash ph;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void processRequest(HttpServletRequest req, HttpServletResponse resp){
        resp.setCharacterEncoding("UTF-8");
        // coge el usuario y hace hash de su password
        Usuario usuarioLogin = (Usuario) req.getAttribute("usuario");
        // busca el usuario en la base de datos
        Usuario usuarioDB = su.findUser(usuarioLogin);
        String response = null;
        try {
            if (usuarioDB != null) {
                if (ph.validatePassword(usuarioLogin.getPassword(), usuarioDB.getPassword())) {
                    if(su.isActivated(usuarioLogin.getUsername())) {
                        req.getSession().setAttribute("login", "OK");
                        response = "loginOK";
                    } else {
                        response = "Error: Este usuario aún no ha sido activado.";
                    }
                } else {
                    response = "Error: Password inválida.";
                }
            } else {
                response = "Error: Usuario inválido.";
            }
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        req.setAttribute("response", response);
    }
}
