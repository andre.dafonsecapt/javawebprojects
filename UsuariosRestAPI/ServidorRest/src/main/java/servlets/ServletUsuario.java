package servlets;


import models.Usuario;
import services.ServicesUsuario;
import utils.PasswordHash;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "Usuario", urlPatterns = {"/private/usuarios"})
public class ServletUsuario extends javax.servlet.http.HttpServlet {

    @Inject
    @RequestScoped
    private ServicesUsuario su;

    @Inject
    @RequestScoped
    private PasswordHash ph;

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Usuario usuario = (Usuario) req.getAttribute("usuario");
        String response = null;
        if (!usuario.getPassword().equals("")) {
            try {
                usuario.setPassword(ph.createHash(usuario.getPassword()));
            } catch (Exception e) {
                e.printStackTrace();
                resp.setStatus(500);
            }
            if (su.update(usuario)) {
                response = "Usuario actualizado con exito.";
            } else {
                resp.setStatus(500);
            }
        } else {
            if (su.updateUsername(usuario)) {
                response = "Usuario actualizado con exito.";
            } else {
                resp.setStatus(500);
            }
        }
        req.setAttribute("response", response);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameter("idUsuario") != null) {
            int idUsuario = Integer.parseInt(req.getParameter("idUsuario"));
            req.setAttribute("response", su.get(idUsuario));
        } else {
            List<Usuario> usuarios = su.getAll();
            usuarios.forEach(usuario -> usuario.setPassword(""));
            req.setAttribute("response", usuarios);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idUsuario = Integer.parseInt(req.getParameter("idUsuario"));
        String response = null;
        if (su.delete(idUsuario)) {
            response = "Usuario borrado con exito.";
        } else {
            resp.setStatus(500);
        }
        req.setAttribute("response", response);
    }
}
