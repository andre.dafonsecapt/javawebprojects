package servlets;

import models.Usuario;
import services.ServicesMail;
import services.ServicesUsuario;
import utils.Constantes;
import utils.PasswordHash;
import utils.Utils;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@WebServlet(name = "NuevaCuenta", urlPatterns = {"/nuevacuenta"})
public class ServletNuevaCuenta extends javax.servlet.http.HttpServlet {

    @Inject
    @RequestScoped
    private ServicesUsuario su;

    @Inject
    @RequestScoped
    private PasswordHash ph;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");
        ServicesMail servicesMail = new ServicesMail();

        Usuario usuario = (Usuario) req.getAttribute("usuario");
        // add attributes to usuario
        try {
            usuario.setPassword(ph.createHash(usuario.getPassword()));
        } catch (Exception e) {
            e.printStackTrace();
            resp.setStatus(503);
        }
        String code = Utils.randomBytes();
        usuario.setCode(code);
        // response logic
        String response = null;
        if (su.add(usuario)) {
            if (servicesMail.mandarMail(usuario.getUsername(), "<html>Para activar la cuenta hace clique <a href=" + Constantes.BASE_URL + Constantes.URL_MAIL + Constantes.URL_CodeQuery + code + ">aqui</a></html>", "Código de Autenticación")) {
                response = "Un codigo de activación ha sido enviado a tu correo.";
            } else {
                resp.setStatus(501);
            }
        } else {
            resp.setStatus(500);
        }
        req.setAttribute("response", response);
    }
}
