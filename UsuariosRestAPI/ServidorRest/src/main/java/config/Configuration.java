package config;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;

public class Configuration {

  private static Configuration config;

  private Configuration() {

  }

  public static Configuration getInstance() {

    return config;
  }

  public static Configuration getInstance(InputStream file) {
    if (config == null) {
      Yaml yaml = new Yaml();
      config = yaml.loadAs(
          file,
          Configuration.class);

    }
    return config;
  }

  private String user;
  private String password;
  private String urlDB;
  private String driver;

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getUrlDB() {
    return urlDB;
  }

  public void setUrlDB(String urlDB) {
    this.urlDB = urlDB;
  }

  public String getDriver() {
    return driver;
  }

  public void setDriver(String driver) {
    this.driver = driver;
  }
}
