package dao;

import models.Usuario;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import utils.Constantes;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RequestScoped
@Named
public class DaoUsuarios {


    public DaoUsuarios() {

    }

    public Usuario get(int idUsuario) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.query(Constantes.SELECT_USUARIO_QUERY,
                new Object[]{idUsuario},
                BeanPropertyRowMapper.newInstance(Usuario.class)).get(0);
    }

    public List<Usuario> getAll() {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.query(Constantes.SELECT_USUARIOS_QUERY,
                BeanPropertyRowMapper.newInstance(Usuario.class));
    }

    public LocalDateTime getDateByCode(String code) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.queryForObject(Constantes.SELECT_DateByCode_QUERY,
                new Object[]{code},
                Timestamp.class).toLocalDateTime();
    }

    public boolean add(Usuario u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        jtm.execute(Constantes.ALTER_AutoIncrement_QUERY);
        boolean success;
        try {
            jtm.update(Constantes.INSERT_USUARIO_QUERY, u.getUsername(), u.getPassword(), u.getCode(), LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            success = true;
        } catch (DuplicateKeyException e){
            success = false;
        }
        return success;
    }

    public boolean delete(int idUsuario) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constantes.DELETE_USUARIO_QUERY, idUsuario) == 1;
    }

    public boolean update(Usuario u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constantes.UPDATE_USUARIO_QUERY, u.getUsername(), u.getPassword(), u.getIdUsuario()) == 1;
    }

    public boolean updateUsername(Usuario u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constantes.UPDATE_USUARIOUsername_QUERY, u.getUsername(), u.getIdUsuario()) == 1;
    }

    public boolean updatePassword(Usuario u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constantes.UPDATE_USUARIOPassword_QUERY, u.getPassword(), u.getUsername()) == 1;
    }

    public boolean updateCode(String code, String userName) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constantes.UPDATE_USUARIOCode_Date_QUERY, code, LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), userName) == 1;
    }

    public Usuario findUser(Usuario u) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.query(Constantes.SELECT_USUARIOName_QUERY,
                new Object[]{u.getUsername()},
                BeanPropertyRowMapper.newInstance(Usuario.class)).stream().findFirst().orElse(null);
    }

    public Usuario findUserByCode(String code) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.query(Constantes.SELECT_USUARIOCode_QUERY,
                new Object[]{code},
                BeanPropertyRowMapper.newInstance(Usuario.class)).stream().findFirst().orElse(null);
    }

    public boolean activateUsuario(String code) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.update(Constantes.UPDATE_USUARIOActivated_QUERY, code) == 1;
    }

    public boolean isActivated(String username) {
        JdbcTemplate jtm = new JdbcTemplate(DBConnPool.getInstance().getPool());
        return jtm.queryForObject(Constantes.SELECT_USUARIOActivated_QUERY, new Object[]{username}, Integer.class) > 0;
    }
}
