package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

@WebFilter(filterName = "FilterLogin", urlPatterns = {"/private/*"})
public class FilterLogin implements Filter {


    @Override
    public void init(FilterConfig config) throws ServletException {
        // necessary empty method
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        try {
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse resp = (HttpServletResponse) res;
            boolean loggedIn = (null != request.getSession().getAttribute("login") && request.getSession().getAttribute("login").equals("OK"));
            if (loggedIn) {
                chain.doFilter(request, res);
            } else {
                resp.setStatus(403);
            }
        } catch (Exception t) {
            Logger.getLogger("Main").info(t.getMessage());
        }
    }

    @Override
    public void destroy() {
        // necessary empty method
    }

}
