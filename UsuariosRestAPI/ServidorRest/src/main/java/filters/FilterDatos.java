package filters;

import models.Usuario;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "FilterDatos", urlPatterns = {"/login" , "/mail" , "/nuevacuenta"})
public class FilterDatos implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    private void doBeforeProcessing(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Usuario usuario;
        try {
            Jsonb jsonb = JsonbBuilder.create();
            usuario = jsonb.fromJson(request.getReader(), Usuario.class);
        } catch (Exception e) {
            usuario = null;
        }
        request.setAttribute("usuario", usuario);
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

        doBeforeProcessing((HttpServletRequest) req, (HttpServletResponse) resp);

        chain.doFilter(req, resp);

        doAfterProcessing((HttpServletRequest) req, (HttpServletResponse) resp);
    }

    private void doAfterProcessing(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (request.getAttribute("response") != null) {
            Jsonb jsonb = JsonbBuilder.create();
            jsonb.toJson(request.getAttribute("response"), response.getWriter());
        }
    }


    @Override
    public void destroy() {
        //necessary empty method
    }
}
