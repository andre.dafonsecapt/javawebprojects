package services;

import dao.DaoUsuarios;
import models.Usuario;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.List;

@RequestScoped
@Named
public class ServicesUsuario {

    @Inject
    private DaoUsuarios daoUsuario;

    public ServicesUsuario() {
        this.daoUsuario = new DaoUsuarios();
    }

    public Usuario get(int idUsuario){
        return daoUsuario.get(idUsuario);
    }

    public List<Usuario> getAll(){
        return daoUsuario.getAll();
    }

    public LocalDateTime getDateByCode(String code){
        return daoUsuario.getDateByCode(code);
    }

    public boolean add(Usuario usuario){
        return daoUsuario.add(usuario);
    }
    
    public boolean delete(int idUsuario){
        return daoUsuario.delete(idUsuario);
    }

    public boolean update(Usuario usuario){
        return daoUsuario.update(usuario);
    }

    public boolean updateUsername(Usuario usuario){
        return daoUsuario.updateUsername(usuario);
    }

    public boolean updatePassword(Usuario usuario){
        return daoUsuario.updatePassword(usuario);
    }

    public boolean updateCode_Date(String code,  String userName){
        return daoUsuario.updateCode(code, userName);
    }

    public Usuario findUser(Usuario usuario){
        return daoUsuario.findUser(usuario);
    }

    public Usuario findUserByCode(String code){
        return daoUsuario.findUserByCode(code);
    }

    public boolean activateUsuario(String code){
        return daoUsuario.activateUsuario(code);
    }

    public boolean isActivated(String username){
        return daoUsuario.isActivated(username);
    }
}
