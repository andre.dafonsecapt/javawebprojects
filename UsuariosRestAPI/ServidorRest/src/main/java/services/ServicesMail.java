/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;


import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author oscar
 */
public class ServicesMail {

    
    public boolean mandarMail(String to, String msg, String subject) {
        boolean mailEnviado = false;
        try {
            Email email = new SimpleEmail();
            
            email.setHostName("smtp.gmail.com");
            email.setSmtpPort(Integer.parseInt("587"));
            email.setAuthentication("gestormail3000@gmail.com", "Domino147258");
//          email.setSSLOnConnect(true);
            email.setStartTLSEnabled(true);
            email.setFrom("gestormail3000@gmail.com");
            email.setSubject(subject);
            email.setContent(msg,"text/html");
            email.addTo(to);
            email.send();
            mailEnviado = true;
        } catch (EmailException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return mailEnviado;
    }
    
    
    public boolean generateAndSendEmail(String to, String msg, String subject) throws AddressException, MessagingException {
        boolean mailEnviado = false;
        try {
            Properties mailServerProperties;
            Session getMailSession;
            MimeMessage generateMailMessage;

            // Step1

            mailServerProperties = System.getProperties();
            mailServerProperties.put("mail.smtp.port", Integer.parseInt("587"));
            mailServerProperties.put("mail.smtp.auth", "true");
            mailServerProperties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
            mailServerProperties.put("mail.smtp.starttls.enable", "true");

            // Step2

            getMailSession = Session.getDefaultInstance(mailServerProperties, null);
            generateMailMessage = new MimeMessage(getMailSession);
            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            generateMailMessage.setSubject(subject);
            String emailBody = msg;
            generateMailMessage.setContent(emailBody, "text/html");


            // Step3

            Transport transport = getMailSession.getTransport("smtp");

            // Enter your correct gmail UserID and Password
            // if you have 2FA enabled then provide App Specific Password
            transport.connect("smtp.gmail.com",
                    "alumnosDamQuevedo@gmail.com",
                    "quevedo2019");
            transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
            transport.close();
            mailEnviado = true;
        } catch (Exception e){
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
        return mailEnviado;
    }
}