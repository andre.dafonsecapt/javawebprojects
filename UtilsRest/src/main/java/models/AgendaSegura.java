package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgendaSegura {

    private int idAgenda;
    private String iv;
    private String salt;
    private int iteraciones;
    private int keySize;
    private String agenda;
    private String encryptKey;
    private String fk_idUsuario;
}
