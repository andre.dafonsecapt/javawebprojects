package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MusicaGetAndPostDTO {

    private int idMusica;
    @NotBlank(message = "Nombre no puede estar vacio.")
    private String nombre;
    @NotBlank(message = "Artista no puede estar vacio.")
    private String artista;
    @PastOrPresent(message = "Fecha de Grabación no puede estar en el futuro.")
    private LocalDate fechaGrabacion;
    private LocalDateTime fechaDB;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("id: ").append(idMusica);
        sb.append(", Nombre: ").append(nombre);
        sb.append(", Artista: ").append(artista);
        sb.append(", Grabación: ").append(fechaGrabacion);
        sb.append(", Fecha DB: ").append(fechaDB.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        return sb.toString();
    }

}
