package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsuarioGetDTO {

    @NotBlank(message = "ID no puede estar vacio.")
    private int idUsuario;
    @NotBlank(message = "Username no puede estar vacio.")
    private String username;

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("Id: ").append(idUsuario);
        sb.append(" - Username: ").append(username);
        return sb.toString();
    }
}
