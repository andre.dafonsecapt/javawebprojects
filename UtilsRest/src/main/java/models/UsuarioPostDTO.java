package models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UsuarioPostDTO {

    private int idUsuario;
    @Email
    @NotBlank(message = "Username no puede estar vacio.")
    private String username;
    @NotBlank(message = "Password no puede estar vacia.")
    private String password;
    private String clavePublica;
}
