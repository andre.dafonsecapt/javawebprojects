package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MusicaLiveGetDTO {

    private List<String> liveActs;

    @Override
    public String toString() {
        return liveActs.toString();
    }
}
