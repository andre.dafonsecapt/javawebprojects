package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AgendaInvitacion {

    private int idagenda_invitacion;
    private String iv;
    private String salt;
    private int iteraciones;
    private int keySize;
    private String agenda;
    private String encryptKey;
    private String firma;
    private int fk_idInvitado;
    private int fk_idInvitador;
}
