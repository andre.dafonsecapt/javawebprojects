package config;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.List;

public class Configuration {

  private static Configuration config;

  private Configuration() {

  }

  public static Configuration getInstance() {

    return config;
  }

  public static Configuration getInstance(InputStream file) {
    if (config == null) {
      Yaml yaml = new Yaml();
      config = (Configuration) yaml.loadAs(
          file,
          Configuration.class);

    }
    return config;
  }

    private List<String> pisos;
    private List<Usuario> usuarios;
    private List<String> informes;

    public List<String> getPisos() {
      return pisos;
    }

    public void setPisos(List<String> pisos) {
      this.pisos = pisos;
    }
    
    public List<Usuario> getUsuarios(){
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public List<String> getInformes() {
        return informes;
    }

    public void setInformes(List<String> informes) {
        this.informes = informes;
    }
}
