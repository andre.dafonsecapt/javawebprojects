/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filters;

import config.Usuario;
import seguridad.Cifrar;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author andre
 */
@WebFilter(filterName = "FilterCifrado", urlPatterns = {"/cifrado/*"})
public class FilterCifrado implements Filter {

    private void doBeforeProcessing(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        if (null != request.getParameter("informeIndex")) {
            Cifrar c = new Cifrar();
            int numeroCesar = Integer.parseInt(request.getSession().getAttribute("numeroCesar").toString());
            int informeIndexDescifrado = Integer.parseInt(c.descifrarString(request.getParameter("informeIndex"), numeroCesar));
            request.setAttribute("informeIndexDescifrado", informeIndexDescifrado);
        }

    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        doBeforeProcessing((HttpServletRequest) req,(HttpServletResponse) resp);

        chain.doFilter(req, resp);

        doAfterProcessing((HttpServletRequest) req, (HttpServletResponse) resp);
    }

    private void doAfterProcessing(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        Cifrar c = new Cifrar();
        int numeroCesar = Integer.parseInt(request.getSession().getAttribute("numeroCesar").toString());
        String resultado = "";

        if (null != request.getAttribute("usuario")) {
            Usuario usuarioTemp = (Usuario) request.getAttribute("usuario");
            String username = usuarioTemp.getUsername();
            String password = usuarioTemp.getPassword();
            String ubicacion = usuarioTemp.getUbicacion();
            String edad = Integer.toString(usuarioTemp.getEdad());
            resultado = username.concat(",").concat(password).concat(",").concat(ubicacion).concat(",").concat(edad);
        }

        if (null != request.getAttribute("pisos")) {
            List<String> pisosList = (List<String>) request.getAttribute("pisos");
            resultado = pisosList.toString();
        }
        
        if (null != request.getAttribute("informes") && null != request.getAttribute("informeIndexDescifrado")){
             List<String> informes = (List<String>)request.getAttribute("informes");
             int informeIndexDescifrado = Integer.parseInt(request.getAttribute("informeIndexDescifrado").toString());
             try {
                 resultado = informes.get(informeIndexDescifrado - 1);
             } catch (Exception e){
                 resultado = "Esse informe no existe.";
             }
        }
        response.getWriter().println(c.cifrarString(resultado, numeroCesar));

    }

    @Override
    public void init(FilterConfig config) throws ServletException {

    }

    @Override
    public void destroy() {
    }

}
