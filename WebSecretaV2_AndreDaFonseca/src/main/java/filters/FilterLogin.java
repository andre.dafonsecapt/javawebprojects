/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.Logger;

/**
 *
 * @author andre
 */
@WebFilter(filterName = "FilterLogin", urlPatterns = {"/WebSecretaV2/*"})
public class FilterLogin implements Filter {
    
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        try {
            HttpServletRequest request = (HttpServletRequest) req;
            boolean loggedIn = null != request.getSession().getAttribute("login") && request.getSession().getAttribute("login").equals("OK");
            boolean loginRequest = request.getRequestURI().equals(request.getContextPath() + "/login");
            if (loggedIn || loginRequest) {
                chain.doFilter(request, res);
            } else {
                res.getWriter().println("Error: Haz el Login primero!");
            }
        } catch (Throwable t) {
            Logger.getLogger("Main").info(t.getMessage());
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
