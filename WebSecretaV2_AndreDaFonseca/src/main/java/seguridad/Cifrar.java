/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package seguridad;

import config.Configuration;

/**
 * @author andre
 */
public class Cifrar {

    public static String cifrarString(String s, int numeroCesar) {
        StringBuffer result = new StringBuffer();
        char c;
        for (int i = 0; i < s.length(); i++) {
            c = s.charAt(i);
            int nC = numeroCesar;
            if (Character.isLetter(c) && c != 'º') {
                c = (char) (s.charAt(i) + nC);
                while (Character.isLowerCase(s.charAt(i)) && (c < 'a'|| c > 'z') || (Character.isUpperCase(s.charAt(i)) && (c < 'A'|| c > 'Z'))) {
                    c = (char) (s.charAt(i) - (26 - nC));
                    nC -= 26;
                }
            } else if (Character.isDigit(s.charAt(i))) {
                c = (char) (s.charAt(i) + nC);
                while (c > '9') {
                    c = (char) (s.charAt(i) - (10 - nC));
                    nC -= 10;
                }
            }
            result.append(c);
        }
        return result.toString();
    }

    public static String descifrarString(String s, int numeroCesar) {
        StringBuffer result = new StringBuffer();
        char c;
        for (int i = 0; i < s.length(); i++) {
            c = s.charAt(i);
            int nC = numeroCesar;
            if (Character.isLetter(c) && c != 'º') {
                c = (char) (s.charAt(i) - nC);
                while ((Character.isLowerCase(s.charAt(i)) && c < 'a') || (Character.isUpperCase(s.charAt(i)) && c < 'A')) {
                    c = (char) (s.charAt(i) + (26 - nC));
                    nC -= 26;
                }
            } else if (Character.isDigit(s.charAt(i))) {
                c = (char) (s.charAt(i) - nC);
                while (c < '0' || c > '9') {
                    c = (char) (s.charAt(i) + (10 - nC));
                    nC -= 10;
                }
            }
            result.append(c);
        }
        return result.toString();
    }

}
